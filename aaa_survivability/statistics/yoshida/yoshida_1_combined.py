import numpy as np
import pandas as pd
import sys

import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')

# Displays results from Yoshida 1 scenario

data_directory = "./data/2018_03_25_06_51_48/"
image_path = None
legend_location = 2

if len(sys.argv) >= 2:
    data_directory = sys.argv[1]

if len(sys.argv) >= 3:
    image_path = sys.argv[2]

filenames = ["scenario_1_AnimatBaseline.csv",
             "scenario_1_Animat.csv", 
             "scenario_1_RandomAgent.csv", 
             "scenario_1_AnimatIncrementalQLearner.csv"
             ]

legend = ["Baseline", "Animat", "Random", "Q-learning"]

# load pandas
data = []
for filename in filenames:
    data.append(pd.read_csv(data_directory + filename))

# Episode length
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Episode length", ylabel="Time steps", xlabel="Trained episodes")

for d in data:
    ax.plot(d.loc[:,'Trained episodes'].values, 
            d.loc[:,'Episode length (mean)'].values, 
            marker='.')

plt.legend(legend, loc=legend_location)
if image_path:
    fig.savefig(image_path+"/combined_episode_length.png")

# Energy level
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Energy level", ylabel="Energy", xlabel="Trained episodes")

for d in data:
    ax.plot(d.loc[:,'Trained episodes'].values, 
            d.loc[:,'Energy level (mean)'].values, 
            marker='.')

plt.legend(legend, loc=legend_location)
if image_path:
    fig.savefig(image_path+"/combined_energy_level.png")

# Need
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Need", ylabel="Need", xlabel="Trained episodes")

for d in data:
    ax.plot(d.loc[:,'Trained episodes'].values, 
            d.loc[:,'Need (mean)'].values, 
            marker='.')

plt.legend(legend, loc=legend_location)
if image_path:
    fig.savefig(image_path+"/combined_need.png")

# Food consumed
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Food consumed", ylabel="Food", xlabel="Trained episodes")

for d in data:
    ax.plot(d.loc[:,'Trained episodes'].values, 
            d.loc[:,'Food consumed (mean)'].values, 
            marker='.')

plt.legend(legend, loc=legend_location)
if image_path:
    fig.savefig(image_path+"/combined_food_consumed.png")

# Poison consumed
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Poison consumed", ylabel="Poison", xlabel="Trained episodes")

for d in data:
    ax.plot(d.loc[:,'Trained episodes'].values, 
            d.loc[:,'Poison consumed (mean)'].values, 
            marker='.')

plt.legend(legend, loc=legend_location)
if image_path:
    fig.savefig(image_path+"/combined_poison_consumed.png")

# Graph complexity
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Graph complexity", ylabel="# Nodes", xlabel="Trained episodes")

for d in data:
    ax.plot(d.loc[:,'Trained episodes'].values, 
            d.loc[:,'Number of nodes (mean)'].values, 
            marker='.')

plt.legend(legend, loc=legend_location)
if image_path:
    fig.savefig(image_path+"/combined_graph_complexity.png")

if not image_path:
    plt.show()