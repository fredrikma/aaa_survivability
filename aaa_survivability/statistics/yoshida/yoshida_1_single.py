import numpy as np
import pandas as pd
import sys

import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')

# Displays results from Yoshida 1 scenario

data_directory = "./data/2018_03_20_04_47_24/"
image_path = None
legend_location = 2

method = "AnimatBaseline"
# AnimatBaseline
# Animat
# RandomAgent
# AnimatIncrementalQLearner

if len(sys.argv) >= 2:
    data_directory = sys.argv[1]

if len(sys.argv) >= 3:
    image_path = sys.argv[2]

if len(sys.argv) >= 4:
    method = sys.argv[3]

filename = "scenario_1_" + method + ".csv"
post_title = " (" + method + ")"

# load pandas
data = pd.read_csv(data_directory + filename)

# Episode length
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Episode length" + post_title, ylabel="Episodes", xlabel="Trained episodes")
ax.errorbar(data.loc[:,'Trained episodes'].values, 
            data.loc[:,'Episode length (mean)'].values, 
            data.loc[:,'Episode length (std)'].values, marker='.')
if image_path:
    fig.savefig(image_path + method + "_episode_length.png")

# Energy level
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Energy level" + post_title, ylabel="Energy", xlabel="Trained episodes")
ax.errorbar(data.loc[:,'Trained episodes'].values, 
            data.loc[:,'Energy level (mean)'].values, 
            data.loc[:,'Energy level (std)'].values, marker='.')
if image_path:
    fig.savefig(image_path + method + "_energy_level.png")

# Need
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Need" + post_title, ylabel="Need", xlabel="Trained episodes")
ax.errorbar(data.loc[:,'Trained episodes'].values, 
            data.loc[:,'Need (mean)'].values, 
            data.loc[:,'Need (std)'].values, marker='.')
if image_path:
    fig.savefig(image_path + method + "_need.png")

# Food consumed
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Food consumed" + post_title, ylabel="Food", xlabel="Trained episodes")
ax.errorbar(data.loc[:,'Trained episodes'].values, 
            data.loc[:,'Food consumed (mean)'].values, 
            data.loc[:,'Food consumed (std)'].values, marker='.')
if image_path:
    fig.savefig(image_path + method + "_food_consumed.png")

# Poison consumed
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Poison consumed" + post_title, ylabel="Poison", xlabel="Trained episodes")
ax.errorbar(data.loc[:,'Trained episodes'].values, 
            data.loc[:,'Poison consumed (mean)'].values, 
            data.loc[:,'Poison consumed (std)'].values, marker='.')
if image_path:
    fig.savefig(image_path + method + "_poison_consumed.png")

# Graph complexity
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set(title="Graph complexity" + post_title, ylabel="# Nodes", xlabel="Trained episodes")
ax.errorbar(data.loc[:,'Trained episodes'].values, 
            data.loc[:,'Number of nodes (mean)'].values, 
            data.loc[:,'Number of nodes (std)'].values, marker='.')
if image_path:
    fig.savefig(image_path + method + "_graph_complexity.png")

if not image_path:
    plt.show()