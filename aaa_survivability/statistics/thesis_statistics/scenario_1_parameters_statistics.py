import numpy as np
import pandas as pd
import sys
import os
import matplotlib.pyplot as plt
from matplotlib import style
from scipy import stats

if __name__ == '__main__':
        
    # Displays results from scenario 1 node formation

    legend_location = 0
    style.use('ggplot')

    #data_directory = "../../data/thesis_results/scenario_1_node_formation/2018_04_21_17_38_21/raw_data/"
    #data_directory = "../../data/thesis_results/scenario_1_parameters/2018_05_15_00_16_01/raw_data/"    
    #image_path = "../../data/thesis_results/scenario_1_node_formation/2018_04_21_17_38_21/images/"
    image_path = None
    
    if len(sys.argv) >= 2:
        data_directory = sys.argv[1]
    if len(sys.argv) >= 3:
        image_path = sys.argv[2]

    filenames = ["config_1.csv",
                 "config_2.csv",                  
                 "config_3.csv",
                 "config_4.csv",
                ]

    config_names = ["R=1, Q(t=0)=mu", "R=sigma, Q(t=0)=mu", "R=1, Q(t=0)=0", "R=sigma, Q(t=0)=0"]

    # load pandas
    data = []
    for filename in filenames:         
        data.append(pd.read_csv(data_directory + filename))
        
    # Time values is always identical for all runs    
    time_variable = 'Trained time steps'        
    time_values = data[0][time_variable].unique()    
    variable_names = []

    # Find all variables, can be different amng configs due to relevant and stable nodes
    tot_stable_relevant_nodes = 0
    for df in data:   
        for x in df.keys():
            if x != time_variable:
                if x not in variable_names:
                    variable_names.append(x)
                    if 'Relevant' in x or 'Stable' in x:
                        tot_stable_relevant_nodes += 1                    
    
    # Add variables to all df that lacks them, filled with zeros    
    num_entries = len(data[0].index)
    for df in data:        
        for variable_name in variable_names:            
            if variable_name not in df.keys():                
                df[variable_name] = np.zeros(num_entries)

    # Calculate mean and std over all agents for each evaluation        
    mean_data = []
    std_data = []            
    for df in data:        
        mean_dict = {}
        std_dict = {}            
        for variable_name in variable_names:
            if variable_name != time_variable:
                mean_dict[variable_name] = []
                std_dict[variable_name] = []                
            for time_value in time_values:
                if variable_name != time_variable:
                    mean = df[df[time_variable]==time_value][variable_name].mean()
                    std = df[df[time_variable]==time_value][variable_name].std()
                    mean_dict[variable_name].append(mean)
                    std_dict[variable_name].append(std)
        mean_data.append(mean_dict)
        std_data.append(std_dict)

    # Calculate A/B testing p-values
    ab_p_values_data = []        
    for variable_name in variable_names:
        if 'Relevant' in variable_name or 'Stable' in variable_name:
            pass
        else:
            ab_p_values_dict = {}    
            for i in range(len(config_names)):
                df_1 = data[i]
                for j in range(i+1, len(config_names)):
                    df_2 = data[j]
                    ab_test_name = config_names[i] + ' vs ' + config_names[j]
                    ab_p_values_dict[ab_test_name] = []
                    for time_value in time_values:
                        a_data = df_1[df_1[time_variable]==time_value][variable_name]
                        b_data = df_2[df_2[time_variable]==time_value][variable_name]
                        # Calculate Welch's t-test
                        p_value = stats.ttest_ind(a_data, b_data, equal_var = False)[1]
                        ab_p_values_dict[ab_test_name].append(p_value)

            ab_p_values_data.append([ab_p_values_dict, variable_name])    
    
    # Plot mean/std
    for variable_name in variable_names:
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        if 'Relevant' in variable_name or 'Stable' in variable_name:
            ylabel = 'Fraction'
            title = variable_name
        else:
            ylabel = variable_name
            title = ''
        ax.set(title=title, ylabel=ylabel, xlabel=time_variable)                
        for i in range(len(config_names)):
            ax.errorbar(time_values, mean_data[i][variable_name], std_data[i][variable_name], 
                marker='.', alpha=0.6, capsize=4, capthick=1.5)            
        
        # None of the statistics we are looking at can be negative
        current_ylim = ax.get_ylim()
        plt.ylim(ymin=max(0, current_ylim[0]))

        # Add legend
        plt.legend(config_names, loc=legend_location, fontsize='small')        

        # Save image
        if image_path:
            fig.savefig(image_path + "/" + variable_name.replace(' ', '_') + ".png")

    # Plot A/B tests
    for i, entry in enumerate(ab_p_values_data):
        ab_p_values_dict = entry[0]
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.set(title='A/B test: ' + entry[1], ylabel='p-value', xlabel=time_variable)                
        legend = []
        for ab_name, ab_p_values in ab_p_values_dict.items():
            legend.append(ab_name)
            ax.plot(time_values, ab_p_values, marker='.')            
        
        # Add dashed line at p=0.05
        ax.axhline(0.05, ls='--', c='black', alpha=0.5)
        legend.append('p=0.05')

        # None of the statistics we are looking at can be negative
        current_ylim = ax.get_ylim()
        plt.ylim(ymin=max(0, current_ylim[0]))

        # Add legend
        plt.legend(legend, loc=legend_location, fontsize='small') 

        # Save image
        if image_path:
            fig.savefig(image_path + "/" + variable_names[i].replace(' ', '_') + "_AB.png")

    
    # Define the colormap
    cmap = plt.get_cmap('brg')
    color_idx = np.linspace(0, 1, tot_stable_relevant_nodes)
    # Plot stable/relevant for each config separatedly        
    for i in range(len(config_names)):                         
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)       
        ax.set(title=config_names[i] + ': stable and relevant nodes ', ylabel='Fraction', xlabel=time_variable)                
        legend_names = []
        j = 0
        for variable_name in variable_names:
            if 'Relevant' in variable_name or 'Stable' in variable_name:
                legend_names.append(variable_name)
                ax.errorbar(time_values, mean_data[i][variable_name], std_data[i][variable_name],                 
                    marker='.', alpha=0.6, capsize=4, capthick=1.5, c=cmap(color_idx[j]))  
                j += 1          
        
        # set ylim       
        plt.ylim(-0.01,1.01) 

        # Shrink current axis by 20%
        #box = ax.get_position()
        #ax.set_position([box.x0, box.y0, box.width * 0.65, box.height])

        # Add legend
        #plt.legend(legend_names, loc='center left', bbox_to_anchor=(1, 0.5), fontsize='small')        

        # Add legend
        plt.legend(legend_names, loc=legend_location, fontsize='small') 

        # Save image
        if image_path:
            fig.savefig(image_path + "/" + 's_r_nodes_' + config_names[i].replace(' ', '_') + ".png")

    if not image_path:
        plt.show()