from aaa_survivability.scenarios.main_app import MainApp
import aaa_survivability.envs.animat.smelly_vision_env
import gym

if __name__ == '__main__':

    config_env = {'Num tiles X' : 20,
                  'Num tiles Y' : 20,
                  'Num food objects' : 250,
                  'Global need decay' : -0.0}

    config_agent = {}

    config = {}
    config['Environment'] = config_env
    config['Agent'] = config_agent
    
    app = MainApp('smelly_vision-v0', config, animat_baseline=True)
    app.mainloop()