import tkinter as tk
import tkinter.ttk
import numpy as np
import gym
import time
import graphviz

import matplotlib
matplotlib.use("TkAgg")

from matplotlib import style
style.use('ggplot')

import matplotlib.pyplot as plt

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
import matplotlib.animation as animation

from aaa_survivability.agents.animat_dqn import AnimatDQN

import aaa_survivability.envs.animat.cat_3x3_env
import aaa_survivability.envs.animat.smelly_vision_env


class MainApp:
    """ The main application 
    """

    STEP_MODE_CONTINOUS = 0
    STEP_MODE_ONE = 1
    
    def __init__(self):
        print("MainApp::__init__")

        self._paused = False
        self._done = False
        self._step_mode = MainApp.STEP_MODE_CONTINOUS
        self._root = tk.Tk()
        self._current_t = 0

        self._setup()

    def _setup(self):
        print("MainApp::setup")

        # Create environment
        config_env = {'Num tiles X' : 3,
                    'Num tiles Y' : 3,
                    'Num food objects' : 3,
                    'Global need decay' : -0.02,
                    'Light Red Green' : 0.25,
                    'Death at NEED=0' : False,
                    #'Noise' : []
                    'Noise' : [0.25 for _ in range(32)] # 32 might be good
                    #'Noise' : [0.5 for _ in range(4)] # 32 might be good
                    }

        config_dqn = {'replay_memory_length': 1000,
                    'replay_batch_size': 32,
                    'replay_num_training_iterations' : 1,
                    'gamma': 0.9,
                    'epsilon': 1.0,
                    'epsilon_min': 0.01,
                    'epsilon_decay': 0.99,
                    'learning_rate': 0.005,
                    'activation': "sigmoid",
                    'tau': 0.1}

        config = {}
        config['Environment'] = config_env
        config['Agent'] = config_dqn

        self._env = gym.make('thesis_1-v0')
        self._env.configure(config['Environment'])
        
        # Initial state
        state, info = self._env.reset()

        # Create agent
        self._agent = AnimatDQN(self._env.action_space, self._env.SENSOR_NAMES, state, info, self._env.ACTION_NAMES, new_parameter_settings=config['Agent'])

        # Create windows
        self._control_window = ControlWindow(app=self, master=self._root)

        w = tk.Toplevel(self._root)
        self._plot_window = PlotWindow(app=self, agent=self._agent, master=w)

        # Hook in on window destruction
        self._root.bind("<Destroy>", self._on_destroy)

    def mainloop(self):
        print("MainApp::mainloop")

        # t==0 initalization
        self._current_t = 0
        state, info = self._env.reset() # double reset since we do it in _setup() also, but shouldn't be a problem
        reward = 0
        done = False

        # Used with STEP_MODE_ONE
        self._current_step = 1

        while not self._done:
            # Update GUI
            self._root.update()

            if self._paused:
                continue

            if self._step_mode == MainApp.STEP_MODE_ONE:
                if self._current_step == 0:
                    self._control_window.on_app_pause()
                    self._current_step = 1
                    continue
                self._current_step -= 1

            # Let agent decide action
            action = self._agent.get_action(state, reward, done, info, self._current_t == 0)

            # Render now so we get the latest state visually also
            self._env.render()

            # Execute action
            state, reward, done, info = self._env.step(action)

            # If the environment is done, pause and let control window remove controls
            if done == True:
                self._paused = True
                self._control_window.on_app_done()
                continue

            #print(state)
            self._current_t += 1

            if self._current_t % 500 == 0:
                self._agent.dump_info()
            
        self._env.close()
            

    def _on_destroy(self, event):
        print("MainApp::_on_destroy")
        self._done = True

    def set_step_mode(self, mode):
        self._step_mode = mode



class ControlWindow(tk.Frame):
    """Window holding and managing the scenario controls."""

    def __init__(self, app, master=None):
        super().__init__(master)

        self._app = app

        self.pack()
        self.create_widgets()

    def create_widgets(self):

        self.master.title("Control")

        # Pause/Continue
        button = tk.Button(self, text="Pause",
                           command=self.on_button_pause)
        button.pack(side="top")
        self.button_pause = button

        # step mode selection
        self.box_step_mode = tk.ttk.Combobox(self)
        self.box_step_mode['values'] = ('Continous', 'One step')
        self.box_step_mode['state'] = 'readonly'
        self.box_step_mode.current(0)
        self.box_step_mode.bind("<<ComboboxSelected>>", self.on_step_mode_selection)
        self.box_step_mode.pack(side="top")

        # Quit
        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self._app._root.destroy)
        self.quit.pack(side="bottom")

    def on_app_pause(self):
        """ MainApp requested pause/continue. """
        print("Control::on_app_pause")
        self.on_button_pause()

    def on_app_done(self):
        """ MainApp signalled done"""
        print("Control::on_app_done")
        self.button_pause['state'] = 'disabled'


    def on_button_pause(self):
        """ User requested pause/continue. """
        print("Control::on_button_pause")
        self._app._paused = not self._app._paused

        if self._app._paused:
            self.button_pause['text'] = 'Continue'
        else:
            self.button_pause['text'] = 'Pause'

    def on_step_mode_selection(self, event):
        value = self.box_step_mode.get()
        if value == 'Continous':
            self._app.set_step_mode(MainApp.STEP_MODE_CONTINOUS)
        elif value == 'One step':
            self._app.set_step_mode(MainApp.STEP_MODE_ONE)

        print("Setting step mode to : {}".format(value))

class PlotWindow(tk.Frame):
    """Window holding all plots related to the scenario."""

    update_interval = 1000  # How often we refresh the graphs

    def __init__(self, app, agent, master=None):
        super().__init__(master)

        self._app = app
        self._agent = agent

        self.master.title("Plot window")
        self.create_widgets()

    def create_widgets(self):
        self.frame = tk.Frame(self.master)
        self.f = plt.figure(figsize=(10, 9), dpi=80)

        # accumulated reward
        self.ax_accum_reward = self.f.add_subplot(2, 1, 1)

        self.ax_accum_reward.set_xlabel('Time')
        self.ax_accum_reward.set_ylabel('Need')
        self._line, = self.ax_accum_reward.plot(0, "r-")

        # windowed accumulated reward
        self.ax_windowed_accum_reward = self.f.add_subplot(2, 2, 4)

        self.ax_windowed_accum_reward.set_xlabel('Time')
        self.ax_windowed_accum_reward.set_ylabel('Need')
        self._windowed_line, = self.ax_windowed_accum_reward.plot(0, "b-")

        # action frequency
        self.ax_actions = self.f.add_subplot(2, 2, 3)
        self.ax_actions.set_xlabel('Action')
        self.ax_actions.set_ylabel('Frequency (%)')

        xpos = np.arange(len(self._agent.action_frequency))
        self._bars = self.ax_actions.bar(
            xpos, [1.0 for x in range(len(xpos))])  # makes y-lim = 1
        self.ax_actions.set_xticks(xpos)
        self.ax_actions.set_xticklabels(self._agent.action_names)

        # Frame and canvas stuff
        self.frame = tk.Frame(self.master)
        self.frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

        self.canvas = FigureCanvasTkAgg(self.f, master=self.frame)
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        # Setup animation interval
        self.ani = animation.FuncAnimation(
            self.f, self._update_graphs, interval=self.update_interval)

        # Initial render, will also start the animation
        self.canvas.show()

        # the toolbar, TODO, try with > 1 figure
        self.toolbar = NavigationToolbar2TkAgg(self.canvas, self.frame)
        self.toolbar.pack()
        self.toolbar.update()

    def _update_graphs(self, i):
        xdata = range(len(self._agent.needs_over_time[0]))
        ydata = self._agent.needs_over_time[0]

        self._line.set_xdata(xdata)
        self._line.set_ydata(ydata)

        self.ax_accum_reward.relim()
        self.ax_accum_reward.autoscale_view()

        # windowed accumulated reward
        curr_t = len(self._agent.needs_over_time[0])
        window = min(100, curr_t)
        curr_t -= window
        xdata = range(curr_t, curr_t+window)
        ydata = self._agent.needs_over_time[0][-window:]
        
        self._windowed_line.set_xdata(xdata)
        self._windowed_line.set_ydata(ydata)

        ymin = min(ydata)
        ymax = max(ydata)
        if ymin > 0:
            y_lower = ymin*0.99
        else:
            y_lower = ymin*1.01
        if ymax > 0:
            y_upper = ymax*1.01
        else:
            y_upper = ymax*0.99

        self.ax_windowed_accum_reward.set_ylim([y_lower, y_upper])
        self.ax_windowed_accum_reward.relim()
        self.ax_windowed_accum_reward.autoscale_view()

        # action frequency, normalize here so we dont have to update ylimits
        num = float(np.sum(self._agent.action_frequency))
        if num > 0:
            for i, b in enumerate(self._bars):
                b.set_height(self._agent.action_frequency[i] / num)


if __name__ == '__main__':
    app = MainApp()
    app.mainloop()
