from aaa_survivability.scenarios.main_app import MainApp
import aaa_survivability.envs.animat.cat_3x3_env

if __name__ == '__main__':
    app = MainApp('cat_3x3-v0', animat_baseline=True)
    app.mainloop()