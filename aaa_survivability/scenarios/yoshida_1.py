from aaa_survivability.scenarios.main_app import MainApp
import aaa_survivability.envs.animat.yoshida_1_env

if __name__ == '__main__':

    config_agent = {'use_probabilistic_merge': False,
                      'probabilistic_merge_prob': 0.01,
                      'use_emotional_merge': False,
                      'use_reward_based_merge': True,
                      'prob_reward_based_merge': 1.0,
                      'reward_based_threshold': 3,
                      'use_stable_node_merge': True,
                      'stable_threshold': 20,
                      'use_average_q': True,
                      'start_alpha': 0.05,
                      'alpha_decay': 1.0,
                      'use_reliability_one': False}

    config = {}
    config['Environment'] = None
    config['Agent'] = config_agent

    app = MainApp('yoshida_1_no_limit-v0', animat_baseline=True, config=config)
    aaa_survivability.envs.animat.yoshida_1_env.Yoshida1Env.USE_PROB_TERMINATION = False
    aaa_survivability.envs.animat.yoshida_1_env.Yoshida1Env.ENERGY_LIMITS = (-1000000,100)
    
    app.mainloop()