from aaa_survivability.scenarios.main_app import MainApp
import aaa_survivability.envs.animat.smelly_vision_env
import gym

if __name__ == '__main__':

    config_env = {'Num tiles X' : 3,
                  'Num tiles Y' : 3,
                  'Num food objects' : 3,
                  'Global need decay' : -0.02,
                  'Light Red Green' : 0.25,
                  'Death at NEED=0' : False,
                  #'Noise': []
                  'Noise' : [0.1 for _ in range(32)] # 32 might be good
                  }

    # config_agent ={'use_probabilistic_merge': True,
    #                   'probabilistic_merge_prob': 0.01,
    #                   'use_emotional_merge': False,
    #                   'use_reward_based_merge': False,
    #                   'prob_reward_based_merge': 1.0,
    #                   'reward_based_threshold': 3,
    #                   'use_stable_node_merge': True,
    #                   'stable_threshold': 20,
    #                   'use_average_q': True,
    #                   'start_alpha': 0.05,
    #                   'alpha_decay': 1}

    # config_agent = {'use_probabilistic_merge': False,
    #                 'probabilistic_merge_prob': 0.01,
    #                   'use_emotional_merge': True,
    #                   'surprise_threshold' : 0.04,
    #                   'use_reward_based_merge': False,
    #                   'use_stable_node_merge': False,
    #                   'use_average_q': True,
    #                   'start_alpha': 0.05,
    #                   'alpha_decay': 1,
    #                   'gamma' : 0.9,
    #                   'use_reliability_one': False}

    config_agent = {'use_probabilistic_merge': False,
                      'probabilistic_merge_prob': 0.01,
                      'use_emotional_merge': False,
                      'use_reward_based_merge': True,
                      'prob_reward_based_merge': 1.0,
                      'reward_based_threshold': 3,
                      'use_stable_node_merge': True,
                      'stable_threshold': 15,
                      'relevant_threshold': 0.95,
                      'use_average_q': True,
                      'start_alpha': 0.05,
                      'gamma' : 0.9,
                      'alpha_decay': 1,
                      'epsilon': 0.9,
                      'epsilon_min': 0.01,
                      'epsilon_decay': 0.99
                      }

    config = {}
    config['Environment'] = config_env
    config['Agent'] = config_agent
    
    app = MainApp('thesis_1-v0', config, animat_baseline=True)
    app.mainloop()