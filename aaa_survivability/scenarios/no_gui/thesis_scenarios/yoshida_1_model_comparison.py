import gym
import numpy as np
import pandas as pd
import sys
import graphviz
import pickle
import time
from multiprocessing import Pool

import aaa_survivability.envs.animat.yoshida_1_env

from aaa_survivability.agents.random_agent import RandomAgent
from aaa_survivability.agents.animat_incremental_q_learner import AnimatIncrementalQLearner
from aaa_survivability.agents.animat.animat_baseline import AnimatBaseline

class Yoshida1Runner:
    def __init__(self, agent, env):
        self.agent = agent
        self.env = env

    def run(self, nTotalTrainingEpisodes, nTrainingEpisodes, nEvaluationEpisodes):
        print("Running...", nTotalTrainingEpisodes, nTrainingEpisodes, nEvaluationEpisodes)

        # statistics
        statistics = {}
        statistics['Trained episodes'] = []
        statistics['Episode length (mean)'] = []
        statistics['Episode length (std)'] = []
        statistics['Energy level (mean)'] = []
        statistics['Energy level (std)'] = []
        statistics['Need (mean)'] = []
        statistics['Need (std)'] = []
        statistics['Food consumed (mean)'] = []
        statistics['Food consumed (std)'] = []
        statistics['Poison consumed (mean)'] = []
        statistics['Poison consumed (std)'] = []
        statistics['Number of nodes (mean)'] = []
        statistics['Number of nodes (std)'] = []

        totalTrainingEpisode = 0
        while totalTrainingEpisode < nTotalTrainingEpisodes:
        # Training phase
            print("Training {}".format(totalTrainingEpisode))
            
            self.agent.learning_enabled = True
            for n in range(nTrainingEpisodes):
                totalTrainingEpisode += 1

                # t=0 init
                t = 0
                state, info = self.env.reset()
                reward = 0
                done = False

                while not done:
                    # Get new action
                    action = self.agent.get_action(state, reward, done, info, t == 0)

                    # Execute action
                    state, reward, done, info = self.env.step(action)

                    t += 1

            # freeze training
            self.agent.learning_enabled = False

        # Evaluation phase
            print("Evaluating")
            
            episode_stats = {}
            episode_stats['Episode length'] = np.zeros(nEvaluationEpisodes)
            episode_stats['Energy level'] = np.zeros(nEvaluationEpisodes)
            episode_stats['Need'] = np.zeros(nEvaluationEpisodes)
            episode_stats['Food consumed'] = np.zeros(nEvaluationEpisodes)
            episode_stats['Poison consumed'] = np.zeros(nEvaluationEpisodes)
            episode_stats['Number of nodes'] = np.zeros(nEvaluationEpisodes)

            for n in range(nEvaluationEpisodes):
                # t=0 init
                t = 0
                state, info = self.env.reset()
                reward = 0
                done = False

                # Track the energy and need over time
                eval_energy = []
                eval_energy.append(info['energy'])

                eval_needs = []
                eval_needs.append(info['needs'][0])

                while not done:
                    # Get new action
                    action = self.agent.get_action(state, reward, done, info, t==0)

                    # Execute action
                    state, reward, done, info = self.env.step(action)

                    eval_energy.append(info['energy'])
                    eval_needs.append(info['needs'][0])

                    t += 1 
                
                episode_stats['Episode length'][n] = t
                episode_stats['Energy level'][n] = np.average(eval_energy)
                episode_stats['Need'][n] = np.average(eval_needs)
                episode_stats['Food consumed'][n] = info['food_eaten']
                episode_stats['Poison consumed'][n] = info['poison_eaten']
                episode_stats['Number of nodes'][n] = self.agent.get_brain_complexity()

            # statistics
            statistics['Trained episodes'].append(totalTrainingEpisode)
            statistics['Episode length (mean)'].append(np.average(episode_stats['Episode length']))
            statistics['Episode length (std)'].append(np.std(episode_stats['Episode length']))
            statistics['Energy level (mean)'].append(np.average(episode_stats['Energy level']))
            statistics['Energy level (std)'].append(np.std(episode_stats['Energy level']))
            statistics['Need (mean)'].append(np.average(episode_stats['Need']))
            statistics['Need (std)'].append(np.std(episode_stats['Need']))
            statistics['Food consumed (mean)'].append(np.average(episode_stats['Food consumed']))
            statistics['Food consumed (std)'].append(np.std(episode_stats['Food consumed']))
            statistics['Poison consumed (mean)'].append(np.average(episode_stats['Poison consumed']))
            statistics['Poison consumed (std)'].append(np.std(episode_stats['Poison consumed']))
            statistics['Number of nodes (mean)'].append(np.average(episode_stats['Number of nodes']))
            statistics['Number of nodes (std)'].append(np.std(episode_stats['Number of nodes']))

        # create a dataframe for our statistics
        df = pd.DataFrame(statistics, columns=['Trained episodes', 
                                                'Episode length (mean)', 'Episode length (std)',
                                                'Energy level (mean)', 'Energy level (std)',
                                                'Need (mean)', 'Need (std)',
                                                'Food consumed (mean)', 'Food consumed (std)',
                                                'Poison consumed (mean)', 'Poison consumed (std)',
                                                'Number of nodes (mean)', 'Number of nodes (std)'])

        return df


if __name__ == '__main__':
    # default config
    nTotalTrainingEpisodes = 150
    nTrainingEpisodes = 50
    nEvaluationEpisodes = 50

    outdir = "./"
    
    # supplied config
    if len(sys.argv) >= 5:
        nTotalTrainingEpisodes = int(sys.argv[1])
        nTrainingEpisodes = int(sys.argv[2])
        nEvaluationEpisodes = int(sys.argv[3])
        outdir = sys.argv[4]

    print("Running with : {}, {}, {}, {}".format(nTotalTrainingEpisodes, nTrainingEpisodes, nEvaluationEpisodes, outdir))

    agent_config_1 = {'use_probabilistic_merge': False,
                      'probabilistic_merge_prob': 0.01,
                      'use_emotional_merge': False,
                      'use_reward_based_merge': True,
                      'prob_reward_based_merge': 1.0,
                      'reward_based_threshold': 3,
                      'use_stable_node_merge': True,
                      'stable_threshold': 20,
                      'use_average_q': True,
                      'start_alpha': 0.05,
                      'alpha_decay': 1}
    # agents to run
    agents = [ "AnimatBaseline", "RandomAgent", "AnimatIncrementalQLearner"]
    agent_configs = [agent_config_1, None, None]

    processConfigs = []
    for i, agent in enumerate(agents):

        csv_filename = outdir + "scenario_1_" + agent + ".csv"
        agent_config = agent_configs[i]

        config = {
            "nTotalTrainingEpisodes" : nTotalTrainingEpisodes,
            "nTrainingEpisodes" : nTrainingEpisodes,
            "nEvaluationEpisodes" : nEvaluationEpisodes,
            "Agent" : agent,
            "Agent configuration": agent_config,
            "CSV filename" : csv_filename,
            "Brain filename" : None
             }

        processConfigs.append(config)


    def f(config):
        # create scenario
        timed_env = gym.make("yoshida_1-v0")
        env = timed_env.env
        state, info = env.reset()

        # agent
        id = config["Agent"]
        constructor = globals()[id]

        # possible config
        if config["Agent configuration"]:
            agent = constructor(env.action_space, env.SENSOR_NAMES,
                                state, info, env.ACTION_NAMES, config["Agent configuration"])
        else:
            agent = constructor(env.action_space, env.SENSOR_NAMES, state, info, env.ACTION_NAMES)

        # run
        runner = Yoshida1Runner(agent, env)
        df = runner.run(config["nTotalTrainingEpisodes"], config["nTrainingEpisodes"], config["nEvaluationEpisodes"])

        # # save the brain if requested
        # brain_filename = config["Brain filename"]
        # if brain_filename:

        #     # pickled brain
        #     with open(brain_filename + ".pickle", 'wb') as file:
        #         # Fix some stuff before saving
        #         agent.learning_enabled = True

        #         pickle.dump(agent, file, pickle.HIGHEST_PROTOCOL)

        #     # png and dot
        #     dot = agent.pg.export_dot(False)
        #     src = graphviz.Source(dot, filename=brain_filename, format="png")
        #     src.render()

        # save data
        filename = config["CSV filename"]
        df.to_csv(filename)

    # process in parallel    
    p = Pool(len(agents))
    p.map(f, processConfigs)