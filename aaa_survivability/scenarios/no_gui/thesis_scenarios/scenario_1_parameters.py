import sys
import numpy as np
import pandas as pd
import gym

from multiprocessing import Pool
from collections import defaultdict

import aaa_survivability.envs.animat.thesis_1_env

from aaa_survivability.agents.animat.animat_baseline import AnimatBaseline

class Scenario1NodeFormationRunner:

    def __init__(self, agent, env, num_evaluations, num_training_steps, num_test_steps, num_test_episodes):

        self.agent = agent
        self.env = env
        self.num_evaluations = num_evaluations
        self.num_training_steps = num_training_steps
        self.num_test_steps = num_test_steps
        self.num_test_episodes = num_test_episodes
        self.added_stable_nodes = set()
        self.added_relevant_nodes = set()

    def set_up_final_data_dict(self):
        data_dict = {}        
        
        data_dict['Trained time steps'] = np.arange(self.num_training_steps, self.num_training_steps + self.num_training_steps*self.num_evaluations, self.num_training_steps, dtype='int')
        data_dict['Need'] = np.empty(self.num_evaluations)
        data_dict['Episode length'] = np.empty(self.num_evaluations)
        data_dict['Green food'] = np.empty(self.num_evaluations)
        data_dict['Red food'] = np.empty(self.num_evaluations)
        data_dict['Number of nodes'] = np.empty(self.num_evaluations)
        data_dict['Stable nodes'] = {}
        data_dict['Relevant nodes'] = {}

        return data_dict

    def set_up_all_episodes_data_dict(self):
        data_dict = {}

        data_dict['Need'] = np.empty(self.num_test_episodes)
        data_dict['Episode length'] = np.empty(self.num_test_episodes)
        data_dict['Green food'] = np.empty(self.num_test_episodes)
        data_dict['Red food'] = np.empty(self.num_test_episodes)

        return data_dict

    def set_up_one_episode_data_dict(self):
        data_dict = {}

        data_dict['Need'] = []

        return data_dict

    def run(self):
        final_data_dict = self.set_up_final_data_dict()
        all_episodes_data_dict = self.set_up_all_episodes_data_dict()
        one_episode_data_dict = self.set_up_one_episode_data_dict()

        for i in range(self.num_evaluations):
            self.agent.learning_enabled = True
            self.env.death_at_need_zero = False
            t = 0
            state, info = self.env.reset()
            reward = 0
            done = False

            # Training
            while not done and t < self.num_training_steps:
                # Get new action
                action = self.agent.get_action(
                    state, reward, done, info, t == 0)

                # Execute action
                state, reward, done, info = self.env.step(action)

                t += 1
            
            # Number of nodes
            final_data_dict['Number of nodes'][i] = self.agent.pg.G.number_of_nodes()
            
            # Stable nodes
            stable_nodes = self.agent.stable_nodes[0].keys()
            if len(stable_nodes) > 0:
                for stable_node in stable_nodes:
                    if stable_node not in self.added_stable_nodes:
                        self.added_stable_nodes.add(stable_node)
                        key_name = 'Stable: '
                        origin = ' AND '.join(self.agent.pg.G.nodes[stable_node]['origin'])
                        key_name += origin
                        final_data_dict['Stable nodes'][key_name] = np.zeros(self.num_evaluations)
                        for idx in range(i, self.num_evaluations):                            
                            final_data_dict['Stable nodes'][key_name][idx] = 1.0

            # Relevant nodes
            relevant_nodes = self.agent.relevant_nodes[0].keys()
            if len(relevant_nodes) > 0:
                for relevant_node in relevant_nodes:
                    if relevant_node not in  self.agent.stable_nodes[0].keys():
                        if relevant_node not in self.added_relevant_nodes:
                            self.added_relevant_nodes.add(relevant_node)
                            key_name = 'Relevant: '
                            origin = ' AND '.join(self.agent.pg.G.nodes[relevant_node]['origin'])
                            key_name += origin
                            final_data_dict['Relevant nodes'][key_name] = np.zeros(self.num_evaluations)
                            for idx in range(i, self.num_evaluations):
                                final_data_dict['Relevant nodes'][key_name][idx] = 1.0
            
            # No learning during test phase
            self.agent.learning_enabled = False
            self.env.death_at_need_zero = True

            # Testing
            for j in range(self.num_test_episodes):
                # Clear one_episode_data_dict each episode
                for statistic in one_episode_data_dict.keys():
                    one_episode_data_dict[statistic].clear()

                # Iterate over all test steps
                t = 0
                state, info = self.env.reset()
                reward = 0
                done = False

                while not done and t < self.num_test_steps:
                    # Get new action
                    action = self.agent.get_action(
                        state, reward, done, info, t == 0)

                    # Execute action
                    state, reward, done, info = self.env.step(action)

                    # Collect step based data
                    one_episode_data_dict['Need'].append(info['needs'][0])                    

                    t += 1

                # Calculate averages over step based data
                for stat_key in one_episode_data_dict.keys():
                    all_episodes_data_dict[stat_key][j] = np.average(
                        one_episode_data_dict[stat_key])

                # Collect episode based data
                all_episodes_data_dict['Episode length'][j] = t
                all_episodes_data_dict['Green food'][j] = info['food_eaten_positive']
                all_episodes_data_dict['Red food'][j] = info['food_eaten_negative']

            # Calculate averages over all episodes
            for stat_key in all_episodes_data_dict.keys():
                final_data_dict[stat_key][i] = np.average(
                    all_episodes_data_dict[stat_key])

        return final_data_dict


if __name__ == '__main__':

    outdir = "./"

    # supplied config
    if len(sys.argv) >= 2:
        outdir = sys.argv[1]    

    # Environment
    environment = "thesis_1-v0"    
    env_config = dict()

    env_config = {'Num tiles X' : 3,
                  'Num tiles Y' : 3,
                  'Num food objects' : 3,
                  'Global need decay' : -0.02,
                  'Light Red Green' : 0.25,
                  'Death at NEED=0' : False,
                  'Noise' : [0.1 for _ in range(10)]
                  }
    
    # Agent configurations
    agent_config_1 = {'use_probabilistic_merge': False,                      
                      'use_emotional_merge': False,
                      'use_reward_based_merge': True,
                      'use_stable_node_merge': True,
                      'use_relevant_node_merge': True,
                      'probabilistic_merge_prob': 0.01,
                      'prob_reward_based_merge': 1.0,
                      'reward_based_threshold': 3,                      
                      'stable_threshold': 15,    
                      'relevant_min_updates': 20,    
                      'relevant_threshold': 0.95,          
                      'start_alpha': 0.05,
                      'gamma' : 0.9,
                      'alpha_decay': 1,
                      'use_average_q': True,
                      'use_reliability_one': True,
                      }
    
    agent_config_2 = {'use_probabilistic_merge': False,                      
                      'use_emotional_merge': False,
                      'use_reward_based_merge': True,
                      'use_stable_node_merge': True,
                      'use_relevant_node_merge': True,
                      'probabilistic_merge_prob': 0.01,
                      'prob_reward_based_merge': 1.0,
                      'reward_based_threshold': 3,   
                      'stable_threshold': 15,    
                      'relevant_min_updates': 20,    
                      'relevant_threshold': 0.95,             
                      'start_alpha': 0.05,
                      'gamma' : 0.9,
                      'alpha_decay': 1,
                      'use_average_q': True,
                      'use_reliability_one': False,
                      }
    
    agent_config_3 = {'use_probabilistic_merge': False,                      
                      'use_emotional_merge': False,
                      'use_reward_based_merge': True,
                      'use_stable_node_merge': True,
                      'use_relevant_node_merge': True,
                      'probabilistic_merge_prob': 0.01,
                      'prob_reward_based_merge': 1.0,
                      'reward_based_threshold': 3,   
                      'stable_threshold': 15,    
                      'relevant_min_updates': 20,    
                      'relevant_threshold': 0.95,  
                      'start_alpha': 0.05,
                      'gamma' : 0.9,
                      'alpha_decay': 1,
                      'use_average_q': False,
                      'use_reliability_one': True,
                      }

    agent_config_4 = {'use_probabilistic_merge': False,                      
                      'use_emotional_merge': False,
                      'use_reward_based_merge': True,
                      'use_stable_node_merge': True,
                      'use_relevant_node_merge': True,
                      'probabilistic_merge_prob': 0.01,
                      'prob_reward_based_merge': 1.0,
                      'reward_based_threshold': 3,                      
                      'stable_threshold': 15,    
                      'relevant_min_updates': 20,    
                      'relevant_threshold': 0.95,            
                      'start_alpha': 0.05,
                      'gamma' : 0.9,
                      'alpha_decay': 1,
                      'use_average_q': False,
                      'use_reliability_one': False,
                      }

    agent_configs = [agent_config_1, agent_config_2, agent_config_3, agent_config_4]
    config_names = ['config_1', 'config_2', 'config_3', 'config_4']

    # Statistic settings
    num_agents = 200
    num_training_steps = 200
    num_test_steps = 100
    num_test_episodes = 20
    num_evaluations = 50

    def f(config):
        # Create environment
        env = gym.make(config['environment'])
        env.configure(config['env_config'])
        state, info = env.reset()

        # Create agent
        agent = AnimatBaseline(env.action_space, env.SENSOR_NAMES,
                               state, info, env.ACTION_NAMES, config['agent_configuration'])

        # Collect data
        runner = Scenario1NodeFormationRunner(agent, env, config['num_evaluations'], config['num_training_steps'], config['num_test_steps'], config['num_test_episodes'])
        data_dict = runner.run()

        return data_dict

    for i, agent_config in enumerate(agent_configs):
        csv_filename = outdir + config_names[i] + ".csv"

        process_configs = []
        for j in range(num_agents):            
            config = {
                'agent_configuration': agent_config,
                'environment': environment,
                'env_config': env_config,
                'num_training_steps': num_training_steps,
                'num_test_steps': num_test_steps,
                'num_test_episodes': num_test_episodes,
                'num_evaluations': num_evaluations
            }

            process_configs.append(config)

        # Generate data with parallel processes
        with Pool(maxtasksperchild=1) as p:
            data_dicts = p.map(f, process_configs, chunksize=1)

        # Combine all dicts to one dict and collect all stable and relevant nodes        
        combined_data_dict = defaultdict(list)
        for d in data_dicts:            
            for key, values in d.items():       
                if key == 'Stable nodes':                                        
                    for stable_node_key, stable_node_values in values.items():                        
                        for stable_node_value in stable_node_values:
                            combined_data_dict[stable_node_key].append(stable_node_value)                        
                elif key == 'Relevant nodes':
                    for relevant_node_key, relevant_node_values in values.items():
                        for relevant_node_value in relevant_node_values:
                            combined_data_dict[relevant_node_key].append(relevant_node_value)
                else:
                    for value in values:                    
                        combined_data_dict[key].append(value)

        # Fill stable/relevant nodes that were not represented by all experiments        
        for key, values in combined_data_dict.items():            
            #print(len(combined_data_dict[key]), key)
            if len(values) != num_evaluations*num_agents:
                for i in range(len(values), num_evaluations*num_agents):
                    combined_data_dict[key].append(0)
            #print(len(combined_data_dict[key]), key)

        # Create df and save to csv
        df = pd.DataFrame.from_dict(combined_data_dict)
        df.to_csv(csv_filename, index=False)
