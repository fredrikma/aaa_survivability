import tkinter as tk
import tkinter.ttk
from tkinter.filedialog import askopenfilename, asksaveasfilename
import numpy as np
import gym
import time
import graphviz
import pickle

import matplotlib
matplotlib.use("TkAgg")

from matplotlib import style
style.use('ggplot')

import matplotlib.pyplot as plt

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
import matplotlib.animation as animation

from aaa_survivability.agents.animat.animat import Animat
from aaa_survivability.agents.animat.animat_baseline import AnimatBaseline


class MainApp:
    """ The main application 

    Written for animat compatible environments.
    Using the Generic Animat model from aaa_survivability.agents.animat.animat.Animat."""

    STEP_MODE_CONTINOUS = 0
    STEP_MODE_ONE = 1

    def __init__(self, scenario, config={'Environment' : {}, 'Agent' : {}}, animat_baseline=False):
        print("MainApp::__init__")

        self.scenario = scenario
        self.config = config
        self._animat_baseline = animat_baseline
        self._paused = False
        self._done = False
        self._step_mode = MainApp.STEP_MODE_CONTINOUS
        self._root = tk.Tk()
        self._current_t = 0

        self._setup()

    def _setup(self):
        print("MainApp::setup")

        # Create and configure the environment
        self._env = gym.make(self.scenario)
        self._env.configure(self.config['Environment'])

        # Initial state
        state, info = self._env.reset()

        # Create agent
        if self._animat_baseline:
            self._agent = AnimatBaseline(
                self._env.action_space, self._env.SENSOR_NAMES, state, info, self._env.ACTION_NAMES, new_parameter_settings = self.config['Agent'])
        else:
            self._agent = Animat(
                self._env.action_space, self._env.SENSOR_NAMES, state, info, self._env.ACTION_NAMES)

        # Create windows
        self._control_window = ControlWindow(app=self, master=self._root)

        w1 = tk.Toplevel(self._root)
        self._info_window = InfoWindow(app=self, master=w1)

        w2 = tk.Toplevel(self._root)
        self._plot_window = PlotWindow(app=self, master=w2)

    def mainloop(self):
        print("MainApp::mainloop")

        # t==0 initalization, most stuff needs to be self. since we support loading of animats and we need a way to reset
        self._current_t = 0

        # double reset since we do it in _setup() also, but shouldn't be a problem
        self.state, self.info = self._env.reset()
        self.reward = 0
        done = False

        # Used with STEP_MODE_ONE
        self._current_step = 1

        while not self._done:
            # Update GUI
            self._root.update()

            if self._paused:
                continue

            if self._step_mode == MainApp.STEP_MODE_ONE:
                if self._current_step == 0:
                    self._control_window.on_app_pause()
                    self._current_step = 1
                    continue
                self._current_step -= 1

            # Get new action
            action = self._agent.get_action(
                self.state, self.reward, done, self.info, self._current_t == 0)

            # Execute action
            self.state, self.reward, done, self.info = self._env.step(action)

            # Render now so we get the latet state visually also
            self._env.render()

            # Reset override action
            self.override_action()

            # If the environment is done, pause and let control window remove controls
            if done == True:
                self._paused = True
                self._control_window.on_app_done()
                continue

            # print(state)
            self._current_t += 1
            #time.sleep(1/30)

        self._env.close()

    def save_animat(self, filename):
        print("Saving animat to {}".format(filename))
        with open(filename, 'wb') as file:
            pickle.dump(self._agent, file, pickle.HIGHEST_PROTOCOL)

    def load_animat(self, filename):
        print("Loading animat from {}".format(filename))
        with open(filename, 'rb') as file:
            self._agent = pickle.load(file)

            # need to reset the agent needs
            self._agent.reset_statistics()
            self._agent.reset_needs()

            # need to reset environment
            self.state, self.info = self._env.reset()
            self.reward = 0

    def quit(self):
        print("MainApp::quit")
        self._done = True

    def plot_pg(self, is_active):
        if is_active:
            self._agent.pg.propagate_activity(self.state)
        dot = self._agent.pg.export_dot(is_active)
        src = graphviz.Source(dot)
        src.view(cleanup=True)

    def dump_info(self):
        print("---Animat info---")
        print(self._agent.dump_info())

    def set_step_mode(self, mode):
        self._step_mode = mode

    def override_action(self, action=None):
        self.info['override_action'] = action


class ControlWindow(tk.Frame):
    """Window holding and managing the scenario controls."""

    def __init__(self, app, master=None):
        super().__init__(master)

        self._app = app

        self.create_widgets()

    def create_widgets(self):

        self.master.title("Control")

        # Button layout
        button_width = 10
        button_padx = 5
        button_pady = 1
        button_font_size = [False, 10]
        frame_font_size = [False, 10, 'bold']

        # Pause/Continue
        button = tk.Button(self.master, text="Pause", width=button_width, font=button_font_size,
                           command=self.on_button_pause)
        button.grid(column=0, padx=button_padx, pady=button_pady)
        self.button_pause = button

        # step mode selection
        self.box_step_mode = tk.ttk.Combobox(
            self.master, width=button_width, font=button_font_size)
        self.box_step_mode['values'] = ('Continous', 'One step')
        self.box_step_mode['state'] = 'readonly'
        self.box_step_mode.current(0)
        self.box_step_mode.bind("<<ComboboxSelected>>",
                                self.on_step_mode_selection)
        self.box_step_mode.grid(column=0, padx=button_padx, pady=button_pady)

        # plot perception graph
        button = tk.Button(self.master, text="Plot pg", width=button_width, font=button_font_size,
                           command=lambda: self._app.plot_pg(False))
        button.grid(column=0, padx=button_padx, pady=button_pady)
        button = tk.Button(self.master, text="Plot pg (active)", width=button_width, font=button_font_size,
                           command=lambda: self._app.plot_pg(True))
        button.grid(column=0, padx=button_padx, pady=button_pady)

        # dump debug info
        button = tk.Button(self.master, text="Dump info", width=button_width, font=button_font_size,
                           command=lambda: self._app.dump_info())
        button.grid(column=0, padx=button_padx, pady=button_pady)

        # Save animat
        button = tk.Button(self.master, text="Save animat...", width=button_width, font=button_font_size,
                           command=self.on_button_save_animat)
        button['state'] = 'disabled'
        button.grid(column=0, padx=button_padx, pady=button_pady)
        self.button_save_animat = button

        # Load animat
        button = tk.Button(self.master, text="Load animat...", width=button_width, font=button_font_size,
                           command=self.on_button_load_animat)
        button['state'] = 'disabled'
        button.grid(column=0, padx=button_padx, pady=button_pady)
        self.button_load_animat = button

        # Quit
        self.quit = tk.Button(self.master, text="QUIT", fg="red", width=button_width, font=button_font_size,
                              command=self._app.quit)
        self.quit.grid(column=0, padx=button_padx, pady=button_pady)

        # Override actions
        override_frame = tk.LabelFrame(
            self.master, text='Override action', font=frame_font_size)
        override_frame.grid(
            row=0, column=1, rowspan=self._app._agent.action_space.n+1)
        self.override_action_buttons = []
        for i, action in enumerate(self._app._agent.action_names):
            button = tk.Button(override_frame, text=action, width=button_width, font=button_font_size,
                               command=lambda i=i: self._app.override_action(i))
            button['state'] = 'disabled'
            button.grid(row=i, column=1, padx=button_padx, pady=button_pady)
            self.override_action_buttons.append(button)

    def on_app_pause(self):
        """ MainApp requested pause/continue. """
        print("Control::on_app_pause")
        self.on_button_pause()

    def on_app_done(self):
        """ MainApp signalled done"""
        print("Control::on_app_done")
        self.button_pause['state'] = 'disabled'

    def on_button_pause(self):
        """ User requested pause/continue. """
        print("Control::on_button_pause")
        self._app._paused = not self._app._paused

        if self._app._paused:
            self.button_pause['text'] = 'Continue'
            self.button_load_animat['state'] = 'normal'
            self.button_save_animat['state'] = 'normal'
            for i in range(self._app._agent.action_space.n):
                self.override_action_buttons[i]['state'] = 'normal'
        else:
            self.button_pause['text'] = 'Pause'
            self.button_load_animat['state'] = 'disabled'
            self.button_save_animat['state'] = 'disabled'
            for i in range(self._app._agent.action_space.n):
                self.override_action_buttons[i]['state'] = 'disabled'

    def on_step_mode_selection(self, event):
        value = self.box_step_mode.get()
        if value == 'Continous':
            self._app.set_step_mode(MainApp.STEP_MODE_CONTINOUS)
        elif value == 'One step':
            self._app.set_step_mode(MainApp.STEP_MODE_ONE)

        print("Setting step mode to : {}".format(value))

    def on_button_save_animat(self):
        filename = asksaveasfilename(filetypes=(
            ("Pickle", "*.pickle"), ("All Files", "*.*")), title="Save as")
        if filename == '':
            return

        self._app.save_animat(filename)

    def on_button_load_animat(self):
        filename = askopenfilename(filetypes=(
            ("Pickle", "*.pickle"), ("All Files", "*.*")), title="Choose a file.")
        if filename == '':
            return

        self._app.load_animat(filename)


class PlotWindow(tk.Frame):
    """Window holding all plots related to the scenario."""

    update_interval = 1000  # How often we refresh the graphs

    def __init__(self, app, master=None):
        super().__init__(master)

        self._app = app

        self.master.title("Plot window")
        self.create_widgets()

    def create_widgets(self):
        self.frame = tk.Frame(self.master)
        self.f = plt.figure(figsize=(10, 9), dpi=80)

        # accumulated reward
        self.ax_accum_reward = self.f.add_subplot(2, 1, 1)

        self.ax_accum_reward.set_xlabel('Time')
        self.ax_accum_reward.set_ylabel('Need')
        self._line, = self.ax_accum_reward.plot(0, "r-")

        # windowed accumulated reward
        self.ax_windowed_accum_reward = self.f.add_subplot(2, 2, 4)

        self.ax_windowed_accum_reward.set_xlabel('Time')
        self.ax_windowed_accum_reward.set_ylabel('Need')
        self._windowed_line, = self.ax_windowed_accum_reward.plot(0, "b-")

        # action frequency
        self.ax_actions = self.f.add_subplot(2, 2, 3)
        self.ax_actions.set_xlabel('Action')
        self.ax_actions.set_ylabel('Frequency (%)')

        xpos = np.arange(len(self._app._agent.action_frequency))
        self._bars = self.ax_actions.bar(
            xpos, [1.0 for x in range(len(xpos))])  # makes y-lim = 1
        self.ax_actions.set_xticks(xpos)
        self.ax_actions.set_xticklabels(self._app._agent.action_names)

        # Frame and canvas stuff
        self.frame = tk.Frame(self.master)
        self.frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

        self.canvas = FigureCanvasTkAgg(self.f, master=self.frame)
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        # Setup animation interval
        self.ani = animation.FuncAnimation(
            self.f, self._update_graphs, interval=self.update_interval)

        # Initial render, will also start the animation
        self.canvas.show()

        # the toolbar, TODO, try with > 1 figure
        self.toolbar = NavigationToolbar2TkAgg(self.canvas, self.frame)
        self.toolbar.pack()
        self.toolbar.update()

    def _update_graphs(self, i):
        xdata = range(len(self._app._agent.needs_over_time[0]))
        ydata = self._app._agent.needs_over_time[0]

        self._line.set_xdata(xdata)
        self._line.set_ydata(ydata)

        self.ax_accum_reward.relim()
        self.ax_accum_reward.autoscale_view()

        # windowed accumulated reward
        curr_t = len(self._app._agent.needs_over_time[0])
        window = min(100, curr_t)
        curr_t -= window
        xdata = range(curr_t, curr_t+window)
        ydata = self._app._agent.needs_over_time[0][-window:]

        self._windowed_line.set_xdata(xdata)
        self._windowed_line.set_ydata(ydata)

        ymin = min(ydata)
        ymax = max(ydata)
        if ymin > 0:
            y_lower = ymin*0.99
        else:
            y_lower = ymin*1.01
        if ymax > 0:
            y_upper = ymax*1.01
        else:
            y_upper = ymax*0.99

        self.ax_windowed_accum_reward.set_ylim([y_lower, y_upper])
        self.ax_windowed_accum_reward.relim()
        self.ax_windowed_accum_reward.autoscale_view()

        # action frequency, normalize here so we dont have to update ylimits
        num = float(np.sum(self._app._agent.action_frequency))
        if num > 0:
            for i, b in enumerate(self._bars):
                b.set_height(self._app._agent.action_frequency[i] / num)

class InfoWindow(tk.Frame):
    """Window showing the scenario parameters and statistics.
       Gets data to display from the Agent"""

    update_interval = 200  # How often we refresh the counters

    def __init__(self, app, master=None):
        super().__init__(master)

        self._app = app
        self.create_widgets()

    def create_widgets(self):
        self.master.title("Information")

        # category
        frame_padx = 5
        frame_pady = 5
        title_font = [False, 10, 'bold']
        
        # entry name
        label_font = [False, 10]
        label_sticky = 'E'
        widget_padx = 1
        widget_pady = 1

        # entry value        
        entry_font = [False, 10]
        entry_sticky = 'W'
        entry_width = 10
        constant_color = 'red'
        variable_color = 'green'

        info = self._app._agent.get_information()

        current_row = 0
        current_column = 0
        extra_space = 2

        max_rows_per_column = 20

        # variable information, category/entries
        self.text_variables = {}
        
        for category in info.keys():

            num_entries = len(info[category].keys())

            print("Creating category : {}, with {} entries".format(category, num_entries))
            frame = tk.LabelFrame(self.master, padx=frame_padx, pady=frame_pady, text=category, font=title_font)

            label_row = 0
            for entry in info[category].keys():
                print("--Entry {} : {}".format(entry, info[category][entry]))

                # entry name
                label = tk.Label(frame, text=entry, font=label_font)
                label.grid(row=label_row, column=0, sticky=label_sticky, padx=widget_padx, pady=widget_pady)

                # entry value
                value_str = info[category][entry][0]

                if info[category][entry][1] == 'variable':

                    # If it is variable we need to allocate and store a reference to it
                    value_text_variable = tk.StringVar()
                    value_text_variable.set(value_str)

                    # first allocation
                    if not category in self.text_variables:
                        self.text_variables[category] = {}

                    # add the variable entry for this category
                    self.text_variables[category][entry] = value_text_variable

                    value = tk.Entry(frame, width=entry_width, fg=variable_color, font=entry_font, textvariable=value_text_variable)

                else:
                    value = tk.Entry(frame, width=entry_width, fg=constant_color, font=entry_font)
                    value.insert(0, value_str)

                value.configure(state='readonly')
                value.grid(row=label_row, column=1, sticky=entry_sticky, padx=widget_padx, pady=widget_pady)

                label_row += 1
            
            # place the frame
            rowspan = num_entries + extra_space
            frame.grid(row=current_row, rowspan=rowspan, column=current_column, sticky='E')

            current_row += rowspan

            # move to next column if needed
            if current_row >= max_rows_per_column:
                current_row = 0
                current_column += 1


        self._update_info()

    def _update_info(self):
        """ Updates the variable information """

        var_info = self._app._agent.get_variable_information()

        for category, entries in self.text_variables.items():
            for entry, text_variable in entries.items():
                value = var_info[category][entry]
                text_variable.set(value)

        self.master.update_idletasks()
        self.master.after(self.update_interval, self._update_info)