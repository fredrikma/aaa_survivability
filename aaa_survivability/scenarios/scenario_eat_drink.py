from aaa_survivability.scenarios.main_app import MainApp
import aaa_survivability.envs.animat.eat_drink_env

if __name__ == '__main__':
    app = MainApp('eat_drink-v0', animat_baseline=True)
    app.mainloop()