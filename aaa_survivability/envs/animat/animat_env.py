import gym

class AnimatEnv(gym.Env):
    """ Base class for animat based environments. 
    
    This kind of environment has to return the
    - needs
    - rewards
    
    in the info dictionary during step.

    {"needs": self.body.needs, "rewards": [reward]} 
    
    It also has a default configure method to give the user an option to control environment parameters. """
    def configure(self, config):
        pass
    