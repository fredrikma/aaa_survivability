import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
import time
from os import path
import pyglet

from aaa_survivability.envs.animat.animat_env import AnimatEnv
from aaa_survivability.envs.animat.renderer import Renderer

class Cat3x3Env(AnimatEnv):
    """Tilebased 3x3 discrete environment.

    Actions:
        0 = Up
        1 = Down
        2 = Left
        3 = Right
        4 = Eat

    Sensors:
        current tile for food and poison 
        non-interactable sensor (not food and not poison), can be seen as a kind of proximit sensor
        von-neuman dir for food
        1+1+1+4=7
    """
    ACTION_NAMES = ["Up", "Down", "Left", "Right", "Eat"]
    NUM_ACTIONS = len(ACTION_NAMES)

    SENSOR_NAMES = ["FOOD", "POISON", "NO interaction",
                    "Dir-up FOOD", "Dir-down FOOD", "Dir-left FOOD", "Dir-right FOOD"]

    NUM_SENSORS = len(SENSOR_NAMES)

    REWARD_FOOD = 0.5
    REWARD_POISON = -0.2
    REWARD_EAT_EMPTY = -0.01
    REWARD_MOVE = -0.01
    NEED_GLOBAL_DECAY = 0
    CAP_NEED = True

    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 4
    }

    # movement actions
    actions_move = {0: [0, 1],
                    1: [0, -1],
                    2: [-1, 0],
                    3: [1, 0]}

    num_tiles_x = 3
    num_tiles_y = 3
    screen_width = 600
    screen_height = 600

    tile_width = screen_width / num_tiles_x
    tile_height = screen_height / num_tiles_y

    class TileObject(object):
        def __init__(self, sprite, x=0, y=0):
            self.sprite = sprite
            self.tile_pos = np.array([x, y], dtype=np.int64)

        @property
        def tile_pos(self):
            return self.__tile_pos

        @tile_pos.setter
        def tile_pos(self, p):
            self.__tile_pos = p
            self.sprite.position = (
                self.tile_pos[0] * Cat3x3Env.tile_width, self.tile_pos[1] * Cat3x3Env.tile_height)

    class Body(TileObject):
        def __init__(self, sprite, x=0, y=0):
            super().__init__(sprite, x, y)

            self.reset()

        def reset(self):
            self.needs = [1.0]
            self.reset_sensors()

        def reset_sensors(self):
            self.sensors = [0 for x in range(Cat3x3Env.NUM_SENSORS)]

    class Food(TileObject):
        def __init__(self, sprite, x=0, y=0):
            super().__init__(sprite, x, y)

    class Poison(TileObject):
        def __init__(self, sprite, x=0, y=0):
            super().__init__(sprite, x, y)

    def __init__(self):
        self.action_space = spaces.Discrete(Cat3x3Env.NUM_ACTIONS)
        self.observation_space = spaces.Discrete(Cat3x3Env.NUM_SENSORS)

        self.viewer = None
        self.done = False

    # sprites
        # background
        fname = path.join(path.dirname(__file__),
                          "assets/green_tile.png")
        image = pyglet.image.load(fname)
        scale_x = Cat3x3Env.tile_width / image.width
        scale_y = Cat3x3Env.tile_height / image.height

        self.sprite_batch_background = pyglet.graphics.Batch()
        self.sprite_background = []

        pos_y = 0
        for _ in range(Cat3x3Env.num_tiles_y):
            pos_x = 0
            for _ in range(Cat3x3Env.num_tiles_x):
                self.sprite_background.append(pyglet.sprite.Sprite(
                    image, pos_x, pos_y, subpixel=True, batch=self.sprite_batch_background))
                self.sprite_background[-1].scale_x = scale_x
                self.sprite_background[-1].scale_y = scale_y

                pos_x += Cat3x3Env.tile_width
            pos_y += Cat3x3Env.tile_height

        # cat
        fname = path.join(path.dirname(__file__), "assets/cat.png")
        image = pyglet.image.load(fname)
        scale_x = Cat3x3Env.tile_width / image.width
        scale_y = Cat3x3Env.tile_height / image.height

        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        self.body = Cat3x3Env.Body(sprite, 0, 0)

        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        # food
        fname = path.join(path.dirname(__file__), "assets/fish.png")
        image = pyglet.image.load(fname)
        scale_x = Cat3x3Env.tile_width / image.width
        scale_y = Cat3x3Env.tile_height / image.height

        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        self.food = Cat3x3Env.Food(sprite, 2, 1)

        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        # poison
        fname = path.join(path.dirname(__file__), "assets/frog.png")
        image = pyglet.image.load(fname)
        scale_x = Cat3x3Env.tile_width / image.width
        scale_y = Cat3x3Env.tile_height / image.height

        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        self.poison = Cat3x3Env.Poison(sprite, 2, 2)

        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        self.seed()
        self.reset()

    def _get_initial_state(self):
        """Get the initial state."""

        self._update_sensors()
        return (self.body.sensors, {"needs": self.body.needs, "rewards": [0]})

    def _limit_coordinates(self, coord):
        coord[0] = min(coord[0], Cat3x3Env.num_tiles_x - 1)
        coord[0] = max(coord[0], 0)
        coord[1] = min(coord[1], Cat3x3Env.num_tiles_y - 1)
        coord[1] = max(coord[1], 0)
        return coord

    def _action_move(self, action):
        diff = Cat3x3Env.actions_move[action]
        tmp = self.body.tile_pos + diff
        self.body.tile_pos = self._limit_coordinates(tmp)

        self.body.needs[0] += Cat3x3Env.REWARD_MOVE

    def _action_eat(self):
        if (self.body.tile_pos == self.food.tile_pos).all():
            #print("Eating food")
            self.body.needs[0] += Cat3x3Env.REWARD_FOOD

            # respawn food at a free tile (one where there is no poison)
            self.food.tile_pos = self._get_random_tile([self.poison.tile_pos])

        elif (self.body.tile_pos == self.poison.tile_pos).all():
            #print("Eating poison")
            self.body.needs[0] += Cat3x3Env.REWARD_POISON

            # respawn poison at a free tile (one where there is no feed)
            self.poison.tile_pos = self._get_random_tile([self.food.tile_pos])
        else:
            self.body.needs[0] += Cat3x3Env.REWARD_EAT_EMPTY

        # cap the need
        if Cat3x3Env.CAP_NEED and self.body.needs[0] > 1:
            self.body.needs[0] = 1

    def _get_random_tile(self, excluded=[]):
        while True:
            rx = np.random.randint(Cat3x3Env.num_tiles_x)
            ry = np.random.randint(Cat3x3Env.num_tiles_y)

            invalid = False
            for x in excluded:
                if rx == x[0] and ry == x[1]:
                    invalid = True
                    break
            if not invalid:
                break

        return np.array([rx, ry], dtype=np.int64)

    def seed(self, seed=None):
        seed = seeding.np_random(seed)
        return [seed]

    def _update_sensors(self):
        """"Sense the environment."""

        # current tile
        if (self.food.tile_pos == self.body.tile_pos).all():
            self.body.sensors[0] = 1
        if (self.poison.tile_pos == self.body.tile_pos).all():
            self.body.sensors[1] = 1

        # no interaction sensor, if we are at an "empty" tile set this
        if sum(self.body.sensors) == 0:
            self.body.sensors[2] = 1

        # far sight food
        if self.food.tile_pos[1] > self.body.tile_pos[1]:  # up
            self.body.sensors[3] = 1
        if self.food.tile_pos[1] < self.body.tile_pos[1]:  # down
            self.body.sensors[4] = 1
        if self.food.tile_pos[0] < self.body.tile_pos[0]:  # left
            self.body.sensors[5] = 1
        if self.food.tile_pos[0] > self.body.tile_pos[0]:  # right
            self.body.sensors[6] = 1
    def step(self, action):

        reward = self.body.needs[0]

        if action == 0 or action == 1 or action == 2 or action == 3:
            self._action_move(action)
        elif action == 4:
            self._action_eat()

        # sensors
        self.body.reset_sensors()
        self._update_sensors()
        
        state = list(self.body.sensors)

        # reward, decoupled from need
        reward = self.body.needs[0] - reward

        # loose energy at each timestep
        self.body.needs[0] += Cat3x3Env.NEED_GLOBAL_DECAY

        return (state, None, self.done, {"needs": self.body.needs,
                                         "rewards": [reward]})

    def reset(self):

        self.body.reset()

        # randomize starting positions
        self.body.tile_pos = self._get_random_tile()
        self.food.tile_pos = self._get_random_tile()
        self.poison.tile_pos = self._get_random_tile([self.food.tile_pos])

        # return the initial state
        return self._get_initial_state()

    def _get_viewer(self):

        if self.viewer is None:
            self.viewer = Renderer(
                Cat3x3Env.screen_width, Cat3x3Env.screen_height)

            self.viewer.add_drawable(self.sprite_batch_background)
            self.viewer.add_drawable(self.food.sprite)
            self.viewer.add_drawable(self.poison.sprite)
            self.viewer.add_drawable(self.body.sprite)

        return self.viewer

    def render(self, mode='human', close=False):

        if close:
            self.done = True
            return

        viewer = self._get_viewer()

        if mode == 'human':
            self.done = not viewer.render()
        elif mode == 'rgb_array':
            return viewer.render(True)
        else:
            raise NotImplementedError

    def close(self):
        print("render::close")
        if self.viewer:
            self.viewer.close()


if __name__ == '__main__':
    import aaa_survivability.envs.animat.cat_3x3_env

    env = gym.make('cat_3x3-v0')
    env.reset()

    #env._max_episode_steps = 100

    done = False
    while not done:
        action = env.action_space.sample()
        state, reward, done, info = env.step(action)

        print(info["needs"], state)

        env.render()
        time.sleep(1/10)

    env.close()
    print("Finished")
