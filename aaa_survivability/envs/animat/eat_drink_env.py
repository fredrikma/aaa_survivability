import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
import time
from os import path
import pyglet

from aaa_survivability.envs.animat.animat_env import AnimatEnv
from aaa_survivability.envs.animat.renderer import Renderer

class EatDrinkEnv(AnimatEnv):
    """ Extremely simple animat environment.

        One tile, two actions (eat, drink)

        One sensor always active
        - positive reward for eat
        - negative reward for drink

    Actions:
        0 = Eat
        1 = Drink

    Sensors:
        current tile, always active
    """
    ACTION_NAMES = ["Eat", "Drink"]
    NUM_ACTIONS = len(ACTION_NAMES)

    SENSOR_NAMES = ["Sensor 1"]
    NUM_SENSORS = len(SENSOR_NAMES)

    REWARD_EAT = 0.1
    REWARD_DRINK = -0.1
    CAP_NEED = True

    metadata = {
        'render.modes': ['human']
    }

    num_tiles_x = 1
    num_tiles_y = 1
    screen_width = 100
    screen_height = 100

    tile_width = screen_width / num_tiles_x
    tile_height = screen_height / num_tiles_y

    class TileObject(object):
        def __init__(self, sprite, x=0, y=0):
            self.sprite = sprite
            self.tile_pos = np.array([x, y], dtype=np.int64)

        @property
        def tile_pos(self):
            return self.__tile_pos

        @tile_pos.setter
        def tile_pos(self, p):
            self.__tile_pos = p
            self.sprite.position = (
                self.tile_pos[0] * EatDrinkEnv.tile_width, self.tile_pos[1] * EatDrinkEnv.tile_height)

    class Body(TileObject):
        def __init__(self, sprite, x=0, y=0):
            super().__init__(sprite, x, y)

            self.reset()

        def reset(self):
            self.needs = [1.0]
            self.reset_sensors()

        def reset_sensors(self):
            self.sensors = [0 for x in range(EatDrinkEnv.NUM_SENSORS)]

    def __init__(self):
        self.action_space = spaces.Discrete(EatDrinkEnv.NUM_ACTIONS)
        self.observation_space = spaces.Discrete(EatDrinkEnv.NUM_SENSORS)

        self.viewer = None
        self.done = False

    # sprites
        # background
        fname = path.join(path.dirname(__file__),
                          "assets/green_tile.png")
        image = pyglet.image.load(fname)
        scale_x = EatDrinkEnv.tile_width / image.width
        scale_y = EatDrinkEnv.tile_height / image.height

        self.sprite_batch_background = pyglet.graphics.Batch()
        self.sprite_background = []

        pos_y = 0
        for _ in range(EatDrinkEnv.num_tiles_y):
            pos_x = 0
            for _ in range(EatDrinkEnv.num_tiles_x):
                self.sprite_background.append(pyglet.sprite.Sprite(
                    image, pos_x, pos_y, subpixel=True, batch=self.sprite_batch_background))
                self.sprite_background[-1].scale_x = scale_x
                self.sprite_background[-1].scale_y = scale_y

                pos_x += EatDrinkEnv.tile_width
            pos_y += EatDrinkEnv.tile_height

        # cat
        fname = path.join(path.dirname(__file__), "assets/cat.png")
        image = pyglet.image.load(fname)
        scale_x = EatDrinkEnv.tile_width / image.width
        scale_y = EatDrinkEnv.tile_height / image.height

        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        self.body = EatDrinkEnv.Body(sprite, 0, 0)

        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        self.seed()
        self.reset()

    def _get_initial_state(self):
        """Get the initial state."""

        self._update_sensors()
        return (self.body.sensors, {"needs": self.body.needs, "rewards": [0]})

    def seed(self, seed=None):
        seed = seeding.np_random(seed)
        return [seed]

    def _update_sensors(self):
        self.body.sensors[0] = 1

    def step(self, action):

        reward = self.body.needs[0]

        if action == 0: # eat
            self.body.needs[0] += EatDrinkEnv.REWARD_EAT
        elif action == 1: # drink
            self.body.needs[0] += EatDrinkEnv.REWARD_DRINK

        if EatDrinkEnv.CAP_NEED and self.body.needs[0] > 1:
            self.body.needs[0] = 1

        # sensors
        self.body.reset_sensors()
        self._update_sensors()
        
        state = list(self.body.sensors)

        # reward
        reward = self.body.needs[0] - reward

        return (state, None, self.done, {"needs": self.body.needs,
                                         "rewards": [reward]})

    def reset(self):
        self.body.reset()

        # return the initial state
        return self._get_initial_state()

    def _get_viewer(self):

        if self.viewer is None:
            self.viewer = Renderer(
                EatDrinkEnv.screen_width, EatDrinkEnv.screen_height)

            self.viewer.add_drawable(self.sprite_batch_background)
            self.viewer.add_drawable(self.body.sprite)

        return self.viewer

    def render(self, mode='human', close=False):

        if close:
            self.done = True
            return

        viewer = self._get_viewer()

        if mode == 'human':
            self.done = not viewer.render()
        elif mode == 'rgb_array':
            return viewer.render(True)
        else:
            raise NotImplementedError

    def close(self):
        print("render::close")
        if self.viewer:
            self.viewer.close()


if __name__ == '__main__':
    import aaa_survivability.envs.animat.eat_drink_env

    env = gym.make('eat_drink-v0')
    env.reset()

    done = False
    while not done:
        action = env.action_space.sample()
        state, reward, done, info = env.step(action)

        print(info["needs"], state, action)

        env.render()
        time.sleep(1/10)

    env.close()
    print("Finished")