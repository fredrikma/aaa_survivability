import pyglet
from pyglet.gl import *

class Renderer(object):
    def __init__(self, width, height):

        self.width = width
        self.height = height
        self.window = pyglet.window.Window(
            width=width, height=height, vsync=False)
        self.window.on_close = self.window_closed_by_user
        self.isopen = True
        self.drawables = []

        # enable alphablending
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    def close(self):
        self.window.close()

    def window_closed_by_user(self):
        self.isopen = False

    def add_drawable(self, drawable):
        self.drawables.append(drawable)

    def remove_drawable(self, drawable):
        self.drawables.remove(drawable)

    def render(self, return_rgb_array=False):
        glClearColor(1, 1, 1, 1)
        self.window.clear()
        self.window.switch_to()
        self.window.dispatch_events()

        for drawable in self.drawables:
            drawable.draw()

        arr = None
        if return_rgb_array:
            # buffer = pyglet.image.get_buffer_manager().get_color_buffer()
            # image_data = buffer.get_image_data()
            # arr = np.fromstring(image_data.data, dtype=np.uint8, sep='')
            # # In https://github.com/openai/gym-http-api/issues/2, we
            # # discovered that someone using Xmonad on Arch was having
            # # a window of size 598 x 398, though a 600 x 400 window
            # # was requested. (Guess Xmonad was preserving a pixel for
            # # the boundary.) So we use the buffer height/width rather
            # # than the requested one.
            # arr = arr.reshape(buffer.height, buffer.width, 4)
            # arr = arr[::-1,:,0:3]
            pass
        self.window.flip()
        return arr if return_rgb_array else self.isopen
