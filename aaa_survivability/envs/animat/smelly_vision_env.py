import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
import time
import math
from os import path
import pyglet

from aaa_survivability.envs.animat.animat_env import AnimatEnv
from aaa_survivability.envs.animat.renderer import Renderer

class SmellyVisionEnv(AnimatEnv):
    """Tilebased NxN discrete environment (default N = 20).

    A scenario for testing the 'smelly vision' sensors.

    Actions:
        0 = Up
        1 = Down
        2 = Left
        3 = Right

    Sensors:
        von-neuman directions for strongest smell, mutually exclusive only the strongest direction active
        4=4

    Rules:
        NEED decays over time
        FOOD objects never overlap
        FOOD is consumed when Body touches it
        When all FOOD is consumed they all respawn at new random locations
        The scenario ends when NEED <= 0

        (optional) small positive reward for moving towards a food source


    Configuration: (key : default value)
        'Num tiles X' : 20
        'Num tiles Y' : 20
        'Num food objects' : 100
        'Global need decay' : -0.001
    """
    ACTION_NAMES = ["Up", "Down", "Left", "Right"]
    NUM_ACTIONS = len(ACTION_NAMES)

    SENSOR_NAMES = ["Dir-up FOOD", "Dir-down FOOD", "Dir-left FOOD", "Dir-right FOOD"]
    RECEPTOR_OFFSETS = [[0,1], [0,-1], [-1,0], [1,0]]

    NUM_SENSORS = len(SENSOR_NAMES)

    REWARD_FOOD = 0.1
    REWARD_MOVE = -0.001

    CAP_NEED = True

    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 4
    }

    # movement actions
    actions_move = {0: [0, 1],
                    1: [0, -1],
                    2: [-1, 0],
                    3: [1, 0]}

    # TODO: make configurable
    screen_width = 600
    screen_height = 600

    class TileObject(object):
        def __init__(self, sprite, env, x=0, y=0):
            self.sprite = sprite
            self.env = env
            self.tile_pos = np.array([x, y], dtype=np.int64)

        @property
        def tile_pos(self):
            return self.__tile_pos

        @tile_pos.setter
        def tile_pos(self, p):
            self.__tile_pos = p
            self.sprite.position = (
                self.tile_pos[0] * self.env.tile_width, self.tile_pos[1] * self.env.tile_height)

    class Body(TileObject):
        """This is the physiological representation for an Agent."""
        def __init__(self, sprite, env, x=0, y=0):
            super().__init__(sprite, env, x, y)

            self.reset()

        def reset(self):
            self.needs = [1.0]
            self.food_eaten = 0
            
            self.reset_sensors()

        def reset_sensors(self):
            self.sensors = [0 for x in range(SmellyVisionEnv.NUM_SENSORS)]

    class Food(TileObject):
        def __init__(self, sprite, env, x=0, y=0):
            super().__init__(sprite, env, x, y)


    def __init__(self):
        self.action_space = spaces.Discrete(SmellyVisionEnv.NUM_ACTIONS)
        self.observation_space = spaces.Discrete(SmellyVisionEnv.NUM_SENSORS)

        self.viewer = None
        self.done = False
    
    def configure(self, config):
        self.num_tiles_x = config.setdefault('Num tiles X', 20)
        self.num_tiles_y = config.setdefault('Num tiles Y', 20)
        self.num_food_objects = config.setdefault('Num food objects', 100)
        self.global_need_decay = config.setdefault('Global need decay', -0.001)

        self.tile_width = SmellyVisionEnv.screen_width / self.num_tiles_x
        self.tile_height = SmellyVisionEnv.screen_height / self.num_tiles_y
        

    # sprites
        # background
        fname = path.join(path.dirname(__file__),
                          "assets/green_tile.png")
        image = pyglet.image.load(fname)
        scale_x = self.tile_width / image.width
        scale_y = self.tile_height / image.height

        self.sprite_batch_background = pyglet.graphics.Batch()
        self.sprite_background = []

        pos_y = 0
        for _ in range(self.num_tiles_y):
            pos_x = 0
            for _ in range(self.num_tiles_x):
                self.sprite_background.append(pyglet.sprite.Sprite(
                    image, pos_x, pos_y, subpixel=True, batch=self.sprite_batch_background))
                self.sprite_background[-1].scale_x = scale_x
                self.sprite_background[-1].scale_y = scale_y

                pos_x += self.tile_width
            pos_y += self.tile_height

        # body
        fname = path.join(path.dirname(__file__), "assets/cat.png")
        image = pyglet.image.load(fname)
        scale_x = self.tile_width / image.width
        scale_y = self.tile_height / image.height

        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        pos = [np.random.randint(self.num_tiles_x),
               np.random.randint(self.num_tiles_x)]
        self.body = SmellyVisionEnv.Body(sprite, self, pos[0], pos[1])

        # food
        fname = path.join(path.dirname(__file__), "assets/fish.png")
        self.food_image = pyglet.image.load(fname)
        self._spawn_food_objects()

        # initialize
        self.seed()
        self.reset()

    def _spawn_food_objects(self):
        """Spawn all the food objects at random locations, making sure they dont overlap. """

        self.food = []
        for _ in range(self.num_food_objects):
            pos = self._get_unoccupied_random_tile()
            self._add_food_object(pos)
    
    def _add_food_object(self, pos):
        """Add a new food object at a specific tile position"""

        scale_x = self.tile_width / self.food_image.width
        scale_y = self.tile_height / self.food_image.height
        
        sprite = pyglet.sprite.Sprite(self.food_image, subpixel=True)
        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        if self.viewer:
            self.viewer.add_drawable(sprite)

        self.food.append(SmellyVisionEnv.Food(sprite, self, pos[0], pos[1]))

    def _remove_food_object(self, food):
        """Remove a food object"""

        self.food.remove(food)

        if self.viewer:
            self.viewer.remove_drawable(food.sprite)

    def _get_initial_state(self):
        """Get the initial state."""

        self._update_sensors()
        return (self.body.sensors, {"needs": self.body.needs, 
                                    "rewards": [0], 
                                    "food_eaten": self.body.food_eaten})

    def _limit_coordinates(self, coord):
        coord[0] = min(coord[0], self.num_tiles_x - 1)
        coord[0] = max(coord[0], 0)
        coord[1] = min(coord[1], self.num_tiles_y - 1)
        coord[1] = max(coord[1], 0)
        return coord

    def _action_move(self, action):
        diff = SmellyVisionEnv.actions_move[action]
        tmp = self.body.tile_pos + diff
        self.body.tile_pos = self._limit_coordinates(tmp)

        self.body.needs[0] += SmellyVisionEnv.REWARD_MOVE


    def _action_eat(self):
        """Automatically eat food that occupies the Body position """

        for f in self.food:
            if (f.tile_pos == self.body.tile_pos).all():

                self.body.needs[0] += SmellyVisionEnv.REWARD_FOOD
                self._remove_food_object(f)

                break # food never overlap

        # If no food left respawn them all
        if len(self.food) == 0:
            self._spawn_food_objects()

    def _get_unoccupied_random_tile(self):
        """ Returns a tile that isn't occupied by anything. """

        excluded = []
        excluded.append(self.body.tile_pos)

        for f in self.food:
            excluded.append(f.tile_pos)

        return self._get_random_tile(excluded)

    def _get_random_tile(self, excluded=[]):
        """Get a random tile with optional exlusions."""
        while True:
            rx = np.random.randint(self.num_tiles_x)
            ry = np.random.randint(self.num_tiles_y)

            invalid = False
            for x in excluded:
                if rx == x[0] and ry == x[1]:
                    invalid = True
                    break
            if not invalid:
                break

        return np.array([rx, ry], dtype=np.int64)

    def seed(self, seed=None):
        seed = seeding.np_random(seed)
        return [seed]

    def _update_sensors(self):
        """"Sense the environment."""

        if len(self.food) == 0:
            return

        intensities = []

        # Receptor locations
        for i in range(len(SmellyVisionEnv.RECEPTOR_OFFSETS)):
            receptor_pos = self.body.tile_pos + SmellyVisionEnv.RECEPTOR_OFFSETS[i]

            # intensity to all food objects
            intensity = 0.0
            for f in self.food:
                food_pos = f.tile_pos

                diff = receptor_pos - food_pos

                d = math.sqrt(diff[0]**2 + diff[1]**2)
                intensity += 1.0 / (1.0 + d**2)

            intensities.append(intensity)

        max_value = np.amax(intensities)
        candidates = np.argwhere(intensities == max_value).flatten()
        highest = np.random.choice(candidates)

        # Smelly vision
        self.body.sensors[highest] = 1        


    def step(self, action):

        reward = self.body.needs[0]

        # Movement
        if action == 0 or action == 1 or action == 2 or action == 3:
            self._action_move(action)

        # Eat automatically if possible
        self._action_eat()

        # cap the need
        if SmellyVisionEnv.CAP_NEED and self.body.needs[0] > 1:
            self.body.needs[0] = 1

        # Sensors
        self.body.reset_sensors()
        self._update_sensors()
        
        state = list(self.body.sensors)

        # reward/s
        reward = self.body.needs[0] - reward

        # Global decay
        self.body.needs[0] += self.global_need_decay

        # Termination condition
        if self.body.needs[0] < 0.0:
            self.done = True

        return (state, None, self.done, {"needs": self.body.needs,
                                           "rewards": [reward],
                                           "food_eaten": self.body.food_eaten})

    def reset(self):

        self.done = False

        self.body.reset()

        # randomize starting positions
        self.body.tile_pos = self._get_random_tile()
        self._spawn_food_objects()

        # return the initial state
        return self._get_initial_state()

    def _get_viewer(self):

        if self.viewer is None:
            self.viewer = Renderer(
                SmellyVisionEnv.screen_width, SmellyVisionEnv.screen_height)

            self.viewer.add_drawable(self.sprite_batch_background)
            
            for f in self.food:
                self.viewer.add_drawable(f.sprite)

            self.viewer.add_drawable(self.body.sprite)

        return self.viewer

    def render(self, mode='human', close=False):

        if close:
            self.done = True
            return

        viewer = self._get_viewer()

        if mode == 'human':
            self.done = not viewer.render()
        elif mode == 'rgb_array':
            return viewer.render(True)
        else:
            raise NotImplementedError

    def close(self):
        print("render::close")
        if self.viewer:
            self.viewer.close()


if __name__ == '__main__':
    import aaa_survivability.envs.animat.smelly_vision_env

    env = gym.make('smelly_vision-v0')
    env.configure({})

    env.reset()

    done = False
    while not done:
        action = env.action_space.sample()
        state, reward, done, info = env.step(action)

        print(info["needs"], info["rewards"], state)

        env.render()
        time.sleep(1/10)

    env.close()
    print("Finished")
