import numpy as np
import sys
import gym
from gym import error, spaces, utils
from gym.utils import seeding


class Simple3x3Env(gym.Env):
    """
    Just to test setup of a new environment.
    3x3 grid where the goal is to reach the lower right corner.

    State space:
        0 1 2
        3 4 5
        6 7 8

    Actions:
        0 = Left
        1 = Right
        2 = Up
        3 = Down
    """
    actions = {0: [-1, 0],
               1: [1, 0],
               2: [0, -1],
               3: [0, 1]}

    metadata = {'render.modes': ['human']}

    def __init__(self):
        print("init")

        self.shape = (3, 3)
        self.num_states = self.shape[0] * self.shape[1]
        self.action_space = spaces.Discrete(4)
        self.observation_space = spaces.Discrete(self.num_states)

        # starting state
        self.curr_pos = np.array([0, 0], dtype=np.int64)
        self.state = self.curr_pos[1] * self.shape[0] + self.curr_pos[0]

        self.seed()

    def _limit_coordinates(self, coord):
        """
        Prevent the agent from falling out of the grid world
        :param coord: 
        :return: 
        """
        coord[0] = min(coord[0], self.shape[0] - 1)
        coord[0] = max(coord[0], 0)
        coord[1] = min(coord[1], self.shape[1] - 1)
        coord[1] = max(coord[1], 0)
        return coord

    def seed(self, seed=None):
        print("seed")
        seed = 0
        #seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):
        print("step: action = {0}".format(action))

        diff = Simple3x3Env.actions[action]
        self.curr_pos = self.curr_pos + diff

        self.curr_pos = self._limit_coordinates(self.curr_pos)
        self.state = self.curr_pos[1] * self.shape[0] + self.curr_pos[0]

        r = -1
        done = False
        if self.state == 8:
            r = 1
            done = True

        return (self.state, r, done, {})

    def reset(self):
        print("reset")

        self.curr_pos = np.array([0, 0], dtype=np.int64)
        self.state = self.curr_pos[1] * self.shape[0] + self.curr_pos[0]
        return self.state

    def render(self, mode='human', close=False):
        outfile = sys.stdout

        for s in range(self.num_states):
            position = np.unravel_index(s, self.shape)
            if self.state == s:
                output = " x "
            # Print terminal state
            elif position == (2, 2):
                output = " T "
            else:
                output = " o "

            if position[1] == 0:
                output = output.lstrip()
            if position[1] == self.shape[1] - 1:
                output = output.rstrip()
                output += '\n'

            outfile.write(output)
        outfile.write('\n')

    def close(self, action):
        print("close")
