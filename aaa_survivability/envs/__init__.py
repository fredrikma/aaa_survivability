print("aaa_survivability/envs/__init__.py")

from gym.envs.registration import register

print("registering")

register(
    id='simple_3x3-v0',
    entry_point='aaa_survivability.envs.simple:Simple3x3Env',
)

register(
    id='cat_3x3-v0',
    entry_point='aaa_survivability.envs.animat:Cat3x3Env',
    #max_episode_steps=100
)

register(
    id='eat_drink-v0',
    entry_point='aaa_survivability.envs.animat:EatDrinkEnv'
)

register(
    id='yoshida_1-v0',
    entry_point='aaa_survivability.envs.animat.yoshida_1_env:Yoshida1Env',    
    max_episode_steps=1000
)

register(
    id='yoshida_1_no_limit-v0',
    entry_point='aaa_survivability.envs.animat.yoshida_1_env:Yoshida1Env',    
)

register(
    id='smelly_vision-v0',
    entry_point='aaa_survivability.envs.animat.smelly_vision_env:SmellyVisionEnv',    
)

register(
    id='thesis_1-v0',
    entry_point='aaa_survivability.envs.animat.thesis_1_env:Thesis1Env',    
)