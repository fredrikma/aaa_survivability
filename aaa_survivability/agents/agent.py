
class Agent(object):
    """ Base class for agents """

    def __init__(self):
        print("Agent::__init__")
        self.learning_enabled = True

    def get_action(self, state, reward, done, info, time_zero):
        """Returns an action to take.
        
        Parameters
        ----------
        state   :
            The current state of the agent (observation space).
        reward  : 
            The reward received from the last action performed.
        done    :
            If the simulation environemnt has ended.
        info    :
            Whatever info we got from the simulation environment.
        time_zero    :
            True if t==0, gives the agent a chance to react to t==0 initialization
        """

        raise NotImplementedError

    def get_brain_complexity(self):
        """ This depends on the Agent, a measurements of its complexity. i.e entries in Q-table or similar"""
        return 0