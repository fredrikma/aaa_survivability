import unittest
import hashlib

from aaa_survivability.agents.animat.perception_graph import PerceptionGraph


class TestPerceptionGraph(unittest.TestCase):

    def setUp(self):
        self.pg = PerceptionGraph()
        sensor_names = ['a', 'b', 'c', 'd']
        [self.pg.add_SENSOR_node(node) for node in sensor_names]

    def test_add_SENSOR_node(self):
        node = 'e'
        self.pg.add_SENSOR_node(node)
        self.assertEqual(self.pg.G.graph['n_added_uid'], 0)
        self.check_existing_node(
            node, 0, node, 'SENSOR', [], [])

    def test_add_AND_node(self):
        predecessors = ['a', 'b']
        self.pg.add_AND_node(predecessors)
        self.assertEqual(self.pg.G.graph['n_added_uid'], 1)
        self.check_existing_node(
            'AND_1', 1, predecessors, 'AND', [], predecessors)

    def test_add_SEQ_node(self):
        first_pred = 'a'
        second_pred = 'b'
        self.pg.add_SEQ_node(first_pred, second_pred)
        self.assertEqual(self.pg.G.graph['n_added_uid'], 1)
        self.check_existing_node(
            'SEQ_1', 1, ['SEQ_1'], 'SEQ', [], [first_pred, second_pred])

    def test_add_random_AND_node(self):
        self.pg.add_random_AND_node()
        self.assertEqual(self.pg.G.graph['n_added_uid'], 1)
        predecessors = list(self.pg.G.predecessors('AND_1'))
        self.check_existing_node(
            'AND_1', 1, predecessors, 'AND', [], predecessors)

    def test_add_random_SEQ_node(self):
        self.pg.add_random_SEQ_node()
        self.assertEqual(self.pg.G.graph['n_added_uid'], 1)
        predecessors = list(self.pg.G.predecessors('SEQ_1'))
        self.check_existing_node(
            'SEQ_1', 1, ['SEQ_1'], 'SEQ', [], predecessors)

    def test_get_active_nodes(self):
        sensor_states = [1, 1, 0, 0]
        self.pg.propagate_activity(sensor_states)
        active_nodes = self.pg.get_active_nodes()
        self.assertEqual(2, len(active_nodes))
        for active_node in active_nodes:
            self.assertIn(active_node, ['a', 'b'])

    def test_get_top_active_nodes(self):
        predecessors = ['a', 'b']
        self.pg.add_AND_node(predecessors)
        sensor_states = [1, 1, 1, 0]
        self.pg.propagate_activity(sensor_states)
        active_nodes = self.pg.get_top_active_nodes()
        self.assertEqual(len(active_nodes), 2)
        expected_top_active = ['AND_1', 'c']
        for active_node in active_nodes:
            self.assertIn(active_node, expected_top_active)

    def test_get_valid_AND_connections(self):
        predecessors = ['a', 'b']
        self.pg.add_AND_node(predecessors)
        node_1 = 'AND_1'
        node_2 = 'c'
        node_3 = 'a'
        valid_nodes_1 = self.pg.get_valid_AND_connections(node_1)
        valid_nodes_2 = self.pg.get_valid_AND_connections(node_2)
        valid_nodes_3 = self.pg.get_valid_AND_connections(node_3)
        expected_valid_nodes_1 = ['c', 'd']
        expected_valid_nodes_2 = ['a', 'b', 'd', 'AND_1']
        expected_valid_nodes_3 = ['c', 'd']
        self.assertEqual(len(valid_nodes_1), len(expected_valid_nodes_1))
        self.assertEqual(len(valid_nodes_2), len(expected_valid_nodes_2))
        self.assertEqual(len(valid_nodes_3), len(expected_valid_nodes_3))
        for valid_node in valid_nodes_1:
            self.assertIn(valid_node, expected_valid_nodes_1)
        for valid_node in valid_nodes_2:
            self.assertIn(valid_node, expected_valid_nodes_2)
        for valid_node in valid_nodes_3:
            self.assertIn(valid_node, expected_valid_nodes_3)

    def test_propagate_activity(self):
        AND_predecessors = [['a', 'b'], ['b', 'c'], ['AND_1', 'c']]
        for i in AND_predecessors:
            self.pg.add_AND_node(i)
        SEQ_predecessors = ['b', 'c']
        self.pg.add_SEQ_node(SEQ_predecessors[0], SEQ_predecessors[1])
        sensor_states_1 = [1, 1, 1, 1]
        self.pg.propagate_activity(sensor_states_1)
        expected_active_1 = ['a', 'b', 'c', 'd', 'AND_1', 'AND_2', 'AND_3']
        self.assertEqual(len(expected_active_1), len(
            self.pg.G.graph['active_nodes']))
        for active_node in self.pg.G.graph['active_nodes']:
            self.assertIn(active_node, expected_active_1)
        expected_top_active_1 = ['d', 'AND_3']
        self.assertEqual(len(expected_top_active_1), len(
            self.pg.G.graph['top_active_nodes']))
        for top_active_node in self.pg.G.graph['top_active_nodes']:
            self.assertIn(top_active_node, expected_top_active_1)

        self.pg.G.nodes['SEQ_4']['stage'] = 1
        sensor_states_2 = [0, 1, 1, 1]
        self.pg.propagate_activity(sensor_states_2)
        expected_active_2 = ['b', 'c', 'd', 'AND_2', 'SEQ_4']
        self.assertEqual(len(expected_active_2), len(
            self.pg.G.graph['active_nodes']))
        for active_node in self.pg.G.graph['active_nodes']:
            self.assertIn(active_node, expected_active_2)
        expected_top_active_2 = ['AND_2', 'SEQ_4', 'd']
        self.assertEqual(len(expected_top_active_2), len(
            self.pg.G.graph['top_active_nodes']))
        for top_active_node in self.pg.G.graph['top_active_nodes']:
            self.assertIn(top_active_node, expected_top_active_2)

        sensor_states_3 = [0, 1, 0, 1]
        self.pg.propagate_activity(sensor_states_3)
        expected_active_3 = ['b', 'd']
        self.assertEqual(len(expected_active_3), len(
            self.pg.G.graph['active_nodes']))
        for active_node in self.pg.G.graph['active_nodes']:
            self.assertIn(active_node, expected_active_3)
        expected_top_active_3 = ['b', 'd']
        self.assertEqual(len(expected_top_active_3), len(
            self.pg.G.graph['top_active_nodes']))
        for top_active_node in self.pg.G.graph['top_active_nodes']:
            self.assertIn(top_active_node, expected_top_active_3)

    def test_remove_node(self):
        AND_predecessors = ['a', 'b']
        self.pg.add_AND_node(AND_predecessors)
        SEQ_predecessors = ['b', 'c']
        self.pg.add_SEQ_node(SEQ_predecessors[0], SEQ_predecessors[1])
        self.pg.remove_node('a')
        self.assertIn('a', self.pg.G.nodes)
        self.pg.remove_node('AND_1')
        self.check_removed_node('AND_1', 1, AND_predecessors, 'AND')
        self.pg.remove_node('SEQ_2')
        self.check_removed_node('SEQ_2', 1, SEQ_predecessors, 'SEQ')

    def test_add_to_graph_layers(self):
        layer_0_node = 'test_0'
        layer_1_node = 'test_1'
        self.pg._add_to_graph_layers(layer_0_node, 0)
        self.pg._add_to_graph_layers(layer_1_node, 1)
        self.assertIn(layer_0_node, self.pg.G.graph['layers'][0])
        self.assertIn(layer_1_node, self.pg.G.graph['layers'][1])

    def test_add_uid(self):
        origin = ['a', 'b']
        uid_1 = self.pg._get_origin_uid(origin, 'AND')
        uid_2 = self.pg._get_origin_uid(origin, 'SEQ')
        self.pg._add_uid(uid_1, 'AND')
        self.pg._add_uid(uid_1, 'AND')
        self.pg._add_uid(uid_2, 'SEQ')
        self.assertEqual(self.pg.G.graph['uid'][uid_1], 'AND_1')
        self.assertEqual(self.pg.G.graph['uid'][uid_2], 'SEQ_2')

    def test_get_origin_uid(self):
        origin = ['a', 'b']
        uid_1 = self.pg._get_origin_uid(origin, 'AND')
        uid_2 = self.pg._get_origin_uid(origin, 'SEQ')
        uid_3 = self.pg._get_origin_uid('a', 'SENSOR')
        expected_uid_1 = hashlib.sha256('_a_b'.encode('utf-8')).hexdigest()
        expected_uid_2 = hashlib.sha256('SEQ_a_b'.encode('utf-8')).hexdigest()
        expected_uid_3 = hashlib.sha256('_a'.encode('utf-8')).hexdigest()
        self.assertEqual(uid_1, expected_uid_1)
        self.assertEqual(uid_2, expected_uid_2)
        self.assertEqual(uid_3, expected_uid_3)

    def test_get_node_uid(self):
        AND_predecessors = ['a', 'b']
        self.pg.add_AND_node(AND_predecessors)
        SEQ_predecessors = ['b', 'c']
        self.pg.add_SEQ_node(SEQ_predecessors[0], SEQ_predecessors[1])
        node_1 = 'AND_1'
        node_2 = 'SEQ_2'
        uid_1 = self.pg._get_node_uid(node_1)
        uid_2 = self.pg._get_node_uid(node_2)
        uid_3 = self.pg._get_node_uid('a')
        expected_uid_1 = hashlib.sha256('_a_b'.encode('utf-8')).hexdigest()
        expected_uid_2 = hashlib.sha256('SEQ_b_c'.encode('utf-8')).hexdigest()
        expected_uid_3 = hashlib.sha256('_a'.encode('utf-8')).hexdigest()
        self.assertEqual(uid_1, expected_uid_1)
        self.assertEqual(uid_2, expected_uid_2)
        self.assertEqual(uid_3, expected_uid_3)

    def test_reset_activity(self):
        sensor_states = [1, 1, 0, 0]
        self.pg.propagate_activity(sensor_states)
        self.pg._reset_activity()
        self.assertEqual(len(self.pg.G.graph['active_nodes']), 0)
        self.assertEqual(len(self.pg.G.graph['top_active_nodes']), 0)

    def test_set_active_sensors(self):
        sensor_states = [1, 1, 0, 0]
        active_sensors = ['a', 'b']
        self.pg._set_active_sensors(sensor_states)
        self.assertEqual(
            len(self.pg.G.graph['active_nodes']), 2)
        for active_sensor in active_sensors:
            self.assertIn(active_sensor, self.pg.G.graph['active_nodes'])

    def test_update_activity_AND_node(self):
        sensor_states = [1, 1, 1, 0]
        self.pg.propagate_activity(sensor_states)
        AND_predecessors = ['a', 'b']
        self.pg.add_AND_node(AND_predecessors)
        self.pg._update_activity_AND_node('AND_1', 'a', 'b')
        self.assertIn('AND_1', self.pg.G.graph['active_nodes'])

    def test_update_activity_SEQ_node(self):
        sensor_states = [1, 1, 1, 0]
        self.pg.propagate_activity(sensor_states)
        SEQ_predecessors = ['a', 'b']
        node = 'SEQ_1'
        self.pg.add_SEQ_node(SEQ_predecessors[0], SEQ_predecessors[1])
        self.pg._update_activity_SEQ_node(
            node, SEQ_predecessors[1], SEQ_predecessors[0])
        self.assertEqual(self.pg.G.nodes[node]['stage'], 1)
        self.assertNotIn(node, self.pg.G.graph['active_nodes'])

        self.pg._update_activity_SEQ_node(
            node, SEQ_predecessors[1], SEQ_predecessors[0])
        self.assertEqual(self.pg.G.nodes[node]['stage'], 1)
        self.assertIn(node, self.pg.G.graph['active_nodes'])

    def check_existing_node(self, node, layer, origin, node_type, successors, predecessors):
        self.assertEqual(self.pg.G.nodes[node]['node_type'], node_type)
        self.assertEqual(self.pg.G.nodes[node]['layer'], layer)
        for sensor_node in origin:
            self.assertIn(sensor_node, self.pg.G.nodes[node]['origin'])
        for successor in successors:
            self.assertIn(successor, self.pg.G.successors(node))
        for predecessor in predecessors:
            self.assertIn(predecessor, self.pg.G.predecessors(node))
            self.assertIn(node, self.pg.G.successors(predecessor))
        self.assertIn(node, self.pg.G.graph['node_types'][node_type])
        self.assertIn(node, self.pg.G.graph['layers'][layer])
        if node_type == 'AND':
            uid = self.pg._get_origin_uid(origin, 'AND')
            self.assertIn(uid, self.pg.G.graph['uid'])
            self.assertEqual(node, self.pg.G.graph['uid'][uid])
        elif node_type == 'SEQ':
            self.assertEqual(self.pg.G.nodes[node]['stage'], 0)
            self.assertIn(self.pg.G.nodes[node]
                          ['first_predecessor'], predecessors)
            node_1_origin = self.pg.G.nodes[predecessors[0]]['origin']
            node_2_origin = self.pg.G.nodes[predecessors[1]]['origin']
            combined_origin = list(set(node_1_origin + node_2_origin))
            uid = self.pg._get_origin_uid(combined_origin, 'SEQ')
            self.assertIn(uid, self.pg.G.graph['uid'])
            self.assertEqual(node, self.pg.G.graph['uid'][uid])

    def check_removed_node(self, node, layer, origin, node_type):
        self.assertNotIn(node, self.pg.G.graph['active_nodes'])
        self.assertNotIn(node, self.pg.G.graph['top_active_nodes'])
        self.assertNotIn(node, self.pg.G)
        if layer in self.pg.G.graph['layers']:
            self.assertNotIn(node, self.pg.G.graph['layers'][layer])
        self.assertNotIn(node, self.pg.G.graph['node_types'][node_type])
        uid = self.pg._get_origin_uid(origin, node_type)
        self.assertNotIn(uid, self.pg.G.graph['uid'])


if __name__ == '__main__':
    unittest.main()
