import unittest
import numpy as np
from gym import spaces

from aaa_survivability.agents.animat.animat import Animat


class TestAnimat(unittest.TestCase):

    def setUp(self):
        action_names = ['left', 'right']
        action_space = spaces.Discrete(len(action_names))
        sensor_names = ['a', 'b']
        sensor_states = [1, 1]
        info = {"needs": [1.0, 2.0]}
        self.animat = Animat(action_space, sensor_names,
                             sensor_states, info, action_names)

        self.animat.epsilon = 0.0
        self.animat.alpha = 1.0
        self.animat.gamma = 1.0
        self.animat.std_from_mean = 1
        self.animat.std_min_updates = 1
        self.percent_min_updates = 1

        self.animat.use_emotional_formation = False
        self.animat.use_probabilistic_formation = False
        self.reliability_based_on = 'delta_Q'
        self.use_explore = False
        self.animat.use_average_q = False
        self.animat.surprise_rule = 'std'
        self.animat.use_probabilistic_merge = False
        self.animat.use_emotional_merge = False
        self.enough_if_one_top_active_is_surprised = True
        self.use_emotional_reward = False

        self.fill_Q_R()
        
    def test_add_emotional_AND_node(self):
        nodes = ['a', 'b']
        node = 'AND_1'
        surprised = np.array([1, 0])
        need = 0
        action = 0
        self.animat._add_emotional_AND_node(nodes, surprised, need, action)
        self.assertIn(node, self.animat.pg.G)
        self.check_node_Q_R(node)

    def test_update_exploit(self):
        self.animat._update_global_Q(self.animat.top_active)
        self.animat._update_exploit()
        expected_values = [-7.42857143, 16.21052632]
        for a in range(self.animat.action_space.n):
            self.assertAlmostEqual(
                expected_values[a], self.animat._exploit[a])

    def test_update_local_R_online(self):
        new_values = np.array([0.1, -0.2, 0.15, -2.11, 101])
        var = np.var(new_values, ddof=1)
        for new_value in new_values:
            self.animat._R_values[0]['a'] = new_value
            self.animat._R_values[1]['a'] = new_value
            self.animat._update_local_R_online(['a'], 1)
        sigmoid_value = self.animat._reliability_sigmoid_function(
            self.animat._R_count[0]['a'][1])
        expected_value = sigmoid_value / (np.sqrt(var) + 1)
        self.assertAlmostEqual(
            expected_value, self.animat.R_matrices[0]['a'][1])

    def test_update_global_Q(self):
        self.animat._update_global_Q(self.animat.top_active)
        expected_Q_global = [[-59/7.0, 270/19], [-59/7.0, 270/19]]
        for i in range(len(self.animat.needs)):
            for j, Q in enumerate(self.animat._global_Q[i]):
                self.assertAlmostEqual(Q, expected_Q_global[i][j])

    def test_update_local_Q(self):
        rewards = [0.1, 0.5]
        action = 0
        pre_active = ['a']
        self.animat._update_global_Q(self.animat.top_active)
        self.animat._update_local_Q(rewards, pre_active, action)
        expected_Q_value = [2719/190, 559/38]
        for i in range(len(self.animat.needs)):
            self.assertAlmostEqual(
                self.animat.Q_matrices[i]['a'][action], expected_Q_value[i])
            self.assertEqual(self.animat.Q_matrices[i]['a'][1], 0)
            self.assertEqual(self.animat.Q_matrices[i]['b'][0], -10)
            self.assertEqual(self.animat.Q_matrices[i]['b'][1], 30)

    def test_check_if_surprised(self):
        top_active = ['a']
        self.animat._R_values[0]['a'] = 0.55
        is_surprised, _ = self.animat._check_if_surprised(top_active, 0)        
        self.assertEqual(is_surprised, 0)
        self.animat._R_values[0]['a'] = 0.35
        is_surprised, _ = self.animat._check_if_surprised(top_active, 0)
        self.assertEqual(is_surprised, 1)
        self.animat._R_values[0]['a'] = 0.85
        is_surprised, _ = self.animat._check_if_surprised(top_active, 0)
        self.assertEqual(is_surprised, 1)
        self.animat._R_values[0]['a'] = 0.115
        is_surprised, _ = self.animat._check_if_surprised(top_active, 1)
        self.assertEqual(is_surprised, 0)
        self.animat._R_values[0]['b'] = 0.175
        is_surprised, _ = self.animat._check_if_surprised(['b'], 0)
        self.assertEqual(is_surprised, 1)

    def test_update_utility(self):
        self.animat._update_global_Q(self.animat.top_active)
        self.animat._update_exploit()
        self.animat._update_utility()
        expected_values = [-7.42857143, 16.21052632]
        for a in range(self.animat.action_space.n):
            self.assertAlmostEqual(self.animat._utility[a], expected_values[a])

    def test_select_action(self):
        self.animat._update_global_Q(self.animat.top_active)
        self.animat._update_exploit()
        self.animat._update_utility()
        self.animat._select_action()
        expected_action = 1
        self.assertEqual(expected_action, self.animat._action)

    def test_add_new_node(self):
        node = 'AND_1'
        self.animat.pg.add_AND_node(['a', 'b'])
        self.animat._add_new_node(node)
        self.check_node_Q_R(node)

    def check_node_Q_R(self, node):
        for i in range(len(self.animat.needs)):
            self.assertIn(node, self.animat.Q_matrices[i])
            self.assertIn(node, self.animat.R_matrices[i])
            self.assertIn(node, self.animat._R_count[i])
            self.assertIn(node, self.animat._R_mean[i])
            self.assertIn(node, self.animat._R_std[i])
            self.assertIn(node, self.animat._R_M2[i])

    def fill_Q_R(self):
        self.animat.Q_matrices[0]['a'] = [1.0, 0]
        self.animat.R_matrices[0]['a'] = [0.1, 1]
        self.animat._R_count[0]['a'] = [10, 0]
        self.animat._R_mean[0]['a'] = [0.5, 0]
        self.animat._R_std[0]['a'] = [0.1, 0]
        self.animat._R_M2[0]['a'] = [0.8, 0]
        self.animat._updates_since_new_node[0]['a'][0] = 10
        self.animat._updates_since_new_node[0]['a'][1] = 0

        self.animat.Q_matrices[0]['b'] = [-10.0, 30.0]
        self.animat.R_matrices[0]['b'] = [0.6, 0.9]
        self.animat._R_count[0]['b'] = [1, 400]
        self.animat._R_mean[0]['b'] = [0.7, 0.9]
        self.animat._R_std[0]['b'] = [0.2, 0.1]
        self.animat._R_M2[0]['b'] = [0.5, 0.3]
        self.animat._updates_since_new_node[0]['b'][0] = 10
        self.animat._updates_since_new_node[0]['b'][1] = 10

        self.animat.Q_matrices[1]['a'] = [1.0, 0]
        self.animat.R_matrices[1]['a'] = [0.1, 1]
        self.animat._R_count[1]['a'] = [10, 0]
        self.animat._R_mean[1]['a'] = [0.5, 0]
        self.animat._R_std[1]['a'] = [0.1, 0]
        self.animat._R_M2[1]['a'] = [0.8, 0]
        self.animat._updates_since_new_node[1]['a'][0] = 0
        self.animat._updates_since_new_node[1]['a'][1] = 0

        self.animat.Q_matrices[1]['b'] = [-10.0, 30.0]
        self.animat.R_matrices[1]['b'] = [0.6, 0.9]
        self.animat._R_count[1]['b'] = [1, 400]
        self.animat._R_mean[1]['b'] = [0.7, 0.9]
        self.animat._R_std[1]['b'] = [0.2, 0.1]
        self.animat._R_M2[1]['b'] = [0.5, 0.3]
        self.animat._updates_since_new_node[1]['b'][0] = 0
        self.animat._updates_since_new_node[1]['b'][1] = 10


if __name__ == '__main__':
    unittest.main()
