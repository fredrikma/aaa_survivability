import random
import networkx as nx
import hashlib
import numpy as np


class PerceptionGraph(object):

    def __init__(self):
        self.G = nx.DiGraph()
        self.G.graph['active_nodes'] = set()
        self.G.graph['layers'] = {}
        self.G.graph['node_types'] = {'AND': [], 'SENSOR': [], 'SEQ': []}
        self.G.graph['n_added_uid'] = 0
        self.G.graph['top_active_nodes'] = []
        self.G.graph['uid'] = {}

    def add_SENSOR_node(self, node):
        if node not in self.G:
            sensor_layer = 0
            self.G.add_node(node, layer=sensor_layer, origin=[
                            node], node_type='SENSOR')
            self.G.graph['node_types']['SENSOR'].append(node)
            self._add_to_graph_layers(node, sensor_layer)
        else:
            print('Could not add SENSOR node since its name is already in use. SENSOR nodes must have unique names.')

    def add_AND_node(self, predecessors):
        node_origin = list(set(self.G.nodes[predecessors[0]]['origin'] +
                               self.G.nodes[predecessors[1]]['origin']))
        uid = self._get_origin_uid(node_origin, 'AND')
        node = None
        if uid not in self.G.graph['uid']:
            self._add_uid(uid, 'AND')
            node = self.G.graph['uid'][uid]
            self.G.add_node(node, node_type='AND')
            node_layer = 0
            for predecessor in predecessors:
                self.G.add_edge(predecessor, node)
                if self.G.nodes[predecessor]['layer'] > node_layer:
                    node_layer = self.G.nodes[predecessor]['layer']
            node_layer += 1
            self.G.nodes[node]['origin'] = node_origin
            self.G.nodes[node]['layer'] = node_layer
            self.G.graph['node_types']['AND'].append(node)
            self._add_to_graph_layers(node, node_layer)

        else:
            # print('Could not add AND node since it already exists. Predecessors: {} and {}'.format(
            #    predecessors[0], predecessors[1]))
            pass
        return node

    def add_SEQ_node(self, first_predecessor, second_predecessor):
        node_origin = list(set(self.G.nodes[first_predecessor]['origin'] +
                               self.G.nodes[second_predecessor]['origin']))
        uid = self._get_origin_uid(node_origin, 'SEQ')
        node = None
        if uid not in self.G.graph['uid']:
            self._add_uid(uid, 'SEQ')
            node = self.G.graph['uid'][uid]
            self.G.add_node(node, stage=0, origin=[node], node_type='SEQ',
                            first_predecessor=first_predecessor)
            self.G.add_edge(first_predecessor, node)
            self.G.add_edge(second_predecessor, node)
            node_layer = self.G.nodes[first_predecessor]['layer']
            if self.G.nodes[second_predecessor]['layer'] > node_layer:
                node_layer = self.G.nodes[second_predecessor]['layer']
            node_layer += 1
            self.G.nodes[node]['layer'] = node_layer
            self.G.graph['node_types']['SEQ'].append(node)
            self._add_to_graph_layers(node, node_layer)
        else:
            # print('Could not add SEQ node since it already exists. First predecessors: {}, second predecessor: {}'.format(
            #    first_predecessor, second_predecessor))
            pass
        return node

    def add_random_AND_node(self, candidates=None):
        if candidates is None:
            candidates = list(self.G.nodes)
        else:
            candidates = list(candidates)
        is_done = False
        node = None
        while not is_done:
            node_1_index = random.choice(range(len(candidates)))
            node_1 = candidates[node_1_index]
            candidates.pop(node_1_index)
            valid_second_nodes = self.get_valid_AND_connections(
                node_1, candidates)
            if len(valid_second_nodes) > 0:
                node_2 = random.choice(valid_second_nodes)
                node = self.add_AND_node([node_1, node_2])
                is_done = True
            elif len(candidates) == 1:
                is_done = True
        return node

    def add_random_SEQ_node(self, candidates=None):
        if candidates is None:
            candidates = list(self.G.nodes)
        else:
            candidates = list(candidates)
        is_done = False
        node = None
        while not is_done:
            node_1, node_2 = random.sample(candidates, 2)
            node_1_origin = self.G.nodes[node_1]['origin']
            node_2_origin = self.G.nodes[node_2]['origin']
            combined_origin = list(set(node_1_origin + node_2_origin))
            uid = self._get_origin_uid(combined_origin, 'SEQ')
            if uid not in self.G.graph['uid']:
                self.add_SEQ_node(node_1, node_2)
                is_done = True
        return node

    def get_active_nodes(self):
        return list(self.G.graph['active_nodes'])

    def get_top_active_nodes(self):
        return list(self.G.graph['top_active_nodes'])

    def get_valid_AND_connections(self, node_1, candidates=None):
        node_1_origin = self.G.nodes[node_1]['origin']
        if candidates is None:
            candidates = list(self.G.nodes)
            candidates.remove(node_1)
        else:
            candidates = list(candidates)
        valid_second_nodes = []
        for candidate in candidates:
            candidate_origin = self.G.nodes[candidate]['origin']
            common_origins = [
                x for x in node_1_origin if x in candidate_origin]
            if len(common_origins) == 0:
                combined_origin = node_1_origin + candidate_origin
                uid = self._get_origin_uid(combined_origin, 'AND')
                if uid not in self.G.graph['uid']:
                    valid_second_nodes.append(candidate)
        return valid_second_nodes

    def propagate_activity(self, sensor_states):
        self._set_active_sensors(sensor_states)
        for layer in range(1, len(self.G.graph['layers'])):
            for node in self.G.graph['layers'][layer]:
                predecessor_1, predecessor_2 = self.G.predecessors(node)
                if self.G.nodes[node]['node_type'] == 'AND':
                    self._update_activity_AND_node(
                        node, predecessor_1, predecessor_2)
                elif self.G.nodes[node]['node_type'] == 'SEQ':
                    self._update_activity_SEQ_node(
                        node, predecessor_1, predecessor_2)
                else:
                    print('Unknown node_type found while propagating activity!')
                    print('layer={}, node={}, node_type={}'.format(
                        layer, node, self.G.nodes[node]['node_type']))
        self._update_top_activity()

    def _update_top_activity(self):
        top_active_candidates = []
        origin = []
        origin_size = np.empty(0)

        # Find all top active candidates
        for active_node in self.G.graph['active_nodes']:
            is_top_active = True
            for successor in self.G.successors(active_node):
                if successor in self.G.graph['active_nodes']:
                    is_top_active = False
            if is_top_active:
                top_active_candidates.append(active_node)
                origin.append(set(self.G.nodes[active_node]['origin']))
                origin_size = np.append(origin_size, len(
                    self.G.nodes[active_node]['origin']))

        # Sort with respect to the sizes of the origins in descending order, the indices are returned.
        origin_size_indices = origin_size.argsort()[::-1]

        # Extract top active nodes by removing all nodes with an origin that is a subset to another origin
        current_origin_index = 0
        not_top_active = []
        while current_origin_index < len(origin_size_indices):
            not_top_active.clear()
            current_origin = origin[origin_size_indices[current_origin_index]]
            for i in range(current_origin_index+1, len(origin_size_indices)):
                candidate_origin = origin[origin_size_indices[i]]
                if candidate_origin.issubset(current_origin):
                    not_top_active.append(i)

            origin_size_indices = np.delete(
                origin_size_indices, not_top_active, axis=None)
            current_origin_index += 1

        for i in origin_size_indices:
            self.G.graph['top_active_nodes'].append(top_active_candidates[i])

    def remove_node(self, node):
        if list(self.G.successors(node)) == []:
            if node in self.G.graph['active_nodes']:
                self.G.graph['active_nodes'].remove(node)
            if node in self.G.graph['top_active_nodes']:
                self.G.graph['top_active_nodes'].remove(node)
            self.G.graph['node_types'][self.G.nodes[node]
                                       ['node_type']].remove(node)
            if len(self.G.graph['layers'][self.G.nodes[node]['layer']]) == 1:
                del self.G.graph['layers'][self.G.nodes[node]['layer']]
            else:
                self.G.graph['layers'][self.G.nodes[node]
                                       ['layer']].remove(node)
            uid = self._get_node_uid(node)
            del self.G.graph['uid'][uid]
            self.G.remove_node(node)
            print('Delete noded {} from perception graph'.format(node))
        else:
            print('Unable to remove node {} since it has successors'.format(node))

    def serialize(self, path):
        nx.write_gpickle(self.G, path)

    def deserialize(self, path):
        self.G = nx.read_gpickle(path)

    def export_dot(self, is_active):
        dot = 'digraph {\n rankdir=BT; \n ranksep=1; \n newrank=true; \n node [style=filled] \n'
        ranking = ''
        for layer, nodes in self.G.graph['layers'].items():
            ranking += '{rank=same; '
            sub_graph = 'subgraph cluster_{} {{\nlabel="Layer {}"; \n style=invis\n'.format(
                layer, layer)
            edges = ''
            node_style = ''
            for node in nodes:
                node_color = self._get_color(node, is_active)
                node_ID = self.get_dot_ID(node)
                node_style += '"{}" [fillcolor={} label="{}"]; \n'.format(
                    node_ID, node_color, node)
                ranking += '"{}"; '.format(node_ID)
                if len(list(self.G.predecessors(node))) == 0:
                    edges += '"{}"; \n'.format(node_ID)
                else:
                    for predecessor in self.G.predecessors(node):
                        pred_ID = self.get_dot_ID(predecessor)
                        edge_color = self._get_color(predecessor, is_active)
                        edges += '"{}" -> "{}" [color={}]; \n'.format(
                            pred_ID, node_ID, edge_color)
            ranking += '} \n'
            dot += ranking
            edges += node_style
            sub_graph += edges + '} \n'
            dot += sub_graph
        legend = self._get_legend(is_active)
        dot += legend
        dot += '}'
        return dot

    def get_dot_ID(self, node):
        return hashlib.sha256(node.encode('utf-8')).hexdigest()

    def _get_legend(self, is_active):
        legend = 'subgraph cluster_legend {\nlabel="Legend:"; \n style=rounded; \n labelloc=b; \n len=0 \n fontsize=18 \n'
        if is_active:
            active = 'active [label="Active", fillcolor={}] \n'.format(
                self._active_color())
            top_active = 'top_active [label="Top Active", fillcolor={}] \n'.format(
                self._top_active_color())
            inactive = 'inactive [label="Inactive", fillcolor={}] \n'.format(
                self._inactive_color())
            SEQ_stage = 'SEQ_stage [label="Stage=1", fillcolor={}] \n'.format(
                self._SEQ_stage_color())
            nodes = 'inactive -> SEQ_stage -> active -> top_active [style="invis"] \n }'
            legend += active + top_active + inactive + SEQ_stage + nodes
        else:
            AND = 'AND [label="AND", fillcolor={}] \n'.format(
                self._AND_color())
            SEQ = 'SEQ [label="SEQ", fillcolor={}] \n'.format(
                self._SEQ_color())
            SENSOR = 'SENSOR [label="SENSOR", fillcolor={}] \n'.format(
                self._SENSOR_color())
            nodes = 'SENSOR -> AND -> SEQ [style="invis"]\n }'
            legend += AND + SEQ + SENSOR + nodes
        return legend

    def _AND_color(self):
        return 'lightgoldenrod'

    def _SEQ_color(self):
        return 'lightblue'

    def _SEQ_stage_color(self):
        return 'lightpink'

    def _SENSOR_color(self):
        return 'lightsalmon'

    def _active_color(self):
        return 'lightcoral'

    def _top_active_color(self):
        return 'lightseagreen'

    def _inactive_color(self):
        return 'lightgray'

    def _get_color(self, node, is_active):
        if is_active:
            if node in self.G.graph['top_active_nodes']:
                color = self._top_active_color()
            elif node in self.G.graph['active_nodes']:
                color = self._active_color()
            elif self.G.nodes[node]['node_type'] == 'SEQ':
                if self.G.nodes[node]['stage'] == 1:
                    color = self._SEQ_stage_color()
                else:
                    color = self._inactive_color()
            else:
                color = self._inactive_color()
        else:
            node_type = self.G.nodes[node]['node_type']
            if node_type == 'SENSOR':
                color = self._SENSOR_color()
            elif node_type == 'AND':
                color = self._AND_color()
            elif node_type == 'SEQ':
                color = self._SEQ_color()
        return color

    def _add_to_graph_layers(self, node, layer):
        if layer not in self.G.graph['layers']:
            self.G.graph['layers'][layer] = [node]
        else:
            self.G.graph['layers'][layer].append(node)

    def _add_uid(self, uid, node_type):
        if uid not in self.G.graph['uid']:
            self.G.graph['n_added_uid'] += 1
            node_name = node_type + '_' + str(self.G.graph['n_added_uid'])
            self.G.graph['uid'][uid] = node_name
        else:
            print('Can not add uid since already exists.')

    def _get_origin_uid(self, origin, node_type):
        if node_type == 'AND' or node_type == 'SENSOR':
            uid = str()
        elif node_type == 'SEQ':
            uid = 'SEQ'
        origin_set = set(origin)
        for SENSOR_node in self.G.graph['node_types']['SENSOR']:
            if SENSOR_node in origin_set:
                uid += '_' + SENSOR_node
        for SEQ_node in self.G.graph['node_types']['SEQ']:
            if SEQ_node in origin_set:
                uid += '_' + SEQ_node
        return hashlib.sha256(uid.encode('utf-8')).hexdigest()

    def _get_node_uid(self, node):
        node_type = self.G.nodes[node]['node_type']
        if node_type == 'AND' or self.G.nodes[node]['node_type'] == 'SENSOR':
            origin = self.G.nodes[node]['origin']
        elif self.G.nodes[node]['node_type'] == 'SEQ':
            origin = []
            for predecessor in self.G.predecessors(node):
                if predecessor == self.G.nodes[node]['first_predecessor']:
                    origin.extend(self.G.nodes[predecessor]['origin'])
                else:
                    origin.extend(self.G.nodes[predecessor]['origin'])
            origin = list(set(origin))
        uid = self._get_origin_uid(origin, node_type)
        return uid

    def _reset_activity(self):
        self.G.graph['active_nodes'] = set()
        self.G.graph['top_active_nodes'] = []

    def _set_active_sensors(self, sensor_states):
        self._reset_activity()
        for i, state in enumerate(sensor_states):
            if state == 1:
                sensor = self.G.graph['node_types']['SENSOR'][i]
                self.G.graph['active_nodes'].add(sensor)

    def _update_activity_AND_node(self, node, predecessor_1, predecessor_2):
        if predecessor_1 in self.G.graph['active_nodes'] and predecessor_2 in self.G.graph['active_nodes']:
            self.G.graph['active_nodes'].add(node)

    def _update_activity_SEQ_node(self, node, predecessor_1, predecessor_2):
        if predecessor_1 == self.G.nodes[node]['first_predecessor']:
            first_pred = predecessor_1
            second_pred = predecessor_2
        else:
            first_pred = predecessor_2
            second_pred = predecessor_1
        if self.G.nodes[node]['stage'] == 1 and second_pred in self.G.graph['active_nodes']:
            self.G.graph['active_nodes'].add(node)
        if first_pred in self.G.graph['active_nodes']:
            self.G.nodes[node]['stage'] = 1
        else:
            self.G.nodes[node]['stage'] = 0
