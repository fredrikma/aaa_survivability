import numpy as np
import random
from .agent import Agent

class RandomAgent(Agent):
    """An agent taking random actions.
    
    Only supports discrete action space."""

    def __init__(self, action_space, sensor_names, sensor_states, info, action_names=None):
        super().__init__()
        print("RandomAgent::__init__")

        self._action_space = action_space

        self.action_frequency = np.zeros(self._action_space.n, dtype = int)
        self.action_names = [str(x) for x in range(self._action_space.n)]

    def get_action(self, state, reward, done, info, time_zero):
            
        # Take a random action
        #action = self._action_space.sample()
        action = random.choice([x for x in range(self._action_space.n)])

        # Statistics
        self.action_frequency[action] += 1

        return action