import random
import math
import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

from aaa_survivability.agents.agent import Agent


class AnimatDQN(Agent):
    """ A model using DQN working against an Animat environment.

        Inspired by code from Yash Patel
        https://towardsdatascience.com/reinforcement-learning-w-keras-openai-dqns-1eed3a5338c
        """

    def _set_default_parameters(self):
        """ Default parameter settings """

        # replay memory settings
        self.replay_memory_length = 1000
        self.replay_batch_size = 32
        self.replay_num_training_iterations = 1

        # learning parameters
        self.gamma = 0.9
        self.epsilon = 1
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.99
        self.learning_rate = 0.005

        # network parameters
        self.tau = .05             # how fast we update the target network
        self.activation = "sigmoid"
        

    def __init__(self, action_space, sensor_names, sensor_states, info, action_names=None, new_parameter_settings={}):
        super().__init__()
        self.action_space = action_space

        self.sensor_states = sensor_states
        self.sensor_names = sensor_names
        self.num_sensors = len(self.sensor_states)

        self.learning_enabled = True

        # Initialize the default parameters settings
        self._set_default_parameters()

        # And replace with optional new parameter settings
        for key, value in new_parameter_settings.items():
            setattr(self, key, value)

        # replay memory
        self.memory  = deque(maxlen=self.replay_memory_length)

        # the two models (networks that will approximate Q-values given (state,action))
        self.model        = self._create_model()
        self.target_model = self._create_model()

    # for statistics
        if 'needs' in info.keys():
            self.needs = list(info["needs"])

        if action_names == None:
            self.action_names = [str(x) for x in range(self.action_space.n)]
        else:
            self.action_names = action_names

        self.action_frequency = np.zeros(self.action_space.n, dtype=int)
        self.needs_over_time = [[] for x in range(len(self.needs))]
        for i, n in enumerate(self.needs):
            self.needs_over_time[i].append(n)

    def _create_model(self):
        model = Sequential()

        model.add(Dense(self.action_space.n, input_dim=self.num_sensors, activation=self.activation, use_bias=True))
        #model.add(Dense(10, input_dim=self.num_sensors, activation="sigmoid", use_bias=True))
        #model.add(Dense(self.action_space.n, activation="linear", use_bias=True))
        model.compile(loss="mean_squared_error",
        optimizer=Adam(lr=self.learning_rate))
            #optimizer=SGD(lr=self.learning_rate, decay=0, momentum=0.9, nesterov=True))
        
        print(model.summary())
        return model

    def _remember(self, state, action, reward, new_state, done):
        """ store a transition to replay memory (state,action) -> (new_state, reward, done) """
        self.memory.append([state, action, reward, new_state, done])

    def _replay(self):
        """ Trains the model by recalling memories from the replay buffer. """
        if len(self.memory) < self.replay_batch_size: 
            return

        samples = random.sample(self.memory, self.replay_batch_size)
        for sample in samples:
            state, action, reward, new_state, done = sample

            # We use the target model for prediction since it has more consisten goals (slower changing)
            target = self.target_model.predict(state)

            # If we are done, there is no future reward to consider
            if done:
                target[0][action] = reward
            else:
                Q_future = max(self.target_model.predict(new_state)[0])
                target[0][action] = reward + Q_future * self.gamma

            # Train the model to give the target output
            self.model.fit(state, target, epochs=self.replay_num_training_iterations, verbose=0)

    def _target_train(self):
        """ This transforms/trains the target model towards the actual model.
        
            This has to be done in a slow manner since,
            the actual model outputs are changing quite rapidly and does not provide a consistent goal,
            therefore we have have a target model that are slower in changing its goals, and then becomes a more stable target.
        """
        weights = self.model.get_weights()
        target_weights = self.target_model.get_weights()
        for i in range(len(target_weights)):
            target_weights[i] = weights[i] * self.tau + target_weights[i] * (1 - self.tau)

        self.target_model.set_weights(target_weights)

    def dump_info(self):
        """Prints useful info to console."""

        print(self.model.summary())
        print(self.model.get_weights())

    def get_action(self, sensor_states, reward, done, info, time_zero):

        # If we are talking with an Animat environment
        # we get the need/s and reward/s supplied to us.
        if 'needs' in info.keys():
            self.needs = list(info['needs'])
            self.rewards = list(info['rewards'])
        else:
            self.rewards = [reward]

        # We only support one need in this agent
        reward = self.rewards[0]
        #reward = np.sign(self.rewards[0])

        # if t > 0 and learning enabled
        if not time_zero and self.learning_enabled:

            # store the transition in replay memory
            cur_state = np.array(self.sensor_states).reshape(1, self.num_sensors)
            new_state = np.array(sensor_states).reshape(1, self.num_sensors)

            self._remember(cur_state, self.action, reward, new_state, done)

            # train main model and update the target model
            self._replay()
            self._target_train()

        # on to the new state
        self.sensor_states = sensor_states
        self.state = np.array(self.sensor_states).reshape(1, self.num_sensors)

        # select action
        self._select_action()

        # stats
        self.action_frequency[self.action] += 1
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs_over_time[i].append(n)

        return self.action

    def _select_action(self):
        if self.learning_enabled:
            # decay exploration rate
            self.epsilon *= self.epsilon_decay
            self.epsilon = max(self.epsilon_min, self.epsilon)
            
            # explore/exploit
            biased_coin = random.random()
            if biased_coin > self.epsilon:
                self.action = np.argmax(self.model.predict(self.state)[0])
            else:
                self.action = random.choice([x for x in range(self.action_space.n)])
        else: # only exploit
            self.action = np.argmax(self.model.predict(self.state)[0])
