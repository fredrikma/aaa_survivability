import gym
import numpy as np

import aaa_survivability.envs.simple.simple_3x3_env

print("random agent")

env = gym.make('simple_3x3-v0')

print("observation space = {0}".format(env.observation_space.n))
print("action space = {0}".format(env.action_space.n))

steps = []

for episode in range(10):
    env.reset()
    num_steps = 0
    while True:
        action = env.action_space.sample()
        num_steps += 1

        state, reward, done, info = env.step(action)
        if done == True:
            env.render()
            break
        if num_steps % 1000 == 0:
            print("State = {0}, Reward = {1}, Action = {2}, Info = {3}".format(state, reward, action, info))
        

    print("steps needed %ld" % num_steps)
    steps.append(num_steps)

print("Average steps = {0}".format(np.mean(steps)))

