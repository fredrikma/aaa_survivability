import gym
import numpy as np

import aaa_survivability.envs.simple.simple_3x3_env

print("Q-learning agent")

env = gym.make('simple_3x3-v0')

print("observation space = {0}".format(env.observation_space.n))
print("action space = {0}".format(env.action_space.n))

Q = np.zeros([env.observation_space.n, env.action_space.n])

alpha = 0.9

steps = []

for episode in range(100):
    num_steps = 0

    state = env.reset()
    while True:
        action = np.argmax(Q[state])
        num_steps += 1

        # execute action, get new state and measure reward
        state2, reward, done, info = env.step(action)

        # learn
        Q[state,action] += alpha * (reward + np.max(Q[state2]) - Q[state,action])
        state = state2


        if done == True:
            env.render()
            break
        if num_steps % 10 == 0:
            print("State = {0}, Reward = {1}, Action = {2}, Info = {3}".format(state, reward, action, info))
        

    print("steps needed %ld" % num_steps)
    steps.append(num_steps)

print("Average steps = {0}".format(np.mean(steps)))

print(Q)