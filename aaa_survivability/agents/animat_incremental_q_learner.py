import random
import math
import numpy as np
from aaa_survivability.agents.agent import Agent


class AnimatIncrementalQLearner(Agent):
    """A model using standard q-learning working against Animat environments.

        Work by allocating the state once it's visited, this means we can keep the state-space down.
        Used for comparison vs the Generic Animat model which uses Q-global and advanced learning rules.
        """

    def __init__(self, action_space, sensor_names, sensor_states, info, action_names=None):
        super().__init__()
        self.action_space = action_space

        self.sensor_states = sensor_states
        self.sensor_names = sensor_names

        self.action = 0
        self.state = 0

        # Enable/Disable learning
        self.learning_enabled = True

        # Parameters
        self.alpha = 0.05
        self.gamma = 0.9
        self.epsilon = 0.01

        # Q-matrix, built incrementally
        self.Q_matrix = {}
        self.Q_matrix[self.state] = np.zeros(self.action_space.n)

        # needs
        if 'needs' in info.keys():
            self.needs = list(info["needs"])

        if action_names == None:
            self.action_names = [str(x) for x in range(self.action_space.n)]
        else:
            self.action_names = action_names

        # stats
        self.action_frequency = np.zeros(self.action_space.n, dtype=int)
        self.needs_over_time = [[] for x in range(len(self.needs))]
        for i, n in enumerate(self.needs):
            self.needs_over_time[i].append(n)


    def get_action(self, sensor_states, reward, done, info, time_zero):

        # If we are talking with an Animat environment
        # we get the need/s and reward/s supplied to us.
        if 'needs' in info.keys():
            self.needs = list(info['needs'])
            self.rewards = list(info['rewards'])
        else:
            self.rewards = [reward]

        # We only support one need in this agent
        reward = self.rewards[0]

        self.sensor_states = sensor_states

        # calculate new state from sensors
        listtodec = lambda l: sum(int(c) * (2**i) for i,c in enumerate(l[::-1]))
        state2 = listtodec(self.sensor_states)

        # if learning is not enabled and the state has not been visited before we just take a random action
        if not self.learning_enabled and not state2 in self.Q_matrix:
            # Take a random action
            #self.action = self.action_space.sample()
            self.action = random.choice([x for x in range(self.action_space.n)])
            return self.action

        # allocate state if not visited before
        if not state2 in self.Q_matrix:
            self.Q_matrix[state2] = np.zeros(self.action_space.n)
        
        # learn
        if self.learning_enabled and not time_zero:
            self.Q_matrix[self.state][self.action] += self.alpha * (reward + np.max(self.Q_matrix[state2]) - self.Q_matrix[self.state][self.action])

        self.state = state2

        # take action
        if self.learning_enabled and not time_zero:
            self._select_action()
        else: # exploit policy
            max_value = np.amax(self.Q_matrix[self.state])
            best_actions = np.argwhere(
                self.Q_matrix[self.state] == max_value).flatten()
            self.action = np.random.choice(best_actions)

        # stats
        self.action_frequency[self.action] += 1
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs_over_time[i].append(n)

        return self.action

    def _select_action(self):
        biased_coin = random.random()
        if biased_coin > self.epsilon:
            max_value = np.amax(self.Q_matrix[self.state])
            best_actions = np.argwhere(
                self.Q_matrix[self.state] == max_value).flatten()
            self.action = np.random.choice(best_actions)
        else:
            #self.action = self.action_space.sample()
            self.action = random.choice([x for x in range(self.action_space.n)])


    def get_brain_complexity(self):
        return len(self.Q_matrix.keys())