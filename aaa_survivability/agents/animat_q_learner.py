import random
import math
import numpy as np
from aaa_survivability.agents.agent import Agent


class AnimatQLearner(Agent):
    """A model using standard q-learning working against Animat environments.

        Only possible against environment with not to many sensors, since 2^n states.
        Tracks accumulated reward.
        """

    def __init__(self, action_space, sensor_names, sensor_states, info, action_names=None):
        super().__init__()
        self.action_space = action_space

        self.sensor_states = sensor_states
        self.sensor_names = sensor_names

        self._action = 0

        self._alpha = 0.6
        self._gamma = 1
        self._epsilon = 0.05

        # needs
        if 'needs' in info.keys():
            self.needs = list(info["needs"])

        if action_names == None:
            self.action_names = [str(x) for x in range(self.action_space.n)]
        else:
            self.action_names = action_names

        # sensor state-space to full state space
        num_sensors = len(self.sensor_states)
        num_states = 2**num_sensors

        self.Q_matrix = np.zeros([num_states, self.action_space.n])
        self._state = 0

        # stats
        self.action_frequency = np.zeros(self.action_space.n, dtype=int)
        self.needs_over_time = [[] for x in range(len(self.needs))]
        for i, n in enumerate(self.needs):
            self.needs_over_time[i].append(n)


    def get_action(self, sensor_states, reward, done, info, time_zero):

        # If we are talking with an Animat environment
        # we get the need/s and reward/s supplied to us.
        if 'needs' in info.keys():
            self.needs = list(info['needs'])
            self.rewards = list(info['rewards'])
        else:
            self.rewards = [reward]

        # We only support one need in this agent
        reward = self.rewards[0]

        self.sensor_states = sensor_states

        # calculate new state from sensors
        listtodec = lambda l: sum(int(c) * (2**i) for i,c in enumerate(l[::-1]))
        state2 = listtodec(self.sensor_states)

        # learn
        self.Q_matrix[self._state, self._action] += self._alpha * (reward + np.max(self.Q_matrix[state2]) - self.Q_matrix[self._state,self._action])
        self._state = state2

        # take action
        self._select_action()

        # stats
        self.action_frequency[self._action] += 1
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs_over_time[i].append(n)

        #print(self.sensor_states)
        #print(self._action)
        #print('......................')

        return self._action

    def _select_action(self):
        biased_coin = random.random()
        if biased_coin > self._epsilon:
            max_value = np.amax(self.Q_matrix[self._state])
            best_actions = np.argwhere(
                self.Q_matrix[self._state] == max_value).flatten()
            self._action = np.random.choice(best_actions)
        else:
            self._action = self.action_space.sample()