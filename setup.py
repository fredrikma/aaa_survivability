from setuptools import setup, find_packages

import aaa_survivability

setup(name='aaa_survivability',
      version='0.0.1',
      install_requires=['gym', 'scipy', 'matplotlib', 'graphviz', 'networkx', 'pandas', 'keras'],
      packages=find_packages()
) 