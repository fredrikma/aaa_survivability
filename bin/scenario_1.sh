#!/bin/sh

#echo "Collecting and generating statistical data for Yoshida scenario 1."

PYTHON=python3
SCENARIO_BIN=../aaa_survivability/scenarios/no_gui/yoshida_1.py
SCENARIO_STAT_COMBINED=../aaa_survivability/statistics/yoshida/yoshida_1_combined.py
SCENARIO_STAT_SINGLE=../aaa_survivability/statistics/yoshida/yoshida_1_single.py

ANIMAT_FILE=../aaa_survivability/agents/animat/animat.py
ANIMAT_BASELINE_FILE=../aaa_survivability/agents/animat/animat_baseline.py

a="5000"
b="100"
c="100"

# a=total, b=training, c=evaluation
while getopts  "a:b:c:" flag
do
    case $flag in
        a) a=$OPTARG;;
        b) b=$OPTARG;;
        c) c=$OPTARG;;
    esac
done

outputpath="../data"

time_stamp=$(date +%Y_%m_%d_%H_%M_%S)

outdir=${outputpath}/${time_stamp}/

echo "Creating directory "${outdir}
mkdir -p ${outdir}

# run scenario and generate data
echo "Running scenario..."
${PYTHON} ${SCENARIO_BIN} ${a} ${b} ${c} ${outdir}

# collect data and present statistics
echo "Generating statistics..."
mkdir -p ${outdir}/"images"
${PYTHON} ${SCENARIO_STAT_COMBINED} ${outdir} ${outdir}/"images/"
${PYTHON} ${SCENARIO_STAT_SINGLE} ${outdir} ${outdir}/"images/" AnimatBaseline
${PYTHON} ${SCENARIO_STAT_SINGLE} ${outdir} ${outdir}/"images/" Animat
${PYTHON} ${SCENARIO_STAT_SINGLE} ${outdir} ${outdir}/"images/" RandomAgent
${PYTHON} ${SCENARIO_STAT_SINGLE} ${outdir} ${outdir}/"images/" AnimatIncrementalQLearner

# copy agents so we know what settings/version we used
cp ${ANIMAT_FILE} ${outdir}/
cp ${ANIMAT_BASELINE_FILE} ${outdir}/

echo "done. Output can be found in : "${outdir}