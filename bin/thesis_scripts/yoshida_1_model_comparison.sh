#!/bin/sh

#echo "Collecting and generating statistical data for Yoshida scenario 1."

PYTHON=python3
SCENARIO_BIN=../../aaa_survivability/scenarios/no_gui/thesis_scenarios/yoshida_1_model_comparison.py
SCENARIO_STAT_COMBINED=../../aaa_survivability/statistics/thesis_statistics/yoshida_1_combined.py
SCENARIO_STAT_SINGLE=../../aaa_survivability/statistics/thesis_statistics/yoshida_1_single.py

ANIMAT_BASELINE_FILE=../../aaa_survivability/agents/animat/animat_baseline.py
ENVIRONMENT_FILE=../../aaa_survivability/envs/animat/yoshida_1_env.py
GENERATE_DATA_FILE=../../aaa_survivability/scenarios/no_gui/thesis_scenarios/yoshida_1_model_comparison.py

a="5000"
b="100"
c="100"

# a=total, b=training, c=evaluation
while getopts  "a:b:c:" flag
do
    case $flag in
        a) a=$OPTARG;;
        b) b=$OPTARG;;
        c) c=$OPTARG;;
    esac
done

outputpath="../../data/thesis_results/yoshida_1_model_comparison"

time_stamp=$(date +%Y_%m_%d_%H_%M_%S)

outdir=${outputpath}/${time_stamp}/

echo "Creating directories"
mkdir -p ${outdir}
mkdir -p ${outdir}/"raw_data"
mkdir -p ${outdir}/"images"
mkdir -p ${outdir}/"modules"

# copy modules so we know what settings/version that was used
cp ${ANIMAT_BASELINE_FILE} ${outdir}/"modules"
cp ${ENVIRONMENT_FILE} ${outdir}/"modules"
cp ${GENERATE_DATA_FILE} ${outdir}/"modules"

# run scenario and generate data
echo "Running scenario..."
${PYTHON} ${SCENARIO_BIN} ${a} ${b} ${c} ${outdir}"raw_data/"

# collect data and present statistics
echo "Generating statistics..."
${PYTHON} ${SCENARIO_STAT_COMBINED} ${outdir}"raw_data/" ${outdir}/"images/"
${PYTHON} ${SCENARIO_STAT_SINGLE} ${outdir}"raw_data/" ${outdir}/"images/" AnimatBaseline
${PYTHON} ${SCENARIO_STAT_SINGLE} ${outdir}"raw_data/" ${outdir}/"images/" RandomAgent
${PYTHON} ${SCENARIO_STAT_SINGLE} ${outdir}"raw_data/" ${outdir}/"images/" AnimatIncrementalQLearner

echo "done. Output can be found in : "${outdir}