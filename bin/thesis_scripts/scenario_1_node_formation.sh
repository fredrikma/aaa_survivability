#!/bin/sh

#echo "Collecting and generating statistical data for scenario 1 node formation."

PYTHON=python3
ANIMAT_FILE=../../aaa_survivability/agents/animat/animat_baseline.py
ENVIRONMENT_FILE=../../aaa_survivability/envs/animat/thesis_1_env.py
GENERATE_DATA_FILE=../../aaa_survivability/scenarios/no_gui/thesis_scenarios/scenario_1_node_formation.py
GENERATE_STATISTICS_FILE=../../aaa_survivability/statistics/thesis_statistics/scenario_1_node_formation_statistics.py

outputpath="../../data/thesis_results/scenario_1_node_formation"

time_stamp=$(date +%Y_%m_%d_%H_%M_%S)

outdir=${outputpath}/${time_stamp}/

echo "Creating directories"
mkdir -p ${outdir}
mkdir -p ${outdir}/"raw_data"
mkdir -p ${outdir}/"images"
mkdir -p ${outdir}/"modules"

# copy modules so we know what settings/version that was used
cp ${ANIMAT_FILE} ${outdir}/"modules"
cp ${ENVIRONMENT_FILE} ${outdir}/"modules"
cp ${GENERATE_DATA_FILE} ${outdir}/"modules"
cp ${GENERATE_STATISTICS_FILE} ${outdir}/"modules"

# run scenario and generate data
echo "Running scenario..."
${PYTHON} ${GENERATE_DATA_FILE} ${outdir}"raw_data/"

# collect data and present statistics
echo "Generating statistics..."
${PYTHON} ${GENERATE_STATISTICS_FILE} ${outdir}"raw_data/" ${outdir}"images/"

echo "done. Output can be found in : "${outdir}