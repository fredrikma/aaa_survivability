# Dynamic State Representation for Homeostatic Agents
Master thesis at Chalmers University of Technology

This project is work in progress and contains implementation and additions to the Generic Animat model
proposed by Strannegård et al. in

**The Animat Path to Artificial General Intelligence**: http://cadia.ru.is/workshops/aga2017/proceedings/AGA_2017_Strannegard_et_al.pdf


## Contributors (current)
Fredrik Mäkeläinen

Hampus Torén

Claes Strannegård


# Installation
Development is done using Ubuntu 16.04 and VMware Workstation Player 14, 
but any modern linux environment should suffice.

## Native packages
```
sudo apt-get install git python3-pip python3-tk python3-venv graphviz
```

## Clone repository
```
git clone https://gitlab.com/fredrikma/aaa_survivability.git
```

## Create and activate a virtual environment
```
python3 -m venv my_animat_env
source my_animat_env/bin/activate
```

## Install a backend for keras (optional)
Follow https://www.tensorflow.org/install/install_linux or
```
pip install tensorflow
```

## Install the project
```
cd aaa_survivability
pip install -e .
```

## Run an example
```
cd aaa_survivability/scenarios
python scenario_cat_3x3.py
```

## Run the code used in article submission
```
git checkout Thesis_results
cd aaa_survivability/bin/thesis_scripts
./scenario_1_model_comparison.sh
```