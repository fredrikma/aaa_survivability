import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
import time
import math
from os import path
import pyglet

from aaa_survivability.envs.animat.animat_env import AnimatEnv
from aaa_survivability.envs.animat.renderer import Renderer


class Yoshida1Env(AnimatEnv):
    """Tilebased 3x3 discrete environment.

    Inspired by Yoshida scenario 1, where A=FOOD, B=POISON.

    Actions:
        0 = Up
        1 = Down
        2 = Left
        3 = Right
        4 = Eat

    Sensors:
        Current tile for FOOD and POISON 
        FOOD in direction Up, Down, Left, Right
        FOOD distance X (1,2), relative
        FOOD distance Y (1,2), relative
        POISON in direction Up, Down, Left, Right
        POISON distance X (1,2), relative
        POISON distance Y (1,2), relative
        Animat position, 3+3, (0,1,2) (0,1,2), absolute
        Discretized Energy
        Total number of sensors: 1+1+8+8+6+20=44

    Goal:
            Keeping the energy E at 60, E in [0,100]
            Don't eat POISON
            The NEED will be 1.0 at E = 60, and decrease the further away we are.

    Rules:
        E starts at 60
        E decreases -1 at every time step.

        B randomly changes position with P = 0.01

        Eating A    = +5 to energy

        Eating B    = -5 to energy            # We need to convey that this is bad to the animat
                    : increased chance of death
        
        Eating empty =  +0 to energy

        Movement = +0 to energy

        A temporal survivability function is used to decide if the agent is alive.
        f(E_t) = exp( -(E_t - 60)^2 / 1000 )
        
        C_t = 
            : 1 if B eaten at time t
            : 0 Otherwise

        g(C_t)  = 
                : 0.5 if C_t = 1
                : 1.0 Otherwise

        P(Alive | E_t, C_t) = f(E_t) * g(C_(t-1))
        Remark: We check C_t for the previous timestep,
        this is to give the animat a chance to learn from its mistakes if it eats poison before it gets killed
    """
    ACTION_NAMES = ["Up", "Down", "Left", "Right", "Eat"]
    NUM_ACTIONS = len(ACTION_NAMES)

    ENERGY_LIMITS = (0,100)
    TARGET_ENERGY = 60
    
    ENERGY_EAT_FOOD     = 5
    ENERGY_EAT_POISON   = -15
    ENERGY_EAT_EMPTY    = -1
    ENERGY_MOVE         = -1
    ENERGY_TIME_STEP    = 0 #TODO: This is now different from first results we have, necessary wince we decoupled needs and reward

    USE_PROB_TERMINATION = True
    PROB_MOVE_POISON = 0.01

    # Sensors for Energy >= x
    #ENERGY_SENSORS = [0,59] #minimal that also works
    ENERGY_SENSORS = [0,35,40,45,50,55,56,57,58,59,60,61,62,63,64,65,70,75,85,90]
    ENERGY_SENSOR_NAMES = ['ENERGY_' + str(int(x)) for x in ENERGY_SENSORS]
    N_ENERGY_SENSORS = len(ENERGY_SENSORS)

    SENSOR_NAMES = ["FOOD", "POISON", 
                    "FOOD Dir-Up", "FOOD Dir-Down", "FOOD Dir-Left", "FOOD Dir-Right",
                    "FOOD X (1)", "FOOD X (2)", "FOOD Y (1)", "FOOD Y (2)",
                    "POISON Dir-Up", "POISON Dir-Down", "POISON Dir-Left", "POISON Dir-Right",
                    "POISON X (1)", "POISON X (2)", "POISON Y (1)", "POISON Y (2)",
                    "POS X (0)",  "POS X (1)",  "POS X (2)", 
                    "POS Y (0)",  "POS Y (1)",  "POS Y (2)"]

    N_NOT_ENERGY_SENSORS = len(SENSOR_NAMES)
    SENSOR_NAMES.extend(ENERGY_SENSOR_NAMES)

    NUM_SENSORS = len(SENSOR_NAMES)

    metadata = {
        'render.modes': ['human', 'rgb_array']
    }

    # movement actions
    actions_move = {0: [0, 1],
                    1: [0, -1],
                    2: [-1, 0],
                    3: [1, 0]}

    num_tiles_x = 3
    num_tiles_y = 3
    screen_width = 600
    screen_height = 600

    tile_width = screen_width / num_tiles_x
    tile_height = screen_height / num_tiles_y

    class TileObject(object):
        def __init__(self, sprite, x=0, y=0):
            self.sprite = sprite
            self.tile_pos = np.array([x, y], dtype=np.int64)

        @property
        def tile_pos(self):
            return self.__tile_pos

        @tile_pos.setter
        def tile_pos(self, p):
            self.__tile_pos = p
            self.sprite.position = (
                self.tile_pos[0] * Yoshida1Env.tile_width, self.tile_pos[1] * Yoshida1Env.tile_height)

    class Body(TileObject):
        def __init__(self, sprite, x=0, y=0):
            super().__init__(sprite, x, y)

            self.reset()

        def reset(self):
            self.energy = Yoshida1Env.TARGET_ENERGY
            self.food_eaten = 0
            self.poison_eaten = 0
 
            self.reset_sensors()

        def reset_sensors(self):
            self.sensors = [0 for x in range(Yoshida1Env.NUM_SENSORS)]

        def energy_to_needs(self):
            diff = math.fabs(Yoshida1Env.TARGET_ENERGY - self.energy)
            #need = max(1.0 - diff / Yoshida1Env.TARGET_ENERGY, 0.0)
            need = 1.0 - diff / Yoshida1Env.TARGET_ENERGY
            return [need]

    class Food(TileObject):
        def __init__(self, sprite, x=0, y=0):
            super().__init__(sprite, x, y)

    class Poison(TileObject):
        def __init__(self, sprite, x=0, y=0):
            super().__init__(sprite, x, y)

    def __init__(self):
        self.action_space = spaces.Discrete(Yoshida1Env.NUM_ACTIONS)
        self.observation_space = spaces.Discrete(Yoshida1Env.NUM_SENSORS)

        self.viewer = None

    # sprites
        # background
        fname = path.join(path.dirname(__file__),
                          "assets/green_tile.png")
        image = pyglet.image.load(fname)
        scale_x = Yoshida1Env.tile_width / image.width
        scale_y = Yoshida1Env.tile_height / image.height

        self.sprite_batch_background = pyglet.graphics.Batch()
        self.sprite_background = []

        pos_y = 0
        for _ in range(Yoshida1Env.num_tiles_y):
            pos_x = 0
            for _ in range(Yoshida1Env.num_tiles_x):
                self.sprite_background.append(pyglet.sprite.Sprite(
                    image, pos_x, pos_y, subpixel=True, batch=self.sprite_batch_background))
                self.sprite_background[-1].scale_x = scale_x
                self.sprite_background[-1].scale_y = scale_y

                pos_x += Yoshida1Env.tile_width
            pos_y += Yoshida1Env.tile_height

        # cat
        fname = path.join(path.dirname(__file__), "assets/cat.png")
        image = pyglet.image.load(fname)
        scale_x = Yoshida1Env.tile_width / image.width
        scale_y = Yoshida1Env.tile_height / image.height

        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        self.body = Yoshida1Env.Body(sprite, 0, 0)

        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        # food
        fname = path.join(path.dirname(__file__), "assets/fish.png")
        image = pyglet.image.load(fname)
        scale_x = Yoshida1Env.tile_width / image.width
        scale_y = Yoshida1Env.tile_height / image.height

        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        self.food = Yoshida1Env.Food(sprite, 2, 1)

        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        # poison
        fname = path.join(path.dirname(__file__), "assets/frog.png")
        image = pyglet.image.load(fname)
        scale_x = Yoshida1Env.tile_width / image.width
        scale_y = Yoshida1Env.tile_height / image.height

        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        self.poison = Yoshida1Env.Poison(sprite, 2, 2)

        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        self.seed()
        self.reset()

    def _get_initial_state(self):
        """Get the initial state."""

        self._update_sensors()

        info = {"needs": self.body.energy_to_needs(), 
                "rewards": [0],
                "food_eaten": self.body.food_eaten,
                "poison_eaten": self.body.poison_eaten, 
                "energy": self.body.energy}

        return (self.body.sensors, info)

    def _limit_coordinates(self, coord):
        coord[0] = min(coord[0], Yoshida1Env.num_tiles_x - 1)
        coord[0] = max(coord[0], 0)
        coord[1] = min(coord[1], Yoshida1Env.num_tiles_y - 1)
        coord[1] = max(coord[1], 0)
        return coord

    def _action_move(self, action):
        diff = Yoshida1Env.actions_move[action]
        tmp = self.body.tile_pos + diff
        self.body.tile_pos = self._limit_coordinates(tmp)

        self.body.energy += Yoshida1Env.ENERGY_MOVE

    def _action_eat(self):
        if (self.body.tile_pos == self.food.tile_pos).all():
            #print("Eating food")
            self.body.food_eaten += 1
            self.body.energy += Yoshida1Env.ENERGY_EAT_FOOD

            # respawn food at a free tile (one where there is no poison)
            self.food.tile_pos = self._get_random_tile([self.poison.tile_pos])

        elif (self.body.tile_pos == self.poison.tile_pos).all():
            #print("Eating poison")
            self.body.poison_eaten += 1
            self.body.energy += Yoshida1Env.ENERGY_EAT_POISON

            # Probability for death increased
            self.C_t = True

            # respawn poison at a free tile (one where there is no food)
            self.poison.tile_pos = self._get_random_tile([self.food.tile_pos])
        else:
            self.body.energy += Yoshida1Env.ENERGY_EAT_EMPTY

    def _get_random_tile(self, excluded=[]):
        while True:
            rx = np.random.randint(Yoshida1Env.num_tiles_x)
            ry = np.random.randint(Yoshida1Env.num_tiles_y)

            invalid = False
            for x in excluded:
                if rx == x[0] and ry == x[1]:
                    invalid = True
                    break
            if not invalid:
                break

        return np.array([rx, ry], dtype=np.int64)

    def seed(self, seed=None):
        seed = seeding.np_random(seed)
        return [seed]

    def _update_sensors(self):
        """"Sense the environment."""

        food_diff = self.food.tile_pos - self.body.tile_pos
        poison_diff = self.poison.tile_pos - self.body.tile_pos
        
        # current tile
        if np.all(food_diff == 0):
            self.body.sensors[0] = 1
        if np.all(poison_diff == 0):
            self.body.sensors[1] = 1

        # FOOD direction, relative to body
        if food_diff[1] > 0: # Up
            self.body.sensors[2] = 1    
        elif food_diff[1] < 0: # Down
            self.body.sensors[3] = 1    
        if food_diff[0] < 0: # Left
            self.body.sensors[4] = 1    
        elif food_diff[0] > 0: # Right
            self.body.sensors[5] = 1    
            
        # FOOD X, relative to body
        if abs(food_diff[0]) == 1:
            self.body.sensors[6] = 1    
        elif abs(food_diff[0]) == 2:
            self.body.sensors[7] = 1
        # FOOD Y, relative to body
        if abs(food_diff[1]) == 1:
            self.body.sensors[8] = 1    
        elif abs(food_diff[1]) == 2:
            self.body.sensors[9] = 1

        # POISON direction, relative to body
        if poison_diff[1] > 0: # Up
            self.body.sensors[10] = 1    
        elif poison_diff[1] < 0: # Down
            self.body.sensors[11] = 1    
        if poison_diff[0] < 0: # Left
            self.body.sensors[12] = 1    
        elif poison_diff[0] > 0: # Right
            self.body.sensors[13] = 1    
            
        # POISON X, relative to body
        if abs(poison_diff[0]) == 1:
            self.body.sensors[14] = 1    
        elif abs(poison_diff[0]) == 2:
            self.body.sensors[15] = 1
        # POISON Y, relative to body
        if abs(poison_diff[1]) == 1:
            self.body.sensors[16] = 1    
        elif abs(poison_diff[1]) == 2:
            self.body.sensors[17] = 1
        
        # POS X
        self.body.sensors[18 + self.body.tile_pos[0]] = 1
        # POS X
        self.body.sensors[21 + self.body.tile_pos[1]] = 1


        # Energy sensors
        idx = len(Yoshida1Env.ENERGY_SENSORS) - 1
        for limit in Yoshida1Env.ENERGY_SENSORS[::-1]:
            if self.body.energy >= limit:
                self.body.sensors[Yoshida1Env.N_NOT_ENERGY_SENSORS + idx] = 1
                break
            idx -= 1

    def _is_alive(self):
        f = math.exp( -(self.body.energy - self.TARGET_ENERGY)**2 / 1000.0)
        g = 1.0
        if self.C_t:
            g = 0.5

        r = np.random.random()
        if r < f * g:
            return True
        
        return False

    def step(self, action):

        # Evaluate terminal condition, uses C_t from last timestep in order to give the animat a chance to learn
        if Yoshida1Env.USE_PROB_TERMINATION and not self._is_alive():
            self.done = True

        reward = self.body.energy_to_needs()[0]

        # No poison eaten (yet) this time step
        self.C_t = False

        # take action
        if action == 0 or action == 1 or action == 2 or action == 3:
            self._action_move(action)
        elif action == 4:
            self._action_eat()

        # Cap the energy
        self.body.energy = min(max(Yoshida1Env.ENERGY_LIMITS[0], self.body.energy), Yoshida1Env.ENERGY_LIMITS[1])
        
        # Move poison randomly
        r = np.random.random()
        if r < Yoshida1Env.PROB_MOVE_POISON:
            self.poison.tile_pos = self._get_random_tile([self.food.tile_pos])

        # sensors
        self.body.reset_sensors()
        self._update_sensors()

        state = list(self.body.sensors)

        # reward
        reward = self.body.energy_to_needs()[0] - reward

        # Energy change at each time step
        self.body.energy += Yoshida1Env.ENERGY_TIME_STEP

        info = {"needs": self.body.energy_to_needs(), 
                "rewards" : [reward], 
                "food_eaten": self.body.food_eaten,
                "poison_eaten": self.body.poison_eaten, 
                "energy": self.body.energy}

        # no explicit reward gives, temporal difference in needs should be used
        return (state, 0, self.done, info)

    def reset(self):

        self.done = False
        self.C_t = False
        
        self.body.reset()

        # randomize starting positions
        self.body.tile_pos = self._get_random_tile()
        self.food.tile_pos = self._get_random_tile()
        self.poison.tile_pos = self._get_random_tile([self.food.tile_pos])

        # return the initial state
        return self._get_initial_state()

    def _get_viewer(self):

        if self.viewer is None:
            self.viewer = Renderer(
                Yoshida1Env.screen_width, Yoshida1Env.screen_height)

            self.viewer.add_drawable(self.sprite_batch_background)
            self.viewer.add_drawable(self.food.sprite)
            self.viewer.add_drawable(self.poison.sprite)
            self.viewer.add_drawable(self.body.sprite)

        return self.viewer

    def render(self, mode='human', close=False):

        if close:
            self.done = True
            return

        viewer = self._get_viewer()

        if mode == 'human':
            self.done = not viewer.render()
        elif mode == 'rgb_array':
            return viewer.render(True)
        else:
            raise NotImplementedError

    def close(self):
        print("render::close")
        if self.viewer:
            self.viewer.close()


if __name__ == '__main__':
    import aaa_survivability.envs.animat

    env = gym.make('yoshida_1_no_limit-v0')
    env.reset()
    aaa_survivability.envs.animat.yoshida_1_env.Yoshida1Env.USE_PROB_TERMINATION = False
    aaa_survivability.envs.animat.yoshida_1_env.Yoshida1Env.ENERGY_LIMITS = (-100000,100)

    print(Yoshida1Env.SENSOR_NAMES)

    done = False
    while not done:
        action = env.action_space.sample()
        state, reward, done, info = env.step(action)
        print(info["energy"], info["needs"], state)
        print(action, reward)
        print('......................')

        env.render()
        time.sleep(1/10)

    env.close()
    print("Finished")
