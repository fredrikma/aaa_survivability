import random
import collections
import itertools
import numpy as np
from aaa_survivability.agents.agent import Agent
from aaa_survivability.agents.animat.perception_graph import PerceptionGraph


class AnimatBaseline(Agent):
    """The Generic Animat model."""

    def _default_parameters(self):

        # Enable/Disable learning
        self.learning_enabled = True

        # use_explore can only be used if a need exists!
        self.use_explore = False
        self.explore_rate = 1
        self.sustainability_horizon = 5
        self.sustainability_maximum = 1e9

        # epsilon is active if use_explore = False
        self.epsilon = 0.01

        # Exploit parameter
        self.exploit_discount_rate = 1.0

        # Probabilistic merge
        self.use_probabilistic_merge = False
        self.probabilistic_merge_prob = 0.002

        # Emotional merge
        self.use_emotional_merge = True

        # Effect surprise parameters
        self.use_effect_surprise = self.use_emotional_merge
        self.surprise_min_updates = 1
        self.surprise_threshold = 0.12

        # Stable node merge
        self.use_stable_node_merge = True
        self.stable_threshold = 20

        # Relevant node merge
        self.use_relevant_node_merge = True
        self.relevant_min_updates = 50
        self.relevant_threshold = 0.9

        # Q-learning parameters
        self.start_alpha = 0.05
        self.alpha_decay = 1.0
        self.min_alpha = 0.05
        self.gamma = 1.0
        self.initial_reliability = 1.0
        self.use_average_q = False
        self.use_reliability_one = False

        # reward based merge
        self.use_reward_based_merge = False
        self.prob_reward_based_merge = 1.0
        self.reward_based_threshold = 3

    def __init__(self, action_space, sensor_names, sensor_states, info, action_names=None, new_parameter_settings={}):
        super().__init__()
        self.action_space = action_space
        self.sensor_states = sensor_states
        self.sensor_names = sensor_names
        self._action = 0

        # needs and rewards
        if 'needs' in info.keys():
            self.initial_needs = list(info["needs"])
            self.needs = list(info["needs"])
            self.rewards = np.zeros(len(self.needs))
        else:
            self.rewards = np.zeros(1)

        if action_names == None:
            self.action_names = [str(x) for x in range(self.action_space.n)]
        else:
            self.action_names = action_names

        # create perception graph
        self.pg = PerceptionGraph()
        [self.pg.add_SENSOR_node(sensor) for sensor in sensor_names]
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()

        # Initialize the default parameters settings
        self._default_parameters()

        # Use new parameter settings
        for key, value in new_parameter_settings.items():
            setattr(self, key, value)

        # Information that can be queried for
        self.information = None
        self.information_variable = None

        # Explore
        if self.use_explore:
            self.sustainability = 0
            self.well_being = []
            self._explore = np.zeros(self.action_space.n)

        # Probabilistic merge
        if self.use_probabilistic_merge:
            self.comb = {}

        # Effect surprise
        if self.use_effect_surprise:
            self._surprise_values = []
            for _ in self.rewards:
                self._surprise_values.append({})

        # Look ahead table, per need
        self.look_ahead = [{} for _ in range(len(self.rewards))]

        # Stable node merge
        self.stable_nodes = []
        self.stable_node_counter = 0
        self.possible_stable_nodes = []
        self.stable_nodes_on_hold = []
        for i in range(len(self.rewards)):
            self.stable_nodes.append({})
            self.possible_stable_nodes.append({})
            self.stable_nodes_on_hold.append({})
            for b in self.sensor_names:
                self.possible_stable_nodes[i][b] = [set(), 0]
                for a in range(self.action_space.n):
                    self.possible_stable_nodes[i][b][0].add(a)

        # Reward history
        self.use_reward_history = self.use_reward_based_merge or self.use_stable_node_merge
        self.reward_history = []
        for i in range(len(self.needs)):
            self.reward_history.append({})
            for b in self.sensor_names:
                self.reward_history[i][b] = {}
                for a in range(self.action_space.n):
                    self.reward_history[i][b][a] = [0, 0]

        # Relevant nodes        
        self.relevant_nodes = [{} for _ in range(len(self.rewards))]        
        self.relevant_node_counter = 0        

        # Learning rate
        self.alpha = self.start_alpha

        # Create Q and R matrices
        self.Q_matrices = []
        self.R_matrices = []
        self._R_mean = []
        self._R_std = []
        self._R_count = [] # this is also used as a top activity counter in other places
        self._R_M2 = []
        for _ in self.rewards:
            Q = {}
            R = {}
            R_mean = {}
            R_std = {}
            R_count = {}
            R_M2 = {}
            for b in self.sensor_names:
                Q[b] = np.zeros(self.action_space.n)
                R[b] = np.full(self.action_space.n,
                               self.initial_reliability, dtype='float')
                R_mean[b] = np.zeros(self.action_space.n)
                R_std[b] = np.zeros(self.action_space.n)
                R_count[b] = np.zeros(self.action_space.n)
                R_M2[b] = np.zeros(self.action_space.n)
            self.Q_matrices.append(Q)
            self.R_matrices.append(R)
            self._R_mean.append(R_mean)
            self._R_std.append(R_std)
            self._R_count.append(R_count)
            self._R_M2.append(R_M2)

        # Global Q matrix
        self._global_Q = np.zeros((len(self.rewards), self.action_space.n))

        # Exploit
        self._exploit = np.zeros(self.action_space.n)

        # Utility
        self._utility = np.zeros(self.action_space.n)

        # Initialize statistics
        self.reset_statistics()

    def _update_relevant_nodes(self, pre_active, active, action):
        """ Update the relevant nodes
        
            If one of the active nodes is a relevant node then the relevant nodes will be updated.            
            The first relevant node is always a stable node, more nodes are added as they prove that 
            they are relevant for the stable node.
        """

        set_of_active_nodes = set(active)                
        # For all needs        
        for i in range(len(self.rewards)):
            active_relevant_nodes = set_of_active_nodes.intersection(self.relevant_nodes[i].keys())
            for relevant_node in active_relevant_nodes:
                self.relevant_nodes[i][relevant_node][1][action] += 1.0
                for b in pre_active:    
                    if b != relevant_node: 
                        # Check if the node is not already represented
                        b_origin = set(self.pg.G.nodes[b]['origin'])
                        already_represented = False 
                        for b1 in self.relevant_nodes[i].keys():
                            b1_origin = set(self.pg.G.nodes[b1]['origin'])
                            if b1_origin.issubset(b_origin):
                                already_represented = True

                        # Continue only if it is a new potential relevant node
                        if not already_represented:
                            if b in self.relevant_nodes[i][relevant_node][0]:
                                self.relevant_nodes[i][relevant_node][0][b][action] += 1.0
                            else:
                                self.relevant_nodes[i][relevant_node][0][b] = np.zeros(self.action_space.n, dtype=float)
                                self.relevant_nodes[i][relevant_node][0][b][action] += 1.0
                            
                            # Check if condition for relevant node is fulfilled
                            is_relevant = False
                            if self.relevant_nodes[i][relevant_node][0][b][action] % self.relevant_min_updates == 0 and self.relevant_nodes[i][relevant_node][0][b][action] > 0:                                                
                                b_count = self.relevant_nodes[i][relevant_node][0][b][action]
                                tot_count = self.relevant_nodes[i][relevant_node][1][action]                        
                                factor = b_count / tot_count
                                if factor > self.relevant_threshold:                                                           
                                    is_relevant = True
                            if is_relevant:
                                # Add node to relevant nodes
                                if b not in self.relevant_nodes[i].keys():
                                    self.relevant_nodes[i][b] = [{}, np.zeros(self.action_space.n, dtype=float), set()]
    
    def _relevant_node_merge(self, active_nodes, top_active_nodes):
        """ Connects all top active nodes to all relevant nodes that are active. The stable nodes are 
        disregarded here since they are updated in _stable_node_merge
            
        """

        for i in range(len(self.rewards)):
            active_source_nodes = [
                x for x in active_nodes if x in self.relevant_nodes[i].keys() and x not in self.stable_nodes[i].keys()]
            for active_source_node in active_source_nodes:
                for b in top_active_nodes:
                    if b not in self.relevant_nodes[i][active_source_node][2] and b != active_source_node:
                        self.relevant_nodes[i][active_source_node][2].add(b)                        
                        new_node = self.pg.add_AND_node(
                            [active_source_node, b])
                        if new_node is not None:
                            print("Formed {}: '{}' AND '{}' : (Relevant node merge)".format(
                                new_node, active_source_node, b))
                            self._add_new_node(new_node)
                            self.relevant_node_counter += 1

    def _update_reward_history(self, pre_active_nodes, rewards, action):
        """ Update the reward history        
            Keeps track of all negative and postive that has been received for all active nodes. 
         """
        for i in range(len(self.rewards)):
            sign_of_reward = int(rewards[i] < 0)
            for b in pre_active_nodes:
                self.reward_history[i][b][action][sign_of_reward] += 1

    def _reset_reward_history_new_node_formed(self, need, actions, predecessors):
        """ Reset the entries for the predecessors to the new node
            
            If the reward history for a predecessor is conflicting the history is reset, since it is 
            expected that a new behaviour will follow.            
        """

        for predecessor in predecessors:
            for action in actions:
                if self.reward_history[need][predecessor][action][0] > 0 and self.reward_history[need][predecessor][action][1] > 0:
                    self.reward_history[need][predecessor][action] = [0, 0]
        

    def _update_stable_nodes(self, pre_active_nodes, rewards, action):
        """ Updates the possible stable nodes.

            Keeps track of all nodes that have only recieved a positive reward for at least one action.
            If a negative reward is received that node is permenantly removed from the table.
            One table per need, action, structured as:
                {node_1: number of positive rewards,
                 node_2: number of positive rewards,                 
                }
        """
        
        for i in range(len(self.rewards)):            
            # Check if a stable node is active and save its origin for a condition futher down
            if rewards[i] >= 0:
                active_stable_nodes_intersect = list(set(self.stable_nodes[i].keys()).intersection(pre_active_nodes))
                stable_nodes_origin = []
                active_stable_nodes = []
                if len(active_stable_nodes_intersect) > 0:
                    for active_stable_node in active_stable_nodes_intersect:
                        if action in self.stable_nodes[i][active_stable_node][0]:
                            active_stable_nodes.append(active_stable_node)
                            stable_nodes_origin.append(set(self.pg.G.nodes[active_stable_node]['origin']))

            # Main loop to update possible_stable_nodes and stable_nodes
            for b in pre_active_nodes:
                if b in self.possible_stable_nodes[i]:
                    if action in self.possible_stable_nodes[i][b][0]:
                        if rewards[i] < 0:
                            self.possible_stable_nodes[i][b][0].remove(action)
                            if len(self.possible_stable_nodes[i][b][0]) == 0:
                                del self.possible_stable_nodes[i][b]
                            if b in self.stable_nodes[i].keys():
                                if action in self.stable_nodes[i][b][0]:
                                    if len(self.stable_nodes[i][b][0]) == 1:
                                        del self.stable_nodes[i][b]
                                    else:
                                        self.stable_nodes[i][b][0].remove(
                                            action)
                                    # Add all nodes on hold to possible stable nodes again
                                    if b in self.stable_nodes_on_hold[i]:
                                        if action in self.stable_nodes_on_hold[i][b]:
                                            for node_on_hold in self.stable_nodes_on_hold[i][b][action]:
                                                if node_on_hold in self.possible_stable_nodes[i]:
                                                    self.possible_stable_nodes[i][node_on_hold][0].add(
                                                        action)
                                                else:
                                                    self.possible_stable_nodes[i][node_on_hold] = [set(action), 0]
                        else:    
                            # Check if the positive reward is due to a stable node being active  
                            if len(active_stable_nodes) > 0 and b not in active_stable_nodes:
                                b_origin = set(self.pg.G.nodes[b]['origin'])
                                is_valid = False
                                for indx, active_stable_node in enumerate(active_stable_nodes):                                    
                                    if b_origin.issubset(stable_nodes_origin[indx]):
                                        # Only valid as a new stable node if it is a subset of any of the active stable nodes
                                        is_valid = True                                        
                                if not is_valid:
                                    self.possible_stable_nodes[i][b][1] += 1

                            # Check if conditions fulfilled for stable node                            
                            if (self.reward_history[i][b][action][0] - self.possible_stable_nodes[i][b][1]) % self.stable_threshold == 0 and (self.reward_history[i][b][action][0] - self.possible_stable_nodes[i][b][1]) > 0:
                                new_to_stable_nodes = True
                                if b in self.stable_nodes[i].keys():
                                    new_to_stable_nodes = False
                                    if action not in self.stable_nodes[i][b][0]:
                                        self.stable_nodes[i][b][0].append(action) 
                                else:
                                    b_origin = set(self.pg.G.nodes[b]['origin'])
                                    for stable_node in self.stable_nodes[i].keys():
                                        stable_origin = set(self.pg.G.nodes[stable_node]['origin'])
                                        if stable_origin.issubset(b_origin):
                                            new_to_stable_nodes = False
                                if new_to_stable_nodes:
                                    self.stable_nodes[i][b] = [[action], set()]
                                    # Add to relevant nodes 
                                    if self.use_relevant_node_merge and b not in self.relevant_nodes[i].keys():                                        
                                        self.relevant_nodes[i][b] = [{}, np.zeros(self.action_space.n, dtype=float), set()]          

    def _check_stable_nodes(self):
        # Check if there is any stable node that is a subset to another stable node and if so remove it
        for i in range(len(self.rewards)):
            for b1 in list(self.stable_nodes[i].keys()):
                b1_origin = set(self.pg.G.nodes[b1]['origin'])
                for b2 in list(self.stable_nodes[i].keys()):
                    if b1 != b2:
                        b2_origin = set(self.pg.G.nodes[b2]['origin'])
                        if b1_origin.issubset(b2_origin):
                            common_actions = set(self.stable_nodes[i][b1][0]).intersection(
                                self.stable_nodes[i][b2][0])
                            if len(common_actions) > 0:
                                for common_action in common_actions:
                                    # Add to stable on hold so that it can be brought back if b1 is proven to not be a stable node in the future
                                    if b1 in self.stable_nodes_on_hold[i]:
                                        if common_action in self.stable_nodes_on_hold[i][b1]:
                                            self.stable_nodes_on_hold[i][b1][common_action].append(
                                                b2)
                                        else:
                                            self.stable_nodes_on_hold[i][b1][common_action] = [
                                                b2]
                                    else:
                                        self.stable_nodes_on_hold[i][b1] = {
                                            common_action: [b2]}
                                    # Delete from possible stable nodes for the common action
                                    self.possible_stable_nodes[i][b2][0].remove(
                                        common_action)
                                    # Remove the common action from b2
                                    self.stable_nodes[i][b2][0].remove(
                                        common_action)
                                # Remove from stable node if it does not have a unique action
                                if len(self.stable_nodes[i][b2][0]) == 0:
                                    del self.stable_nodes[i][b2]

    def _stable_node_merge(self, active_nodes, top_active_nodes):
        """ Connects all top active nodes to all source nodes that are active """

        for i in range(len(self.rewards)):
            active_source_nodes = [
                x for x in active_nodes if x in self.stable_nodes[i].keys()]
            for active_source_node in active_source_nodes:
                for b in top_active_nodes:
                    if b not in self.stable_nodes[i][active_source_node][1] and b != active_source_node:
                        self.stable_nodes[i][active_source_node][1].add(b)
                        self._reset_reward_history_new_node_formed(i, self.stable_nodes[i][active_source_node][0], [active_source_node, b])
                        new_node = self.pg.add_AND_node(
                            [active_source_node, b])
                        if new_node is not None:
                            print("Formed {}: '{}' AND '{}' : (Stable node merge)".format(
                                new_node, active_source_node, b))
                            self._add_new_node(new_node)
                            self.stable_node_counter += 1

    def _update_look_ahead_table(self, top_active, rewards, action):
        """ Updates the look ahead table.

            Keeps track of positive/negative reward frequency for pairs of top active nodes. 
            One table per need, structured as:
                {node_1,node_2: action[[pos,neg], [pos,neg], [pos,neg]...],
                 node_1,node_3: action[[pos,neg], [pos,neg], [pos,neg]...]
                }
            """

        # for all pairs of top active nodes
        pairs = itertools.combinations(top_active, 2)
        for (node_1, node_2) in pairs:

            name_1 = node_1 + ',' + node_2
            name_2 = node_2 + ',' + node_1

            # for all needs
            for need, reward in enumerate(rewards):

                # Check so that at node_1 and/or node_2 have conflicting rewards
                conflicting_rewards = False
                if self.reward_history[need][node_1][action][0] > 0 and self.reward_history[need][node_1][action][1] > 0:
                    conflicting_rewards = True
                if self.reward_history[need][node_2][action][0] > 0 and self.reward_history[need][node_2][action][1] > 0:
                    conflicting_rewards = True 
                
                if conflicting_rewards:
                    # one 'table' per need
                    tbl = self.look_ahead[need]

                    # negative or positive reward
                    reward_idx = 1
                    if reward >= 0:
                        reward_idx = 0

                    # either name_1 or name_2 might exist
                    if name_1 in tbl:
                        tbl[name_1][action][reward_idx] += 1
                    elif name_2 in tbl:
                        tbl[name_2][action][reward_idx] += 1
                    else:  # Add name_1
                        tbl[name_1] = [[0, 0] for _ in range(self.action_space.n)]
                        tbl[name_1][action][reward_idx] += 1

    def _update_combination_probabilities(self, top_active):
        """ Comb(c,c') : Gives the probability for c and c' to be top-active at the same time.
        Definition 12 (Experience set).

        We track counters for (c AND c') """

        nodes = list(top_active)
        while len(nodes) > 1:
            for i in range(1, len(nodes)):
                name_1 = nodes[0] + ',' + nodes[i]
                name_2 = nodes[i] + ',' + nodes[0]
                if name_1 in self.comb:
                    self.comb[name_1] += 1
                elif name_2 in self.comb:
                    self.comb[name_2] += 1
                else:
                    self.comb[name_1] = 1
            nodes = nodes[1:]

    def _probabilistic_merge(self):
        """ Learning rule 6 (Probabilistic merge)."""

        random_uniform = np.random.random()
        if random_uniform > self.probabilistic_merge_prob:
            return

        self.probabilistic_merge_counter += 1
        n_pairs = sum(self.comb.values())
        probabilities = {key: value / n_pairs for key,
                         value in self.comb.items()}

        # select predecessors weighted by occurence, RWS
        def weighted_random_choice(choices):
            max = sum(choices.values())
            pick = random.uniform(0, max)
            current = 0
            for key, value in choices.items():
                current += value
                if current > pick:
                    return key
            return None

        predecessors_key = weighted_random_choice(probabilities)
        if not predecessors_key:
            return

        predecessors = predecessors_key.split(',')
        new_node = self.pg.add_AND_node(predecessors)
        print("Formed {}: '{}' AND '{}' : (Probabilistic merge)".format(
            new_node, predecessors[0], predecessors[1]))
        self._add_new_node(new_node)
        self.probabilistic_merge_node_counter += 1

    def _emotional_merge(self, pre_top_active):
        if len(pre_top_active) > 1:
            self.emotional_merge_counter += 1
            new_node = self.pg.add_random_AND_node(pre_top_active)
            if new_node is not None:
                predecessors = [x for x in self.pg.G.predecessors(new_node)]
                print("Formed {}: '{}' AND '{}' : (Emotional merge)".format(
                    new_node, predecessors[0], predecessors[1]))
                self._add_new_node(new_node)
                self.emotional_merge_node_counter += 1

    def _reward_based_merge(self):
        """ Formation rule based on received rewards. 

            Will form c AND c' if at least one of them have conflicting rewards
        """
        random_uniform = np.random.random()
        if random_uniform > self.prob_reward_based_merge:
            return

        # We do this per need
        for need, tbl in enumerate(self.look_ahead):

            # calculate factor positive/negative for each entry per action
            candidates = []            
            candidates_factors = []

            for potential_node, action_freq in tbl.items():
                # Add all factors that are greater than zero  
                potential_predecessors = potential_node.split(',')
                for ia, freq in enumerate(action_freq):
                    factor = 0.0
                    if freq[0] < self.reward_based_threshold: # minimum positive updates to form a connection
                        pass
                    # Fixed max value if only positive rewards have been received
                    elif freq[1] == 0:
                        factor = 100.0
                    else:
                        factor = freq[0] / freq[1]
                    
                    # Check if at least one of the potential predecessors have contradicting rewards
                    valid_predecessors = False
                    if self.reward_history[need][potential_predecessors[0]][ia][0] > 0 and self.reward_history[need][potential_predecessors[0]][ia][1] > 0:
                        valid_predecessors = True
                    if self.reward_history[need][potential_predecessors[1]][ia][0] > 0 and self.reward_history[need][potential_predecessors[1]][ia][1] > 0:
                        valid_predecessors = True                    
                    if not valid_predecessors:
                        factor = 0.0          

                    # Add to list of possible nodes
                    if factor > 0:
                        candidates.append([potential_predecessors, ia])
                        candidates_factors.append(factor)

            if len(candidates_factors) > 0:
                factor_sum = np.sum(candidates_factors)
                candidates_factors /= factor_sum
                indices = np.arange(0, len(candidates_factors))
                indx = np.random.choice(indices, size=1, p=candidates_factors)[0]                
                selected_candidate = candidates[indx]
                predecessors = selected_candidate[0]
                action = selected_candidate[1]                

                new_node = self.pg.add_AND_node(predecessors)
                print("Formed {}: '{}' AND '{}' : (Reward based merge)".format(
                    new_node, predecessors[0], predecessors[1]))
                self._add_new_node(new_node)
                self._reset_reward_history_new_node_formed(need, [action], predecessors)

    def _update_well_being(self, needs):
        if len(self.well_being) == self.sustainability_horizon:
            self.well_being = self.well_being[1:]
        self.well_being.append(np.amin(needs))

    def _update_sustainability(self, well_being):
        if len(self.well_being) == self.sustainability_horizon:
            if self.well_being[0] <= self.well_being[-1]:
                self.sustainability = self.sustainability_maximum
            else:
                self.sustainability = self.well_being[-1]*self.sustainability_horizon / (
                    self.well_being[0] - self.well_being[-1])
        else:
            self.sustainability = 0

    def reset_needs(self):
        if hasattr(self, 'needs'):
            for i, _ in enumerate(self.needs):
                self.needs[i] = self.initial_needs[i]

    def reset_statistics(self):
        self.probabilistic_merge_counter = 0
        self.probabilistic_merge_node_counter = 0
        self.emotional_merge_counter = 0
        self.emotional_merge_node_counter = 0
        self.surprise_counter = 0
        self.explore_counter = 0
        self.epsilon_counter = 0
        self.stable_node_counter = 0
        self.relevant_node_counter = 0

        self.action_frequency = np.zeros(self.action_space.n, dtype=int)

        if hasattr(self, 'needs'):
            self.needs_over_time = [[] for x in range(len(self.needs))]
            for i, n in enumerate(self.needs):
                self.needs_over_time[i].append(n)

    def get_action_learning_disabled(self, sensor_states, reward, done, info):
        self.sensor_states = sensor_states

        # Update needs
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs[i] = n

        # propagation
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()

        # Q-global update
        self._update_global_Q(self.top_active)

        # Decision making
        self._update_exploit()
        self._update_utility()

        # Action to take, here we choose to just exploit the policy. i.e no exploration
        self._exploit_policy()

        # Check if user overrides the action
        if 'override_action' in info.keys():
            if info['override_action'] != None:
                self._action = info['override_action']

        # stats
        self.action_frequency[self._action] += 1
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs_over_time[i].append(n)

        return self._action

    def get_action_learning_enabled(self, sensor_states, reward, done, info):

        # If we are talking with an Animat environment
        # we get the need/s and reward/s supplied to us.
        if 'needs' in info.keys():
            self.needs = list(info['needs'])
            self.rewards = list(info['rewards'])
        else:
            self.rewards = [reward]

        self.sensor_states = sensor_states

        # previous top active state
        pre_top_active = list(self.top_active)
        pre_active = self.pg.get_active_nodes()

        # Update reward history
        if self.use_reward_history:
            self._update_reward_history(pre_active, self.rewards, self._action)

        # Update look ahead table
        if self.use_reward_based_merge:
            self._update_look_ahead_table(
                pre_top_active, self.rewards, self._action)

        # Update stable nodes
        if self.use_stable_node_merge:
            self._update_stable_nodes(pre_active, self.rewards, self._action)

        # First propagation
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()
        active = self.pg.get_active_nodes()

        # Update relevant nodes
        if self.use_relevant_node_merge:
            self._update_relevant_nodes(pre_active, active, self._action)

        # Combination probabilities
        if self.use_probabilistic_merge:
            self._update_combination_probabilities(self.top_active)

        # First q-global update
        self._update_global_Q(self.top_active)

        # Local Q update
        self._update_local_Q(self.rewards, pre_top_active, self._action)

        # Check if emotionally surprised
        if self.use_effect_surprise:
            is_surprised = self._check_effect_surprised(
                pre_top_active, self._action)

        # Local R update
        self._update_local_R_online(pre_top_active, self._action)

        # Probabilistic merge
        if self.use_probabilistic_merge:
            self._probabilistic_merge()

        # Emotional merge
        if self.use_emotional_merge and is_surprised:
            self._emotional_merge(pre_top_active)

        # Reward based merge
        if self.use_reward_based_merge:
            self._reward_based_merge()

        # Stable node merge
        if self.use_stable_node_merge:
            active_nodes = self.pg.get_active_nodes()
            self._stable_node_merge(active_nodes, self.top_active)
            self._check_stable_nodes()

        # Relevant node merge
        if self.use_relevant_node_merge:
            self._relevant_node_merge(active, self.top_active)

        # Second propagation to prepare for decision making
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()

        # Second q-global update to prepare for decision making
        self._update_global_Q(self.top_active)

        # Decision making
        if self.use_explore:
            self._update_well_being(self.needs)
            self._update_sustainability(self.well_being)
            self._update_explore()
        self._update_exploit()
        self._update_utility()

        # Action to take
        self._select_action()

        # Check if user overrides the action
        if 'override_action' in info.keys():
            if info['override_action'] != None:
                self._action = info['override_action']

        # stats
        self.action_frequency[self._action] += 1
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs_over_time[i].append(n)

        return self._action

    def get_action(self, sensor_states, reward, done, info, time_zero):
        # In case learning is disabled or time_zero = True, take alternate path
        if self.learning_enabled and not time_zero:
            action = self.get_action_learning_enabled(
                sensor_states, reward, done, info)
        else:
            action = self.get_action_learning_disabled(
                sensor_states, reward, done, info)

        # learning rate decay
        self.alpha *= self.alpha_decay
        self.alpha = max(self.alpha, self.min_alpha)

        return action

    def dump_info(self):
        """Prints useful info to console."""

        print("Q-Matrices:")
        for ni, qm in enumerate(self.Q_matrices):
            print("Need {}".format(ni))
            print("Actions : {}".format(self.action_names))

            for b, value in qm.items():
                print("{} : {}".format(b, value))
        print("----------")
        print("R-Matrices:")
        for ni, qm in enumerate(self.R_matrices):
            print("Need {}".format(ni))
            print("Actions : {}".format(self.action_names))

            for b, value in qm.items():
                print("{} : {}".format(b, value))

        print("----------")
        print("Look ahead:")
        for ni, qm in enumerate(self.look_ahead):
            print("Need {}".format(ni))

            for b, value in qm.items():
                print("{} : {}".format(b, value))

    def _update_global_Q(self, top_active):
        """ Updates the global Q-values: Learning rule 1 (Local Q-value updates) """

        QR_sum = np.zeros(self.action_space.n)
        R_sum = np.zeros(self.action_space.n)
        for i in range(len(self.rewards)):
            QR_sum.fill(0)
            R_sum.fill(0)
            for b in top_active:
                QR_sum += np.multiply(self.Q_matrices[i][b],
                                      self.R_matrices[i][b])
                R_sum += self.R_matrices[i][b]
            self._global_Q[i] = np.divide(QR_sum, R_sum)

    def _update_local_Q(self, rewards, pre_top_active, a):
        """ Updates the local Q-values: Learning rule 1 (Local Q-value updates) """

        for i in range(len(self.rewards)):
            global_Q_max = np.amax(self._global_Q[i])
            for b in pre_top_active:
                pre_Q = self.Q_matrices[i][b][a]
                self.Q_matrices[i][b][a] = pre_Q + self.alpha * \
                    (rewards[i] + self.gamma*global_Q_max - pre_Q)

                # Keep track on how local-Q will change for a specific top active node
                if self.use_effect_surprise:
                    self._surprise_values[i][b] = [
                        pre_Q, self.Q_matrices[i][b][a]]

    def _update_local_R_online(self, pre_top_active, a):
        """ Updates the local R-values: Learning rule 2 (Local R-value updates)

            This is Welford's online algorithm "
            https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance """

        for i in range(len(self.rewards)):
            for b in pre_top_active:
                self._R_count[i][b][a] += 1 # We need to track this for other purposes

                # update R values if needed
                if not self.use_reliability_one:
                    var = 0
                    rel_value = self.initial_reliability
                    if self._R_count[i][b][a] > 1:
                        delta = self.Q_matrices[i][b][a] - self._R_mean[i][b][a]
                        self._R_mean[i][b][a] += delta / self._R_count[i][b][a]
                        delta2 = self.Q_matrices[i][b][a] - self._R_mean[i][b][a]
                        self._R_M2[i][b][a] += delta * delta2
                        var = self._R_M2[i][b][a] / (self._R_count[i][b][a] - 1)
                        self._R_std[i][b][a] = np.sqrt(var)
                        rel_value = 1 / (self._R_std[i][b][a] + 1)
                    else:
                        self._R_mean[i][b][a] = self.Q_matrices[i][b][a]

                    self.R_matrices[i][b][a] = rel_value

    def _check_effect_surprised(self, pre_top_active, a):
        """ Check if the Animat becomes surprised.
            Definition 18 (Effect surprise).

            TODO FM: This doesn't really make sense i think... """

        surprised = []
        for i in range(len(self.rewards)):
            surprised.append(np.zeros(len(pre_top_active)))
            for j, b in enumerate(pre_top_active):
                if self._R_count[i][b][a] > self.surprise_min_updates:
                    old_value = self._surprise_values[i][b][0]
                    new_value = self._surprise_values[i][b][1]
                    if old_value != 0:
                        update_value = np.fabs(
                            new_value - old_value) / np.fabs(old_value)

                        # Did we pass the surprise treshold
                        if update_value > self.surprise_threshold:
                            surprised[i][j] = 1
                    else:  # if Q == 0 we are always surprised
                        surprised[i][j] = 1

        # if all top active nodes for some need are surprised then the Animat is surprised
        is_surprised = False
        for i in range(len(self.rewards)):
            if sum(surprised[i]) == len(pre_top_active):
                is_surprised = True

        if is_surprised:
            self.surprise_counter += 1

        return is_surprised

    def _update_exploit(self):
        if hasattr(self, 'needs'):
            new_values = np.add(self.needs, np.multiply(
                self.exploit_discount_rate, self._global_Q))
        else:
            new_values = np.multiply(
                self.exploit_discount_rate, self._global_Q)
        self._exploit = np.amin(new_values, axis=0)

    def _update_explore(self):
        random_uniform = np.random.random(self.action_space.n)
        self._explore = self.explore_rate * \
            np.add(random_uniform, self.sustainability)

    def _update_utility(self):
        if self.use_explore:
            self._utility = np.amin([self._exploit, self._explore], axis=0)
            self._used_for_explore_counter = np.argmin(
                [self._exploit, self._explore], axis=0)
        else:
            self._utility = np.copy(self._exploit)

    def _exploit_policy(self):
        max_value = np.amax(self._utility)
        best_actions = np.argwhere(
            self._utility == max_value).flatten()
        self._action = np.random.choice(best_actions)

    def _select_action(self):
        if self.use_explore:
            max_value = np.amax(self._utility)
            best_actions = np.argwhere(self._utility == max_value).flatten()
            self._action = np.random.choice(best_actions)
            if self._used_for_explore_counter[self._action] == 1:
                self.explore_counter += 1
        else:
            biased_coin = random.random()
            if biased_coin > self.epsilon:
                max_value = np.amax(self._utility)
                best_actions = np.argwhere(
                    self._utility == max_value).flatten()
                self._action = np.random.choice(best_actions)
            else:
                self.epsilon_counter += 1
                self._action = self.action_space.sample()

    def _add_new_node(self, node):
        """ Adds a new node and updates the various matrixes. """

        b1, b2 = self.pg.G.predecessors(node)

        for i, _ in enumerate(self.rewards):

            # Q-value for newly formed node
            if self.use_average_q:
                self.Q_matrices[i][node] = np.divide(
                    np.add(self.Q_matrices[i][b1], self.Q_matrices[i][b2]), 2.0)
            else:
                self.Q_matrices[i][node] = np.zeros(self.action_space.n)

            # R-value new node
            if self.use_reliability_one:
                self.R_matrices[i][node] = np.empty(self.action_space.n)
                self.R_matrices[i][node].fill(1.0)
                self._R_count[i][node] = np.zeros(self.action_space.n)
            else:
                self.R_matrices[i][node] = np.empty(self.action_space.n)
                self.R_matrices[i][node].fill(self.initial_reliability)
                self._R_mean[i][node] = np.zeros(self.action_space.n)
                self._R_std[i][node] = np.zeros(self.action_space.n)
                self._R_count[i][node] = np.zeros(self.action_space.n)
                self._R_M2[i][node] = np.zeros(self.action_space.n)

        # Remove the added combination from our look ahead table
        if self.use_reward_based_merge:
            remove_these = []
            for key in self.look_ahead[0]:
                # TODO: Origin can be precalculated if needed
                predecessors = key.split(',')
                origin = set(self.pg.G.nodes[predecessors[0]]['origin'] +
                             self.pg.G.nodes[predecessors[1]]['origin'])
                uid = self.pg._get_origin_uid(origin, 'AND')

                # If we have a node with this origin already remove this combination
                if uid in self.pg.G.graph['uid']:
                    remove_these.append(key)

            for tbl in self.look_ahead:
                for key in remove_these:
                    tbl.pop(key, None)

        # Add node to possible stable nodes
        for i in range(len(self.rewards)):
            self.possible_stable_nodes[i][node] = [set(), 0]
            for a in range(self.action_space.n):
                self.possible_stable_nodes[i][node][0].add(a)

        # Add node to reward history
        for i in range(len(self.rewards)):
            self.reward_history[i][node] = {}
            for a in range(self.action_space.n):
                self.reward_history[i][node][a] = [0, 0]

        # Remove possible predecessors from probabilistic merge
        # TODO: this might be very costly for larger graphs
        if self.use_probabilistic_merge:
            remove_these = []
            for key in self.comb.keys():
                predecessors = key.split(',')
                origin = set(self.pg.G.nodes[predecessors[0]]['origin'] +
                             self.pg.G.nodes[predecessors[1]]['origin'])
                uid = self.pg._get_origin_uid(origin, 'AND')
                if uid in self.pg.G.graph['uid']:
                    remove_these.append(key)
            for key in remove_these:
                del self.comb[key]

    def get_brain_complexity(self):
        return self.pg.G.number_of_nodes()

    def get_information(self):
        """ Get a description for all information available, both 'constant' and 'variable' """

        if not self.information:
            self.setup_information()

        return self.information

    def get_variable_information(self):
        """ Get information that can change over time. i.e entries 'variable' """

        # variable information, careful to name and structure exactly as for self.information
        self.information_variable['Q-learning']['Alpha'] = "%.4f" % self.alpha

        self.information_variable['Decision making']['Epsilon counter'] = str(
            self.epsilon_counter)
        self.information_variable['Decision making']['Exploration counter'] = str(
            self.explore_counter)

        self.information_variable['Probabilistic merge']['Merge counter'] = str(
            self.probabilistic_merge_counter)
        self.information_variable['Probabilistic merge']['Merge node counter'] = str(
            self.probabilistic_merge_node_counter)

        self.information_variable['Emotional merge']['Merge counter'] = str(
            self.emotional_merge_counter)
        self.information_variable['Emotional merge']['Merge node counter'] = str(
            self.emotional_merge_node_counter)

        self.information_variable['Effect surprise']['Surprise counter'] = str(
            self.surprise_counter)

        self.information_variable['Perception graph']['# AND nodes'] = str(
            len(self.pg.G.graph['node_types']['AND']))
        self.information_variable['Perception graph']['# SEQ nodes'] = str(
            len(self.pg.G.graph['node_types']['SEQ']))
        self.information_variable['Perception graph']['# Total nodes'] = str(
            len(self.pg.G))

        return self.information_variable

    def setup_information(self):
        """ Setup the information to display in GUI runs. """

        # dict with categories
        self.information = collections.OrderedDict()
        self.information_variable = collections.OrderedDict()

        # each category is then a dict with entries
        self.information['Q-learning'] = collections.OrderedDict()
        self.information['Decision making'] = collections.OrderedDict()
        self.information['Probabilistic merge'] = collections.OrderedDict()
        self.information['Emotional merge'] = collections.OrderedDict()
        self.information['Reward based merge'] = collections.OrderedDict()
        self.information['Effect surprise'] = collections.OrderedDict()
        self.information['Perception graph'] = collections.OrderedDict()

        # categories that can hold variable information, these are set in self.get_variable_information()
        self.information_variable['Q-learning'] = collections.OrderedDict()
        self.information_variable['Decision making'] = collections.OrderedDict(
        )
        self.information_variable['Probabilistic merge'] = collections.OrderedDict(
        )
        self.information_variable['Emotional merge'] = collections.OrderedDict(
        )
        self.information_variable['Effect surprise'] = collections.OrderedDict(
        )
        self.information_variable['Perception graph'] = collections.OrderedDict(
        )

        # an entry is a tuple (value, volatility)
        # Q-learning parameters
        self.information['Q-learning']['Alpha'] = (
            "%.4f" % self.alpha, 'variable')
        self.information['Q-learning']['Start alpha'] = (
            "%.4f" % self.start_alpha, 'constant')
        self.information['Q-learning']['Minimum alpha'] = (
            "%.4f" % self.min_alpha, 'constant')
        self.information['Q-learning']['Alpha decay'] = (
            "%.4f" % self.alpha_decay, 'constant')
        self.information['Q-learning']['Gamma'] = (
            "%.4f" % self.gamma, 'constant')
        self.information['Q-learning']['Initial reliability'] = (
            "%.4f" % self.initial_reliability, 'constant')
        self.information['Q-learning']['Use average Q'] = (
            str(self.use_average_q), 'constant')
        self.information['Q-learning']['Use reliability 1.0'] = (
            str(self.use_reliability_one), 'constant')

        # # Exploration
        self.information['Decision making']['Epsilon'] = (
            "%.4f" % self.epsilon, 'constant')
        self.information['Decision making']['Epsilon counter'] = (
            str(self.epsilon_counter), 'variable')

        self.information['Decision making']['Use exploration'] = (
            str(self.use_explore), 'constant')
        self.information['Decision making']['Exploration rate'] = (
            "%.4f" % self.explore_rate, 'constant')
        self.information['Decision making']['Exploration counter'] = (
            str(self.explore_counter), 'variable')
        self.information['Decision making']['Sustainability horizon'] = (
            str(self.sustainability_horizon), 'constant')
        self.information['Decision making']['Maximum sustainability'] = (
            "%.4e" % self.sustainability_maximum, 'constant')
        self.information['Decision making']['Exploit discount rate'] = (
            "%.4f" % self.exploit_discount_rate, 'constant')

        # # Probabilistic merge
        self.information['Probabilistic merge']['Use probabilistic merge'] = (
            str(self.use_probabilistic_merge), 'constant')
        self.information['Probabilistic merge']['Merge counter'] = (
            str(self.probabilistic_merge_counter), 'variable')
        self.information['Probabilistic merge']['Merge node counter'] = (
            str(self.probabilistic_merge_node_counter), 'variable')
        self.information['Probabilistic merge']['Merge probability'] = (
            "%.4f" % self.probabilistic_merge_prob, 'constant')

        # # Emotional merge
        self.information['Emotional merge']['Use emotional merge'] = (
            str(self.use_emotional_merge), 'constant')
        self.information['Emotional merge']['Merge counter'] = (
            str(self.emotional_merge_counter), 'variable')
        self.information['Emotional merge']['Merge node counter'] = (
            str(self.emotional_merge_node_counter), 'variable')

        # # Reward based merge
        self.information['Reward based merge']['Use reward based merge'] = (
            str(self.use_reward_based_merge), 'constant')
        self.information['Reward based merge']['Merge probability'] = (
            "%.4f" % self.prob_reward_based_merge, 'constant')

        # # Effect surprise parameters
        self.information['Effect surprise']['Use effect surprise'] = (
            str(self.use_effect_surprise), 'constant')
        self.information['Effect surprise']['Minimum updates'] = (
            str(self.surprise_min_updates), 'constant')
        self.information['Effect surprise']['Surprise treshold'] = (
            "%.4f" % self.surprise_threshold, 'constant')
        self.information['Effect surprise']['Surprise counter'] = (
            str(self.surprise_counter), 'variable')

        # # Perception graph
        self.information['Perception graph']['# SENSOR nodes'] = (
            len(self.sensor_names), 'constant')
        self.information['Perception graph']['# AND nodes'] = (
            str(len(self.pg.G.graph['node_types']['AND'])), 'variable')
        self.information['Perception graph']['# SEQ nodes'] = (
            str(len(self.pg.G.graph['node_types']['SEQ'])), 'variable')
        self.information['Perception graph']['# Total nodes'] = (
            str(len(self.pg.G)), 'variable')

        # refresh the variable information
        self.get_variable_information()
