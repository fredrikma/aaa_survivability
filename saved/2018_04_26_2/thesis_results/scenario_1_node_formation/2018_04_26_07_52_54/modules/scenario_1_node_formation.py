import sys
import numpy as np
import pandas as pd
import gym

from multiprocessing import Pool
from collections import defaultdict

import aaa_survivability.envs.animat.thesis_1_env

from aaa_survivability.agents.animat.animat_baseline import AnimatBaseline

class Scenario1NodeFormationRunner:

    def __init__(self, agent, env, num_evaluations, num_training_steps, num_test_steps, num_test_episodes):

        self.agent = agent
        self.env = env
        self.num_evaluations = num_evaluations
        self.num_training_steps = num_training_steps
        self.num_test_steps = num_test_steps
        self.num_test_episodes = num_test_episodes

    def set_up_final_data_dict(self):
        data_dict = {}        
        
        data_dict['Trained episodes'] = np.arange(self.num_training_steps, self.num_training_steps + self.num_training_steps*self.num_evaluations, self.num_training_steps, dtype='int')
        data_dict['Need'] = np.empty(self.num_evaluations)
        data_dict['Episode length'] = np.empty(self.num_evaluations)
        data_dict['Green food'] = np.empty(self.num_evaluations)
        data_dict['Red food'] = np.empty(self.num_evaluations)
        data_dict['Number of nodes'] = np.empty(self.num_evaluations)
        #data_dict['Relevant nodes'] = np.empty(self.num_evaluations)
        #data_dict['Irrelevant nodes'] = np.empty(self.num_evaluations)

        return data_dict

    def set_up_all_episodes_data_dict(self):
        data_dict = {}

        data_dict['Need'] = np.empty(self.num_test_episodes)
        data_dict['Episode length'] = np.empty(self.num_test_episodes)
        data_dict['Green food'] = np.empty(self.num_test_episodes)
        data_dict['Red food'] = np.empty(self.num_test_episodes)

        return data_dict

    def set_up_one_episode_data_dict(self):
        data_dict = {}

        data_dict['Need'] = []

        return data_dict

    def evaluate_new_nodes(self):

        new_nodes = [0, 0]
        for node in self.agent.pg.G.graph['node_types']['AND']:
            noise_in_origin =  next((x for x in self.agent.pg.G.nodes[node]['origin'] if 'Noise' in x), None)
            if noise_in_origin is None:
                #Relevant node
                new_nodes[0] += 1
            else:
                #Irrelevant node
                new_nodes[1] += 1
        
        return(new_nodes)

    def run(self):
        final_data_dict = self.set_up_final_data_dict()
        all_episodes_data_dict = self.set_up_all_episodes_data_dict()
        one_episode_data_dict = self.set_up_one_episode_data_dict()

        for i in range(self.num_evaluations):
            self.agent.learning_enabled = True
            self.env.death_at_need_zero = False
            t = 0
            state, info = self.env.reset()
            reward = 0
            done = False

            # Training
            while not done and t < self.num_training_steps:
                # Get new action
                action = self.agent.get_action(
                    state, reward, done, info, t == 0)

                # Execute action
                state, reward, done, info = self.env.step(action)

                t += 1
            
            # Number of nodes
            final_data_dict['Number of nodes'][i] = self.agent.pg.G.number_of_nodes()

            # Save ir/relevant nodes
            #new_nodes = self.evaluate_new_nodes()
            #final_data_dict['Relevant nodes'][i] = new_nodes[0]
            #final_data_dict['Irrelevant nodes'][i] = new_nodes[1]

            # No learning during test phase
            self.agent.learning_enabled = False
            self.env.death_at_need_zero = True

            # Testing
            for j in range(self.num_test_episodes):
                # Clear one_episode_data_dict each episode
                for statistic in one_episode_data_dict.keys():
                    one_episode_data_dict[statistic].clear()

                # Iterate over all test steps
                t = 0
                state, info = self.env.reset()
                reward = 0
                done = False

                while not done and t < self.num_test_steps:
                    # Get new action
                    action = self.agent.get_action(
                        state, reward, done, info, t == 0)

                    # Execute action
                    state, reward, done, info = self.env.step(action)

                    # Collect step based data
                    one_episode_data_dict['Need'].append(info['needs'][0])                    

                    t += 1

                # Calculate averages over step based data
                for stat_key in one_episode_data_dict.keys():
                    all_episodes_data_dict[stat_key][j] = np.average(
                        one_episode_data_dict[stat_key])

                # Collect episode based data
                all_episodes_data_dict['Episode length'][j] = t
                all_episodes_data_dict['Green food'][j] = info['food_eaten_positive']
                all_episodes_data_dict['Red food'][j] = info['food_eaten_negative']

            # Calculate averages over all episodes
            for stat_key in all_episodes_data_dict.keys():
                final_data_dict[stat_key][i] = np.average(
                    all_episodes_data_dict[stat_key])

        return final_data_dict


if __name__ == '__main__':

    outdir = "./"

    # supplied config
    if len(sys.argv) >= 2:
        outdir = sys.argv[1]    

    # Environment
    environment = "thesis_1-v0"    
    env_config = dict()

    env_config = {'Num tiles X' : 3,
                  'Num tiles Y' : 3,
                  'Num food objects' : 3,
                  'Global need decay' : -0.02,
                  'Light Red Green' : 0.25,
                  'Death at NEED=0' : False,
                  'Noise' : [0.1 for _ in range(20)]
                  }
    
    # Agent configurations
    agent_config_1 = {'use_probabilistic_merge': True,
                    'probabilistic_merge_prob': 0.01,
                      'use_emotional_merge': True,
                      'use_reward_based_merge': False,
                      'use_stable_node_merge': False,
                      'use_average_q': True,
                      'start_alpha': 0.05,
                      'alpha_decay': 1}

    agent_config_2 = {'use_probabilistic_merge': False,
                      'probabilistic_merge_prob': 0.01,
                      'use_emotional_merge': False,
                      'use_reward_based_merge': True,
                      'prob_reward_based_merge': 1.0,
                      'reward_based_threshold': 3,
                      'use_stable_node_merge': True,
                      'stable_threshold': 20,
                      'use_average_q': True,
                      'start_alpha': 0.05,
                      'alpha_decay': 1}

    agent_configs = [agent_config_1, agent_config_2]
    config_names = ['config_1', 'config_2']

    # Statistic settings
    num_agents = 20 #4
    num_training_steps = 100
    num_test_steps = 100
    num_test_episodes = 20
    num_evaluations = 30

    def f(config):
        # Create environment
        env = gym.make(config['environment'])
        env.configure(config['env_config'])
        state, info = env.reset()

        # Create agent
        agent = AnimatBaseline(env.action_space, env.SENSOR_NAMES,
                               state, info, env.ACTION_NAMES, config['agent_configuration'])

        # Collect data
        runner = Scenario1NodeFormationRunner(agent, env, config['num_evaluations'], config['num_training_steps'], config['num_test_steps'], config['num_test_episodes'])
        data_dict = runner.run()

        return data_dict

    for i, agent_config in enumerate(agent_configs):
        csv_filename = outdir + config_names[i] + ".csv"

        process_configs = []
        for j in range(num_agents):            
            config = {
                'agent_configuration': agent_config,
                'environment': environment,
                'env_config': env_config,
                'num_training_steps': num_training_steps,
                'num_test_steps': num_test_steps,
                'num_test_episodes': num_test_episodes,
                'num_evaluations': num_evaluations
            }

            process_configs.append(config)

        # Generate data with parallel processes
        with Pool(maxtasksperchild=1) as p:
            data_dicts = p.map(f, process_configs, chunksize=1)

        # Combine all dicts to one dict
        combined_data_dict = defaultdict(list)
        for d in data_dicts:
            for key, values in d.items():
                for value in values:
                    combined_data_dict[key].append(value)

        # Create df and save to csv
        df = pd.DataFrame.from_dict(combined_data_dict)
        df.to_csv(csv_filename, index=False)
