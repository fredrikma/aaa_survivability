import sys
import numpy as np
import pandas as pd
import gym

from multiprocessing import Pool
from collections import defaultdict

import aaa_survivability.envs.animat.thesis_1_env

from aaa_survivability.agents.animat.animat_baseline import AnimatBaseline
from aaa_survivability.agents.animat_dqn import AnimatDQN
from aaa_survivability.agents.random_agent import RandomAgent
from aaa_survivability.agents.animat_incremental_q_learner import AnimatIncrementalQLearner

class Scenario1Runner:

    def __init__(self, agent, env, num_evaluations, num_training_steps, num_test_steps, num_test_episodes):

        self.agent = agent
        self.env = env
        self.num_evaluations = num_evaluations
        self.num_training_steps = num_training_steps
        self.num_test_steps = num_test_steps
        self.num_test_episodes = num_test_episodes

    def set_up_final_data_dict(self):
        data_dict = {}        
        
        data_dict['Trained time steps'] = np.arange(self.num_training_steps, self.num_training_steps + self.num_training_steps*self.num_evaluations, self.num_training_steps, dtype='int')
        data_dict['Energy'] = np.empty(self.num_evaluations)
        data_dict['Episode length'] = np.empty(self.num_evaluations)
        data_dict['Green food'] = np.empty(self.num_evaluations)
        data_dict['Red food'] = np.empty(self.num_evaluations)
        data_dict['Number of nodes'] = np.empty(self.num_evaluations)

        return data_dict

    def set_up_all_episodes_data_dict(self):
        data_dict = {}

        data_dict['Energy'] = np.empty(self.num_test_episodes)
        data_dict['Episode length'] = np.empty(self.num_test_episodes)
        data_dict['Green food'] = np.empty(self.num_test_episodes)
        data_dict['Red food'] = np.empty(self.num_test_episodes)

        return data_dict

    def set_up_one_episode_data_dict(self):
        data_dict = {}

        data_dict['Energy'] = []

        return data_dict

    def run(self):
        final_data_dict = self.set_up_final_data_dict()
        all_episodes_data_dict = self.set_up_all_episodes_data_dict()
        one_episode_data_dict = self.set_up_one_episode_data_dict()

        for i in range(self.num_evaluations):
            self.agent.learning_enabled = True
            self.env.death_at_need_zero = False
            t = 0
            state, info = self.env.reset()
            reward = 0
            done = False

            # Training
            while not done and t < self.num_training_steps:
                # Get new action
                action = self.agent.get_action(
                    state, reward, done, info, t == 0)

                # Execute action
                state, reward, done, info = self.env.step(action)

                t += 1
            
            # Number of nodes
            final_data_dict['Number of nodes'][i] = self.agent.get_brain_complexity()#.pg.G.number_of_nodes()

            # No learning during test phase
            self.agent.learning_enabled = False
            self.env.death_at_need_zero = True

            # Testing
            for j in range(self.num_test_episodes):
                # Clear one_episode_data_dict each episode
                for statistic in one_episode_data_dict.keys():
                    one_episode_data_dict[statistic].clear()

                # Iterate over all test steps
                t = 0
                state, info = self.env.reset()
                reward = 0
                done = False

                while not done and t < self.num_test_steps:
                    # Get new action
                    action = self.agent.get_action(
                        state, reward, done, info, t == 0)

                    # Execute action
                    state, reward, done, info = self.env.step(action)

                    # Collect step based data
                    one_episode_data_dict['Energy'].append(info['needs'][0])                    

                    t += 1

                # Calculate averages over step based data
                for stat_key in one_episode_data_dict.keys():
                    all_episodes_data_dict[stat_key][j] = np.average(
                        one_episode_data_dict[stat_key])

                # Collect episode based data
                all_episodes_data_dict['Episode length'][j] = t
                all_episodes_data_dict['Green food'][j] = info['food_eaten_positive']
                all_episodes_data_dict['Red food'][j] = info['food_eaten_negative']

            # Calculate averages over all episodes
            for stat_key in all_episodes_data_dict.keys():
                final_data_dict[stat_key][i] = np.average(
                    all_episodes_data_dict[stat_key])

        return final_data_dict


if __name__ == '__main__':

    outdir = "./"

    # supplied config
    if len(sys.argv) >= 2:
        outdir = sys.argv[1]    

    # Environment
    environment = "thesis_1-v0"    
    env_config = dict()

    env_config = {'Num tiles X' : 3,
                  'Num tiles Y' : 3,
                  'Num food objects' : 3,
                  'Global need decay' : -0.02,
                  'Light Red Green' : 0.25,
                  'Death at NEED=0' : False,
                  'Noise' : [0.25 for _ in range(32)]
                  }
    
    # Agent configurations
    config_new_animat = {'use_probabilistic_merge': False,
                         'use_emotional_merge': False,
                         'use_reward_based_merge': True,
                         'use_stable_node_merge': True,
                         'use_relevant_node_merge': True,
                         'use_average_q': True,
                         'use_reliability_one': True,
                         'prob_reward_based_merge': 1.0,
                         'reward_based_threshold': 10,                      
                         'relevant_min_updates': 20,
                         'relevant_threshold': 0.65,
                         'stable_threshold': 15,
                         'start_alpha': 0.05,
                         'alpha_decay': 1,
                         'gamma' : 0.9,
                         'epsilon': 1.0,
                         'epsilon_min': 0.01,
                         'epsilon_decay': 0.99,
                        }


    config_old_animat = {'use_probabilistic_merge': True,
                         'probabilistic_merge_prob': 0.02,
                         'use_emotional_merge': True,
                         'surprise_threshold' : 0.04,
                         'use_reward_based_merge': False,
                         'use_stable_node_merge': False,
                         'use_relevant_node_merge': False,
                         'use_average_q': False,
                         'use_reliability_one': False,
                         'prob_reward_based_merge': 1.0,
                         'reward_based_threshold': 10,                      
                         'relevant_min_updates': 20,
                         'relevant_threshold': 0.65,
                         'stable_threshold': 15,
                         'start_alpha': 0.05,
                         'alpha_decay': 1,
                         'gamma' : 0.9,
                         'epsilon': 1.0,
                         'epsilon_min': 0.01,
                         'epsilon_decay': 0.99,
                        }

    config_dqn = {'replay_memory_length': 1000,
                  'replay_batch_size': 32,
                  'replay_num_training_iterations' : 1,
                  'gamma': 0.9,
                  'epsilon': 1.0,
                  'epsilon_min': 0.01,
                  'epsilon_decay': 0.99,
                  'learning_rate': 0.005,
                  'activation': "sigmoid",
                  'tau': 0.1}                        


    # agents to run
    agents = ["AnimatBaseline", "AnimatBaseline", "AnimatDQN", "AnimatIncrementalQLearner", "RandomAgent"]
    agent_configs = [config_new_animat, config_old_animat, config_dqn, None, None]
    csv_names = ["AnimatBaseline_New", "AnimatBaseline_Old", "AnimatDQN", "AnimatIncrementalQLearner", "RandomAgent"]

    # Statistic settings
    num_agents = 20
    num_training_steps = 200
    num_test_steps = 100
    num_test_episodes = 20
    num_evaluations = 50

    def f(config):
        # Create environment
        env = gym.make(config['environment'])
        env.configure(config['env_config'])
        state, info = env.reset()

        # agent
        id = config['agent']
        constructor = globals()[id]

        # possible config
        if config['agent_configuration']:
            agent = constructor(env.action_space, env.SENSOR_NAMES,
                                state, info, env.ACTION_NAMES, config['agent_configuration'])
        else:
            agent = constructor(env.action_space, env.SENSOR_NAMES, state, info, env.ACTION_NAMES)

        # Collect data
        runner = Scenario1Runner(agent, env, config['num_evaluations'], config['num_training_steps'], config['num_test_steps'], config['num_test_episodes'])
        data_dict = runner.run()

        return data_dict

    #for i, agent_config in enumerate(agent_configs):
    for i, agent in enumerate(agents):

        csv_filename = outdir + csv_names[i] + ".csv"
        agent_config = agent_configs[i]

        process_configs = []
        for j in range(num_agents):            
            config = {
                'agent': agent,
                'agent_configuration': agent_config,
                'environment': environment,
                'env_config': env_config,
                'num_training_steps': num_training_steps,
                'num_test_steps': num_test_steps,
                'num_test_episodes': num_test_episodes,
                'num_evaluations': num_evaluations
            }

            process_configs.append(config)

        # Generate data with parallel processes
        with Pool(maxtasksperchild=1) as p:
            data_dicts = p.map(f, process_configs, chunksize=1)

        # Combine all dicts to one dict
        combined_data_dict = defaultdict(list)
        for d in data_dicts:
            for key, values in d.items():
                for value in values:
                    combined_data_dict[key].append(value)

        # Create df and save to csv
        df = pd.DataFrame.from_dict(combined_data_dict)
        df.to_csv(csv_filename, index=False)
