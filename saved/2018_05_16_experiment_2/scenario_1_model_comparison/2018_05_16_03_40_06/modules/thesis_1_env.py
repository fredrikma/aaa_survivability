import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
import time
import math
from os import path
import pyglet

from aaa_survivability.envs.animat.animat_env import AnimatEnv
from aaa_survivability.envs.animat.renderer import Renderer

class Thesis1Env(AnimatEnv):
    """Tilebased AxB discrete environment (default A,B = 3).

    An environment for evaluating some properties of the Generic Animat Model.

    Actions:
        0 = Up
        1 = Down
        2 = Left
        3 = Right
        4 = Eat

    Sensors:
        von-neuman directions for strongest smell, mutually exclusive only the strongest direction active
        current tile sensor (if we stand on food)
        red light sensor
        green light sensor
        N noise sensors, random activation
        4 + 1 + 1 + 1 + N = 7 + N
    

    Rules:
        NEED decays over time
        FOOD objects never overlap
        When all FOOD is consumed they all respawn at new random locations
        Red and Green light are mutually exclusive, however one of them is always active
        The scenario ends when NEED <= 0 (configurable)
        
    Configuration: (key : default value)
        'Num tiles X' : 1
        'Num tiles Y' : 1
        'Num food objects' : 1
        'Global need decay' : -0.001
        'Noise' : []
        'Light Red Green' : 0.5
        'Death at NEED=0' : False
    """
    ACTION_NAMES = ["Up", "Down", "Left", "Right", "Eat"]
    NUM_ACTIONS = len(ACTION_NAMES)

    # sensors, configure will append the noise sensors
    SENSOR_NAMES = ["Dir-up FOOD", "Dir-down FOOD", "Dir-left FOOD", "Dir-right FOOD", 
                    "Center FOOD",
                    "Red Light", "Green Light"]

    # offset for smelly vision receptors
    RECEPTOR_OFFSETS = [[0,1], [0,-1], [-1,0], [1,0]]

    REWARD_EAT_FOOD_POSITIVE = 0.3
    REWARD_EAT_FOOD_NEGATIVE = -0.3
    REWARD_EAT_FOOD_EMPTY = -0.015   # If empty and move are the same, learning is a lot slower
    REWARD_MOVE = -0.01

    CAP_NEED = True

    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 4
    }

    # movement actions
    actions_move = {0: [0, 1],
                    1: [0, -1],
                    2: [-1, 0],
                    3: [1, 0]}

    # TODO: make configurable
    game_area_width = 600
    game_area_height = 600
    
    status_area_height = 40
    status_area_width = game_area_height
    status_area_tile_height = status_area_height
    status_area_tile_width = status_area_tile_height

    screen_width = game_area_width
    screen_height = game_area_height + status_area_height

    class StatusArea(object):
        def __init__(self, sprites, width, height, offset):
            self.sprites = sprites
            self.active_colors = [s.color for s in sprites]
            self.inactive_colors = [tuple(int(c * 0.3) for c in s.color) for s in sprites]

            self.offset = offset

            self.tile_width = width / len(sprites)
            self.tile_height = height

            for i, sprite in enumerate(self.sprites):
                sprite.position = (i * self.tile_width + self.offset[0], self.offset[1])
            
        
        def set_visibility(self, visibility):
            for i, sprite in enumerate(self.sprites):
                if visibility[i]:
                    sprite.color = self.active_colors[i]
                else:
                    sprite.color = self.inactive_colors[i]
                
    class TileObject(object):
        def __init__(self, sprite, env, x=0, y=0):
            self.sprite = sprite
            self.env = env
            self.tile_pos = np.array([x, y], dtype=np.int64)

        @property
        def tile_pos(self):
            return self.__tile_pos

        @tile_pos.setter
        def tile_pos(self, p):
            self.__tile_pos = p
            self.sprite.position = (
                self.tile_pos[0] * self.env.tile_width, self.tile_pos[1] * self.env.tile_height)

    class Body(TileObject):
        """This is the physiological representation for an Agent."""
        def __init__(self, sprite, env, x=0, y=0):
            super().__init__(sprite, env, x, y)

            self.reset()

        def reset(self):
            self.needs = [1.0]
            self.food_eaten_positive = 0
            self.food_eaten_negative = 0
            
            self.reset_sensors()

        def reset_sensors(self):
            self.sensors = [0 for x in range(self.env.num_sensors)]

    class Food(TileObject):
        def __init__(self, sprite, env, x=0, y=0):
            super().__init__(sprite, env, x, y)


    def __init__(self):
        self.viewer = None
        self.done = False
    
    def configure(self, config):
        self.num_tiles_x = config.setdefault('Num tiles X', 3)
        self.num_tiles_y = config.setdefault('Num tiles Y', 3)
        self.num_food_objects = config.setdefault('Num food objects', 1)
        self.global_need_decay = config.setdefault('Global need decay', -0.001)
        self.light_red_green = config.setdefault('Light Red Green', 0.5)
        self.noise = config.setdefault('Noise', [])
        self.death_at_need_zero = config.setdefault('Death at NEED=0', False)

        self.tile_width = Thesis1Env.game_area_width / self.num_tiles_x
        self.tile_height = Thesis1Env.game_area_height / self.num_tiles_y

        # noise sensors
        for i, n in enumerate(self.noise):
            name = "Noise " + str(i)
            self.SENSOR_NAMES.append(name)

        self.num_sensors = len(self.SENSOR_NAMES)

        self.action_space = spaces.Discrete(self.NUM_ACTIONS)
        self.observation_space = spaces.Discrete(self.num_sensors)

    # status area
        sprites = []

        # light
        fname = path.join(path.dirname(__file__), "assets/light.png")
        image = pyglet.image.load(fname)
        scale_x = self.status_area_tile_width / image.width
        scale_y = self.status_area_tile_height / image.height

        # red light
        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        sprite.scale_x = scale_x
        sprite.scale_y = scale_y
        sprite.color = (255,128,128)
        sprites.append(sprite)

        # green light
        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        sprite.scale_x = scale_x
        sprite.scale_y = scale_y
        sprite.color = (128,255,128)
        sprites.append(sprite)

        # noise
        for _ in self.noise:
            sprite = pyglet.sprite.Sprite(image, subpixel=True)
            sprite.scale_x = scale_x
            sprite.scale_y = scale_y
            sprite.color = (255,255,255)
            sprites.append(sprite)

        self.status_area = Thesis1Env.StatusArea(sprites, self.status_area_width, self.status_area_height, [0, self.game_area_height])

    # sprites
        # background
        fname = path.join(path.dirname(__file__),
                          "assets/green_tile.png")
        image = pyglet.image.load(fname)
        scale_x = self.tile_width / image.width
        scale_y = self.tile_height / image.height

        self.sprite_batch_background = pyglet.graphics.Batch()
        self.sprite_background = []

        pos_y = 0
        for _ in range(self.num_tiles_y):
            pos_x = 0
            for _ in range(self.num_tiles_x):
                self.sprite_background.append(pyglet.sprite.Sprite(
                    image, pos_x, pos_y, subpixel=True, batch=self.sprite_batch_background))
                self.sprite_background[-1].scale_x = scale_x
                self.sprite_background[-1].scale_y = scale_y

                pos_x += self.tile_width
            pos_y += self.tile_height

        # body
        fname = path.join(path.dirname(__file__), "assets/cat.png")
        image = pyglet.image.load(fname)
        scale_x = self.tile_width / image.width
        scale_y = self.tile_height / image.height

        sprite = pyglet.sprite.Sprite(image, subpixel=True)
        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        pos = [np.random.randint(self.num_tiles_x),
               np.random.randint(self.num_tiles_x)]
        self.body = Thesis1Env.Body(sprite, self, pos[0], pos[1])

        # food
        fname = path.join(path.dirname(__file__), "assets/fish.png")
        self.food_image = pyglet.image.load(fname)
        self._spawn_food_objects()

        # initialize
        self.reset()

    def _spawn_food_objects(self):
        """Spawn all the food objects at random locations, making sure they dont overlap. """

        self.food = []
        for _ in range(self.num_food_objects):
            pos = self._get_unoccupied_random_tile()
            self._add_food_object(pos)
    
    def _add_food_object(self, pos):
        """Add a new food object at a specific tile position"""

        scale_x = self.tile_width / self.food_image.width
        scale_y = self.tile_height / self.food_image.height
        
        sprite = pyglet.sprite.Sprite(self.food_image, subpixel=True)
        sprite.scale_x = scale_x
        sprite.scale_y = scale_y

        if self.viewer:
            self.viewer.add_drawable(sprite)

        self.food.append(Thesis1Env.Food(sprite, self, pos[0], pos[1]))

    def _remove_food_object(self, food):
        """Remove a food object"""

        self.food.remove(food)

        if self.viewer:
            self.viewer.remove_drawable(food.sprite)

    def _get_initial_state(self):
        """Get the initial state."""

        self._simulate()
        self._update_sensors()
        return (self.body.sensors, {"needs": self.body.needs, 
                                    "rewards": [0], 
                                    "food_eaten_positive": self.body.food_eaten_positive,
                                    "food_eaten_negative": self.body.food_eaten_negative})

    def _limit_coordinates(self, coord):
        coord[0] = min(coord[0], self.num_tiles_x - 1)
        coord[0] = max(coord[0], 0)
        coord[1] = min(coord[1], self.num_tiles_y - 1)
        coord[1] = max(coord[1], 0)
        return coord

    def _action_move(self, action):
        diff = Thesis1Env.actions_move[action]
        tmp = self.body.tile_pos + diff
        self.body.tile_pos = self._limit_coordinates(tmp)

        self.body.needs[0] += Thesis1Env.REWARD_MOVE


    def _action_eat(self):
        food_eaten = False
        for f in self.food:
            if (f.tile_pos == self.body.tile_pos).all():

                # check Light status
                if self.red_light_active:
                    self.body.needs[0] += Thesis1Env.REWARD_EAT_FOOD_NEGATIVE
                    self.body.food_eaten_negative += 1
                elif self.green_light_active:
                    self.body.needs[0] += Thesis1Env.REWARD_EAT_FOOD_POSITIVE
                    self.body.food_eaten_positive += 1
                else:
                    print('omg, both lights active')

                self._remove_food_object(f)
                food_eaten = True

                break # food never overlap

        # If no food left respawn them all
        if len(self.food) == 0:
            self._spawn_food_objects()

        # eating at an empty tile
        if not food_eaten:
            self.body.needs[0] += Thesis1Env.REWARD_EAT_FOOD_EMPTY        

    def _get_unoccupied_random_tile(self):
        """ Returns a tile that isn't occupied by anything. """

        excluded = []
        excluded.append(self.body.tile_pos)

        for f in self.food:
            excluded.append(f.tile_pos)

        return self._get_random_tile(excluded)

    def _get_random_tile(self, excluded=[]):
        """Get a random tile with optional exlusions."""
        while True:
            rx = np.random.randint(self.num_tiles_x)
            ry = np.random.randint(self.num_tiles_y)

            invalid = False
            for x in excluded:
                if rx == x[0] and ry == x[1]:
                    invalid = True
                    break
            if not invalid:
                break

        return np.array([rx, ry], dtype=np.int64)

    def seed(self, seed=None):
        seed = seeding.np_random(seed)
        return [seed]

    def _update_sensors(self):
        """"Sense the environment."""

        # smelly vision and standing on food
        if len(self.food) != 0:
            intensities = []

            standing_on_food = False

            # Receptor locations
            for i in range(len(Thesis1Env.RECEPTOR_OFFSETS)):
                receptor_pos = self.body.tile_pos + Thesis1Env.RECEPTOR_OFFSETS[i]

                # intensity to all food objects
                intensity = 0.0
                for f in self.food:
                    food_pos = f.tile_pos

                    if (self.body.tile_pos == food_pos).all():
                        standing_on_food = True

                    diff = receptor_pos - food_pos

                    d = math.sqrt(diff[0]**2 + diff[1]**2)
                    intensity += 1.0 / (1.0 + d**2)

                intensities.append(intensity)

            max_value = np.amax(intensities)
            candidates = np.argwhere(intensities == max_value).flatten()
            highest = np.random.choice(candidates)

            # Smelly vision
            self.body.sensors[highest] = 1

            # center
            if standing_on_food:
                self.body.sensors[4] = 1
                if len(self.food) == 1:
                    # This is a special case when there is only one food in the environment, since we know that if the center 
                    # is active it makes no sense to activate any other direction. This can otherwise happen since 
                    # a random direction is picked if multiple directions have the same value.
                    self.body.sensors[highest] = 0
        
        # red/green light
        self.body.sensors[5] = self.red_light_active
        self.body.sensors[6] = self.green_light_active

        # noise
        for i, n in enumerate(self.noise_active):
            self.body.sensors[7 + i] = n

    def _simulate(self):
        # red/green light
        random_uniform = np.random.random()
        if random_uniform < self.light_red_green:
            self.red_light_active = 1
            self.green_light_active = 0
        else:
            self.red_light_active = 0
            self.green_light_active = 1

        # noise
        self.noise_active = []
        for n in self.noise:
            random_uniform = np.random.random()
            if random_uniform < n:
                self.noise_active.append(1)
            else:
                self.noise_active.append(0)

        visibility = [self.red_light_active, self.green_light_active]
        visibility += self.noise_active

        # set icons
        self.status_area.set_visibility(visibility)

    def step(self, action):

        reward = self.body.needs[0]
    
        # Movement
        if action == 0 or action == 1 or action == 2 or action == 3:
            self._action_move(action)
        elif action == 4:
            self._action_eat()

        # cap the need
        if Thesis1Env.CAP_NEED and self.body.needs[0] > 1:
            self.body.needs[0] = 1

        # progress simulation (light and noise)
        self._simulate()

        # Sensors
        self.body.reset_sensors()
        self._update_sensors()
        
        state = list(self.body.sensors)

        # reward/s
        reward = self.body.needs[0] - reward

        # Global decay
        self.body.needs[0] += self.global_need_decay

        # Termination condition
        if self.death_at_need_zero and self.body.needs[0] < 0.0:
            self.done = True

        return (state, None, self.done, {"needs": self.body.needs,
                                         "rewards": [reward],
                                         "food_eaten_positive": self.body.food_eaten_positive,
                                         "food_eaten_negative": self.body.food_eaten_negative})

    def reset(self):
        self.seed()

        self.done = False

        self.body.reset()

        # randomize starting positions
        self.body.tile_pos = self._get_random_tile()
        self._spawn_food_objects()

        # return the initial state
        return self._get_initial_state()

    def _get_viewer(self):

        if self.viewer is None:
            self.viewer = Renderer(
                Thesis1Env.screen_width, Thesis1Env.screen_height)

            self.viewer.add_drawable(self.sprite_batch_background)
            
            for f in self.food:
                self.viewer.add_drawable(f.sprite)
            
            for s in self.status_area.sprites:
                self.viewer.add_drawable(s)

            self.viewer.add_drawable(self.body.sprite)

        return self.viewer

    def render(self, mode='human', close=False):

        if close:
            self.done = True
            return

        viewer = self._get_viewer()

        if mode == 'human':
            self.done = not viewer.render()
        elif mode == 'rgb_array':
            return viewer.render(True)
        else:
            raise NotImplementedError

    def close(self):
        print("render::close")
        if self.viewer:
            self.viewer.close()


if __name__ == '__main__':
    import aaa_survivability.envs.animat.smelly_vision_env

    env = gym.make('thesis_1-v0')

    config = dict()
    config['Num tiles X'] = 3
    config['Num tiles Y'] = 3
    config['Num food objects'] = 3
    config['Global need decay'] = -0.1
    config['Noise'] = [0.5, 0.1, 0.9]
    config['Death at NEED=0'] = False
    env.configure(config)

    env.reset()

    done = False
    while not done:
        action = env.action_space.sample()
        state, reward, done, info = env.step(action)

        print(info["needs"], info["rewards"], state)

        env.render()
        time.sleep(1/10)

    env.close()
    print("Finished")
