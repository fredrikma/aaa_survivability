import random
import collections
import numpy as np
from aaa_survivability.agents.agent import Agent
from aaa_survivability.agents.animat.perception_graph import PerceptionGraph


class AnimatBaseline(Agent):
    """The Generic Animat model."""

    def __init__(self, action_space, sensor_names, sensor_states, info, action_names=None):
        super().__init__()
        self.action_space = action_space
        self.sensor_states = sensor_states
        self.sensor_names = sensor_names
        self._action = 0

        # Information that can be queried for
        self.information = None
        self.information_variable = None

        # Enable/Disable learning
        self.learning_enabled = True

        # use_explore can only be used if a need exists!
        self.use_explore = False
        self.explore_rate = 1
        self.explore_counter = 0
        self.well_being = []
        self.sustainability_horizon = 5
        self.sustainability = 0
        self.sustainability_maximum = 1e9

        # epsilon is active if use_explore = False
        self.epsilon = 0.01
        self.epsilon_counter = 0

        # Exploit parameter
        self.exploit_discount_rate = 1

        # Probabilistic merge
        self.use_probabilistic_merge = True
        self.probabilistic_merge_counter = 0
        self.probabilistic_merge_node_counter = 0
        self.probabilistic_merge_prob = 0.002
        self.comb = {}

        # Emotional merge
        self.use_emotional_merge = True
        self.emotional_merge_counter = 0
        self.emotional_merge_node_counter = 0

        # Effect surprise parameters
        self.use_emotional_learning = self.use_emotional_merge
        self.surprise_min_updates = 1
        self.emotional_learning_threshold = 0.12
        self.emotional_surprise_counter = 0

        # Q-learning parameters
        self.start_alpha = 0.6
        self.alpha_decay = 0.999
        self.min_alpha = 0.05
        self.alpha = self.start_alpha
        self.gamma = 1.0
        self.initial_reliability = 1

        # needs and rewards
        if 'needs' in info.keys():
            self.initial_needs = list(info["needs"])
            self.needs = list(info["needs"])
            self.rewards = np.zeros(len(self.needs))
        else:
            self.rewards = np.zeros(1)

        if action_names == None:
            self.action_names = [str(x) for x in range(self.action_space.n)]
        else:
            self.action_names = action_names

        # create perception graph
        self.pg = PerceptionGraph()
        [self.pg.add_SENSOR_node(sensor) for sensor in sensor_names]
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()        

        # create Q, R and surprise matrices
        self.Q_matrices = []
        self.R_matrices = []         
        self._R_mean = []
        self._R_std = []         
        self._R_count = []
        self._R_M2 = []
        if self.use_emotional_learning: 
            self._surprise_values = []
        for _ in self.rewards:
            Q = {}
            R = {}      
            R_mean = {}
            R_std = {}       
            R_count = {}
            R_M2 = {}
            if self.use_emotional_learning: 
                self._surprise_values.append({})
            for b in self.sensor_names:
                Q[b] = np.zeros(self.action_space.n)
                R[b] = np.empty(self.action_space.n)
                R[b].fill(self.initial_reliability)                
                R_mean[b] = np.zeros(self.action_space.n)
                R_std[b] = np.zeros(self.action_space.n)
                R_count[b] = np.zeros(self.action_space.n)
                R_M2[b] = np.zeros(self.action_space.n)
            self.Q_matrices.append(Q)
            self.R_matrices.append(R)            
            self._R_mean.append(R_mean)
            self._R_std.append(R_std)
            self._R_count.append(R_count)
            self._R_M2.append(R_M2)

        # create global Q matrix
        self._global_Q = np.zeros((len(self.rewards), self.action_space.n))

        # create exploit
        self._exploit = np.zeros(self.action_space.n)

        # create explore
        self._explore = np.zeros(self.action_space.n)

        # create utility
        self._utility = np.zeros(self.action_space.n)

        # Fill state space
        # self._fill_pg_AND_nodes()

        # init statistics
        self.reset_statistics()

    def _update_combination_probabilities(self, top_active):
        """ Comb(c,c') : Gives the probability for c and c' to be top-active at the same time.
        Definition 12 (Experience set).
        
        We track counters for (c AND c'), (c' AND c) """

        nodes = list(top_active)
        while len(nodes) > 1:
            for i in range(1, len(nodes)):
                name_1 = nodes[0] + ',' + nodes[i]
                name_2 = nodes[i] + ',' + nodes[0]
                if name_1 in self.comb:
                    self.comb[name_1] += 1
                elif name_2 in self.comb:
                    self.comb[name_2] += 1
                else:
                    self.comb[name_1] = 1
            nodes = nodes[1:]
        
    def _probabilistic_merge(self):
        """ Learning rule 6 (Probabilistic merge)."""

        random_uniform = np.random.random()
        if random_uniform > self.probabilistic_merge_prob:
            return

        self.probabilistic_merge_counter += 1
        n_pairs = sum(self.comb.values())
        probabilities = {key:value / n_pairs for key, value in self.comb.items()}
        
        # select predecessors weighted by occurence, RWS
        def weighted_random_choice(choices):
            max = sum(choices.values())
            pick = random.uniform(0, max)
            current = 0
            for key, value in choices.items():
                current += value
                if current > pick:
                    return key
            return None

        predecessors_key = weighted_random_choice(probabilities)
        if not predecessors_key:
            return

        predecessors = predecessors_key.split(',')
        new_node = self.pg.add_AND_node(predecessors)
        print("Formed {}: '{}' AND '{}' : (Probabilistic merge)".format(
                new_node, predecessors[0], predecessors[1]))
        self._add_new_node(new_node)
        self.probabilistic_merge_node_counter += 1
    
    def _emotional_merge(self, pre_top_active):
        if len(pre_top_active) > 1:
            self.emotional_merge_counter += 1
            new_node = self.pg.add_random_AND_node(pre_top_active)
            if new_node is not None:
                predecessors = [x for x in self.pg.G.predecessors(new_node)]
                print("Formed {}: '{}' AND '{}' : (Emotional merge)".format(
                    new_node, predecessors[0], predecessors[1]))
                self._add_new_node(new_node)
                self.emotional_merge_node_counter += 1

    def _update_well_being(self, needs):
        if len(self.well_being) == self.sustainability_horizon:
            self.well_being = self.well_being[1:]
        self.well_being.append(np.amin(needs))

    def _update_sustainability(self, well_being):
        if len(self.well_being) == self.sustainability_horizon:
            if self.well_being[0] <= self.well_being[-1]:
                self.sustainability = self.sustainability_maximum
            else:
                self.sustainability = self.well_being[-1]*self.sustainability_horizon / (
                    self.well_being[0] - self.well_being[-1])
        else:
            self.sustainability = 0

    def reset_needs(self):
        if hasattr(self, 'needs'):
            for i, _ in enumerate(self.needs):
                self.needs[i] = self.initial_needs[i]

    def reset_statistics(self):
        self.action_frequency = np.zeros(self.action_space.n, dtype=int)
        self.probabilistic_merge_counter = 0
        self.probabilistic_merge_node_counter = 0
        self.emotional_surprise_counter = 0
        self.emotional_merge_counter = 0
        self.emotional_merge_node_counter = 0
        self.epsilon_counter = 0        

        if hasattr(self, 'needs'):
            self.needs_over_time = [[] for x in range(len(self.needs))]
            for i, n in enumerate(self.needs):
                self.needs_over_time[i].append(n)

    def get_action_learning_disabled(self, sensor_states, reward, done, info):
        self.sensor_states = sensor_states

        # Update needs
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs[i] = n

        # propagation
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()

        # Q-global update
        self._update_global_Q(self.top_active)

        # Decision making
        self._update_exploit()
        self._update_utility()

        # Action to take, here we choose to just exploit the policy. i.e no exploration
        self._exploit_policy()

        # Check if user overrides the action
        if 'override_action' in info.keys():
            if info['override_action'] != None:
                self._action = info['override_action']

        # stats
        self.action_frequency[self._action] += 1        
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs_over_time[i].append(n)

        return self._action

    def get_action_learning_enabled(self, sensor_states, reward, done, info):
        
        # If we are talking with an Animat environment, use temporal difference in needs as reward
        if 'needs' in info.keys():
            prev_needs = list(self.needs) 
            self.rewards = list(info['needs'])
            for i, n in enumerate(info["needs"]):                
                self.needs[i] = n                
                self.rewards[i] -=  prev_needs[i]
        else:
            self.rewards = [reward]

        self.sensor_states = sensor_states

        # First propagation
        pre_top_active = list(self.top_active)
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()
        
        # Combination probabilities
        if self.use_probabilistic_merge:
            self._update_combination_probabilities(self.top_active)

        # First q-global update
        self._update_global_Q(self.top_active)

        # Local Q update
        self._update_local_Q(self.rewards, pre_top_active, self._action)

        # Check if emotionally surprised
        if self.use_emotional_learning:
            is_surprised = self._check_effect_surprised(pre_top_active, self._action)

        # Local R update
        self._update_local_R_online(pre_top_active, self._action)

        # Probabilistic merge 
        if self.use_probabilistic_merge:
            self._probabilistic_merge()

        # Emotional merge
        if self.use_emotional_merge and is_surprised:
            self._emotional_merge(pre_top_active)

        # Second propagation to prepare for decision making
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()

        # Second q-global update to prepare for decision making
        self._update_global_Q(self.top_active)

        # Decision making
        if self.use_explore:
            self._update_well_being(self.needs)
            self._update_sustainability(self.well_being)
            self._update_explore()
        self._update_exploit()
        self._update_utility()

        # Action to take
        self._select_action()

        # Check if user overrides the action
        if 'override_action' in info.keys():
            if info['override_action'] != None:
                self._action = info['override_action']

        # stats
        self.action_frequency[self._action] += 1
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs_over_time[i].append(n)

        return self._action

    def get_action(self, sensor_states, reward, done, info, time_zero):
        # In case learning is disabled or time_zero = True, take alternate path
        if self.learning_enabled and not time_zero:
            action = self.get_action_learning_enabled(
                sensor_states, reward, done, info)
        else:
            action = self.get_action_learning_disabled(
                sensor_states, reward, done, info)

        # learning rate decay
        self.alpha *= self.alpha_decay
        self.alpha = max(self.alpha, self.min_alpha)

        return action

    def dump_info(self):
        """Prints useful info to console."""

        print("Q-Matrices:")
        for ni, qm in enumerate(self.Q_matrices):
            print("Need {}".format(ni))
            print("Actions : {}".format(self.action_names))

            for b, value in qm.items():
                print("{} : {}".format(b, value))
        print("----------")
        print("R-Matrices:")
        for ni, qm in enumerate(self.R_matrices):
            print("Need {}".format(ni))
            print("Actions : {}".format(self.action_names))

            for b, value in qm.items():
                print("{} : {}".format(b, value))

    def _update_global_Q(self, top_active):
        """ Updates the global Q-values: Learning rule 1 (Local Q-value updates) """

        QR_sum = np.zeros(self.action_space.n)
        R_sum = np.zeros(self.action_space.n)
        for i in range(len(self.rewards)):
            QR_sum.fill(0)
            R_sum.fill(0)
            for b in top_active:
                QR_sum += np.multiply(self.Q_matrices[i][b],
                                        self.R_matrices[i][b])
                R_sum += self.R_matrices[i][b]
            self._global_Q[i] = np.divide(QR_sum, R_sum)

    def _update_local_Q(self, rewards, pre_top_active, a):
        """ Updates the local Q-values: Learning rule 1 (Local Q-value updates) """
    
        for i in range(len(self.rewards)):
            global_Q_max = np.amax(self._global_Q[i])
            for b in pre_top_active:
                pre_Q = self.Q_matrices[i][b][a]
                self.Q_matrices[i][b][a] = pre_Q + self.alpha * \
                    (rewards[i] + self.gamma*global_Q_max - pre_Q)

                # Keep track on how local-Q will change for a specific top active node
                if self.use_emotional_learning:
                    self._surprise_values[i][b] = [pre_Q, self.Q_matrices[i][b][a]]

    def _update_local_R_online(self, pre_top_active, a):
        """ Updates the local R-values: Learning rule 2 (Local R-value updates)

            This is Welford's online algorithm "
            https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance """

        for i in range(len(self.rewards)):
            for b in pre_top_active:
                self._R_count[i][b][a] += 1
                var = 0
                rel_value = self.initial_reliability
                if self._R_count[i][b][a] > 1:
                    delta = self.Q_matrices[i][b][a] - self._R_mean[i][b][a]
                    self._R_mean[i][b][a] += delta / self._R_count[i][b][a]
                    delta2 = self.Q_matrices[i][b][a] - self._R_mean[i][b][a]
                    self._R_M2[i][b][a] += delta * delta2
                    var = self._R_M2[i][b][a] / (self._R_count[i][b][a] - 1)
                    self._R_std[i][b][a] = np.sqrt(var)
                    rel_value = 1 / (self._R_std[i][b][a] + 1)
                else:
                    self._R_mean[i][b][a] = self.Q_matrices[i][b][a]

                self.R_matrices[i][b][a] = rel_value

    def _check_effect_surprised(self, pre_top_active, a):
        """ Check if the Animat becomes surprised.
            Definition 18 (Effect surprise).
            
            TODO FM: This doesn't really make sense i think... """

        surprised = []
        for i in range(len(self.rewards)):
            surprised.append(np.zeros(len(pre_top_active)))
            for j, b in enumerate(pre_top_active):
                if self._R_count[i][b][a] > self.surprise_min_updates:
                    old_value = self._surprise_values[i][b][0]
                    new_value = self._surprise_values[i][b][1]
                    if old_value != 0:
                        update_value = np.fabs(new_value - old_value) / np.fabs(old_value)

                        # Did we pass the surprise treshold
                        if update_value > self.emotional_learning_threshold:
                            surprised[i][j] = 1
                    else: # if Q == 0 we are always surprised
                        surprised[i][j] = 1
    
        # if all top active nodes for some need are surprised then the Animat is surprised
        is_surprised = False
        for i in range(len(self.rewards)):
            if sum(surprised[i]) == len(pre_top_active):
                is_surprised = True
        
        if is_surprised:
            self.emotional_surprise_counter += 1

        return is_surprised

    def _update_exploit(self):
        if hasattr(self, 'needs'):
            new_values = np.add(self.needs, np.multiply(
                self.exploit_discount_rate, self._global_Q))
        else:
            new_values = np.multiply(
                self.exploit_discount_rate, self._global_Q)
        self._exploit = np.amin(new_values, axis=0)

    def _update_explore(self):
        random_uniform = np.random.random(self.action_space.n)
        self._explore = self.explore_rate * \
            np.add(random_uniform, self.sustainability)

    def _update_utility(self):
        if self.use_explore:
            self._utility = np.amin([self._exploit, self._explore], axis=0)
            self._used_for_explore_counter = np.argmin(
                [self._exploit, self._explore], axis=0)
        else:
            self._utility = np.copy(self._exploit)

    def _exploit_policy(self):
        max_value = np.amax(self._utility)
        best_actions = np.argwhere(
            self._utility == max_value).flatten()
        self._action = np.random.choice(best_actions)

    def _select_action(self):
        if self.use_explore:
            max_value = np.amax(self._utility)
            best_actions = np.argwhere(self._utility == max_value).flatten()
            self._action = np.random.choice(best_actions)
            if self._used_for_explore_counter[self._action] == 1:
                self.explore_counter += 1            
        else:
            biased_coin = random.random()
            if biased_coin > self.epsilon:
                max_value = np.amax(self._utility)
                best_actions = np.argwhere(
                    self._utility == max_value).flatten()
                self._action = np.random.choice(best_actions)
            else:
                self.epsilon_counter += 1
                self._action = self.action_space.sample()

    def _add_new_node(self, node):
        """ Adds a new node and updates the various matrixes"""

        for i, _ in enumerate(self.rewards):

            # Q-value for newly formed node
            self.Q_matrices[i][node] = np.zeros(self.action_space.n)

            # R-value new node
            self.R_matrices[i][node] = np.empty(self.action_space.n)
            self.R_matrices[i][node].fill(self.initial_reliability)            
            self._R_mean[i][node] = np.zeros(self.action_space.n)
            self._R_std[i][node] = np.zeros(self.action_space.n)
            self._R_count[i][node] = np.zeros(self.action_space.n)
            self._R_M2[i][node] = np.zeros(self.action_space.n)         

        # bump up learning rate
        #self.alpha = self.start_alpha
        
        # Remove possible predecessors from probabilistic merge
        # TODO: this might be very costly for larger graphs
        if self.use_probabilistic_merge:
            remove_these = []
            for key in self.comb.keys():
                predecessors = key.split(',')
                origin = set(self.pg.G.nodes[predecessors[0]]['origin'] +
                             self.pg.G.nodes[predecessors[1]]['origin'])
                uid = self.pg._get_origin_uid(origin, 'AND')
                if uid in self.pg.G.graph['uid']:
                    remove_these.append(key)            
            for key in remove_these:
                del self.comb[key]

    def get_brain_complexity(self):
        return self.pg.G.number_of_nodes()

    def get_information(self):
        """ Get a description for all information available, both 'constant' and 'variable' """

        if not self.information:
            self.setup_information()

        return self.information

    def get_variable_information(self):
        """ Get information that can change over time. i.e entries 'variable' """

        # variable information, careful to name and structure exactly as for self.information
        self.information_variable['Q-learning']['Alpha'] = "%.4f" % self.alpha

        self.information_variable['Decision making']['Epsilon counter'] = str(self.epsilon_counter)
        self.information_variable['Decision making']['Exploration counter'] = str(self.explore_counter)

        self.information_variable['Probabilistic merge']['Merge counter'] = str(self.probabilistic_merge_counter)
        self.information_variable['Probabilistic merge']['Merge node counter'] = str(self.probabilistic_merge_node_counter)

        self.information_variable['Emotional merge']['Merge counter'] = str(self.emotional_merge_counter)
        self.information_variable['Emotional merge']['Merge node counter'] = str(self.emotional_merge_node_counter)

        self.information_variable['Effect surprise']['Surprise counter'] = str(self.emotional_surprise_counter)

        self.information_variable['Perception graph']['# AND nodes'] = str(len(self.pg.G.graph['node_types']['AND']))
        self.information_variable['Perception graph']['# SEQ nodes'] = str(len(self.pg.G.graph['node_types']['SEQ']))
        self.information_variable['Perception graph']['# Total nodes'] = str(len(self.pg.G))
        
        return self.information_variable
        
    def setup_information(self):
        """ Setup the information to display in GUI runs. """

        # dict with categories
        self.information = collections.OrderedDict()
        self.information_variable = collections.OrderedDict()

        # each category is then a dict with entries
        self.information['Q-learning'] = collections.OrderedDict()
        self.information['Decision making'] = collections.OrderedDict()
        self.information['Probabilistic merge'] = collections.OrderedDict()
        self.information['Emotional merge'] = collections.OrderedDict()
        self.information['Effect surprise'] = collections.OrderedDict()
        self.information['Perception graph'] = collections.OrderedDict()

        # categories that can hold variable information, these are set in self.get_variable_information()
        self.information_variable['Q-learning'] = collections.OrderedDict()
        self.information_variable['Decision making'] = collections.OrderedDict()
        self.information_variable['Probabilistic merge'] = collections.OrderedDict()
        self.information_variable['Emotional merge'] = collections.OrderedDict()
        self.information_variable['Effect surprise'] = collections.OrderedDict()
        self.information_variable['Perception graph'] = collections.OrderedDict()

        # an entry is a tuple (value, volatility)
        ## Q-learning parameters
        self.information['Q-learning']['Alpha'] = ("%.4f" % self.alpha, 'variable')
        self.information['Q-learning']['Start alpha'] = ("%.4f" % self.start_alpha, 'constant')
        self.information['Q-learning']['Minimum alpha'] = ("%.4f" % self.min_alpha, 'constant')
        self.information['Q-learning']['Alpha decay'] = ("%.4f" % self.alpha_decay, 'constant')
        self.information['Q-learning']['Gamma'] = ("%.4f" % self.gamma, 'constant')
        self.information['Q-learning']['Initial reliability'] = ("%.4f" % self.initial_reliability, 'constant')

        # # Exploration
        self.information['Decision making']['Epsilon'] = ("%.4f" % self.epsilon, 'constant')
        self.information['Decision making']['Epsilon counter'] = (str(self.epsilon_counter), 'variable')

        self.information['Decision making']['Use exploration'] = (str(self.use_explore), 'constant')
        self.information['Decision making']['Exploration rate'] = ("%.4f" % self.explore_rate, 'constant')
        self.information['Decision making']['Exploration counter'] = (str(self.explore_counter), 'variable')
        self.information['Decision making']['Sustainability horizon'] = (str(self.sustainability_horizon), 'constant')
        self.information['Decision making']['Maximum sustainability'] = ("%.4e" % self.sustainability_maximum, 'constant')
        self.information['Decision making']['Exploit discount rate'] = ("%.4f" % self.exploit_discount_rate, 'constant')

        # # Probabilistic merge
        self.information['Probabilistic merge']['Use probabilistic merge'] = (str(self.use_probabilistic_merge), 'constant')
        self.information['Probabilistic merge']['Merge counter'] = (str(self.probabilistic_merge_counter), 'variable')
        self.information['Probabilistic merge']['Merge node counter'] = (str(self.probabilistic_merge_node_counter), 'variable')
        self.information['Probabilistic merge']['Merge probability'] = ("%.4f" % self.probabilistic_merge_prob, 'constant')

        # # Emotional merge
        self.information['Emotional merge']['Use emotional merge'] = (str(self.use_emotional_merge), 'constant')
        self.information['Emotional merge']['Merge counter'] = (str(self.emotional_merge_counter), 'variable')
        self.information['Emotional merge']['Merge node counter'] = (str(self.emotional_merge_node_counter), 'variable')

        # # Effect surprise parameters
        self.information['Effect surprise']['Use emotional learning'] = (str(self.use_emotional_learning), 'constant')
        self.information['Effect surprise']['Minimum updates'] = (str(self.surprise_min_updates), 'constant')
        self.information['Effect surprise']['Surprise treshold'] = ("%.4f" % self.emotional_learning_threshold, 'constant')
        self.information['Effect surprise']['Surprise counter'] = (str(self.emotional_surprise_counter), 'variable')


        # # Perception graph
        self.information['Perception graph']['# SENSOR nodes'] = (len(self.sensor_names), 'constant')
        self.information['Perception graph']['# AND nodes'] = (str(len(self.pg.G.graph['node_types']['AND'])), 'variable')
        self.information['Perception graph']['# SEQ nodes'] = (str(len(self.pg.G.graph['node_types']['SEQ'])), 'variable')
        self.information['Perception graph']['# Total nodes'] = (str(len(self.pg.G)), 'variable')
        
        # refresh the variable information
        self.get_variable_information()