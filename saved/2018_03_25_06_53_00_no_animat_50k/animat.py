import random
import collections
import numpy as np
from aaa_survivability.agents.agent import Agent
from aaa_survivability.agents.animat.perception_graph import PerceptionGraph


class Animat(Agent):
    """The Generic Animat model."""

    def __init__(self, action_space, sensor_names, sensor_states, info, action_names=None):
        super().__init__()
        self.action_space = action_space
        self.sensor_states = sensor_states
        self.sensor_names = sensor_names
        self._action = 0

        # Information that can be queried for
        self.information = None
        self.information_variable = None

        # Enable/Disable learning
        self.learning_enabled = True

        # use_explore can only be used if a need exists!
        self.use_explore = False
        self.explore_rate = 1
        self.explore_counter = 0
        self.well_being = []
        self.sustainability_horizon = 5
        self.sustainability = 0
        self.sustainability_maximum = 1e9

        # epsilon is active if use_explore = False
        self.epsilon = 0.01
        self.epsilon_counter = 0

        # Exploit parameter
        self.exploit_discount_rate = 1

        # Probabilistic formation
        self.use_probabilistic_formation = False
        self.probabilistic_formation_counter = 0
        self.probabilistic_node_counter = 0
        self.combination_window = 100
        self.probabilistic_prob = 0.001
        self.and_comb = {}

        # Probabilistic merge
        self.use_probabilistic_merge = False
        self.probabilistic_merge_counter = 0
        self.probabilistic_merge_node_counter = 0
        self.probabilistic_merge_prob = 0.001
        self.comb = {}

        # Emotional formation
        self.use_emotional_formation = True        
        self.emotional_formation_counter = 0
        self.emotional_AND_counter = 0
        self.emotional_SEQ_counter = 0
        self.emotional_AND_prob = 1.0
        
        # Emotional merge
        self.use_emotional_merge = False
        self.emotional_merge_counter = 0
        self.emotional_merge_node_counter = 0

        # Emotional surprise parameters
        self.use_emotional_learning = self.use_emotional_formation or self.use_emotional_merge
        self.use_new_surprise = True
        self.enough_if_one_top_active_is_surprised = True
        self.new_surprise_num_std = 2
        self.new_surprise_min_updates = 20
        self.surprise_min_updates = 1
        self.emotional_learning_threshold = 0.1
        self.emotional_surprise_counter = 0

        # Q-learning parameters
        self.start_alpha = 0.05
        self.alpha_decay = 1
        self.min_alpha = 0.01
        self.alpha = self.start_alpha
        self.gamma = 1.0
        self.use_average_q = True        
        self.sigmoid_max = 1.0
        self.sigmoid_middle = 10.0
        self.sigmoid_slope = 0.4
        self.initial_reliability = self._reliability_sigmoid_function(0)

        # Use all data or moving window reliability, both resets after a new node is created
        self.use_moving_window_reliability = False
        self.reliability_window = 100

        # Choose what to base the reliability on, the possible choises are:
        # 'delta_Q', 'Q' and 'reward'.
        self.reliability_based_on = 'delta_Q'

        # Reset reliability for all nodes after a new node is added
        self.reset_reliability_new_node = False

        # Use relibility in exploit
        self.use_reliability_in_exploit = True

        # needs and rewards
        if 'needs' in info.keys():
            self.initial_needs = list(info["needs"])
            self.needs = list(info["needs"])
            self.rewards = np.zeros(len(self.needs))
        else:
            self.rewards = np.zeros(1)

        if action_names == None:
            self.action_names = [str(x) for x in range(self.action_space.n)]
        else:
            self.action_names = action_names

        # create perception graph
        self.pg = PerceptionGraph()
        [self.pg.add_SENSOR_node(sensor) for sensor in sensor_names]
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()        

        # create Q, R and surprise matrices
        self.Q_matrices = []
        self.R_matrices = []        
        self._new_R_values = []        
        self._R_mean = []
        self._R_std = []            
        if self.use_moving_window_reliability:
            self._R_values_moving_window = []        
        else:
            self._R_count = []
            self._R_M2 = []
        if self.use_emotional_learning:   
            if self.use_new_surprise:
                self._updates_since_new_node = []
            else:
                self._surprise_values = []
        for _ in self.rewards:
            Q = {}
            R = {}
            self._new_R_values.append({})            
            R_mean = {}
            R_std = {}            
            if self.use_moving_window_reliability:
                moving_window = {}  
            else:
                R_count = {}
                R_M2 = {}
            if self.use_emotional_learning:   
                if self.use_new_surprise:
                    U = {}                
                else:
                    self._surprise_values.append({})
            for b in self.sensor_names:
                Q[b] = np.zeros(self.action_space.n)
                R[b] = np.empty(self.action_space.n)
                R[b].fill(self.initial_reliability)                
                R_mean[b] = np.zeros(self.action_space.n)
                R_std[b] = np.zeros(self.action_space.n)
                if self.use_moving_window_reliability:
                    moving_window[b] = []
                    [moving_window[b].append(np.empty(0)) for _ in range(self.action_space.n)]                    
                else:
                    R_count[b] = np.zeros(self.action_space.n)
                    R_M2[b] = np.zeros(self.action_space.n)
                if self.use_emotional_learning:   
                    if self.use_new_surprise:
                        U[b] = np.zeros(self.action_space.n)
            self.Q_matrices.append(Q)
            self.R_matrices.append(R)            
            self._R_mean.append(R_mean)
            self._R_std.append(R_std)            
            if self.use_moving_window_reliability:
                self._R_values_moving_window.append(moving_window)    
            else:
                self._R_count.append(R_count)
                self._R_M2.append(R_M2)
            if self.use_emotional_learning:   
                if self.use_new_surprise:
                    self._updates_since_new_node.append(U)

        # create global Q matrix
        self._global_Q = np.zeros((len(self.rewards), self.action_space.n))

        # create exploit
        self._exploit = np.zeros(self.action_space.n)

        # create explore
        self._explore = np.zeros(self.action_space.n)

        # create utility
        self._utility = np.zeros(self.action_space.n)

        # Fill state space
        # self._fill_pg_AND_nodes()

        # init statistics
        self.reset_statistics()

    def _update_comb(self, top_active):
        nodes = list(top_active)
        while len(nodes) > 1:
            for i in range(1, len(nodes)):
                name_1 = nodes[0] + ',' + nodes[i]
                name_2 = nodes[i] + ',' + nodes[0]
                if name_1 in self.comb:
                    self.comb[name_1] += 1
                elif name_2 in self.comb:
                    self.comb[name_2] += 1
                else:
                    self.comb[name_1] = 1
            nodes = nodes[1:]
        
    def _probabilistic_merge(self):

        random_uniform = np.random.random()
        if random_uniform > self.probabilistic_merge_prob:
            return

        self.probabilistic_merge_counter += 1
        n_pairs = sum(self.comb.values())
        probabilities = {key:value / n_pairs for key, value in self.comb.items()}
        
        # select predecessors weighted by occurence, RWS
        def weighted_random_choice(choices):
            max = sum(choices.values())
            pick = random.uniform(0, max)
            current = 0
            for key, value in choices.items():
                current += value
                if current > pick:
                    return key
            return None

        predecessors_key = weighted_random_choice(probabilities)
        predecessors = predecessors_key.split(',')
        new_node = self.pg.add_AND_node(predecessors)
        print("Formed {}: '{}' AND '{}'".format(
                new_node, predecessors[0], predecessors[1]))
        self._add_new_node(new_node)
        self.probabilistic_merge_node_counter += 1
    
    def _emotional_merge(self, pre_top_active):
        if len(pre_top_active) > 1:
            self.emotional_merge_counter += 1
            new_node = self.pg.add_random_AND_node(pre_top_active)
            if new_node is not None:
                predecessors = [x for x in self.pg.G.predecessors(new_node)]
                print("Formed {}: '{}' AND '{}'".format(
                    new_node, predecessors[0], predecessors[1]))
                self._add_new_node(new_node)
                self.emotional_merge_node_counter += 1

    def _update_well_being(self, needs):
        if len(self.well_being) == self.sustainability_horizon:
            self.well_being = self.well_being[1:]
        self.well_being.append(np.amin(needs))

    def _update_sustainability(self, well_being):
        if len(self.well_being) == self.sustainability_horizon:
            if self.well_being[0] <= self.well_being[-1]:
                self.sustainability = self.sustainability_maximum
            else:
                self.sustainability = self.well_being[-1]*self.sustainability_horizon / (
                    self.well_being[0] - self.well_being[-1])
        else:
            self.sustainability = 0

    def reset_needs(self):
        if hasattr(self, 'needs'):
            for i, _ in enumerate(self.needs):
                self.needs[i] = self.initial_needs[i]

    def reset_statistics(self):
        self.action_frequency = np.zeros(self.action_space.n, dtype=int)
        self.probabilistic_formation_counter = 0
        self.probabilistic_node_counter = 0
        self.probabilistic_merge_counter = 0
        self.probabilistic_merge_node_counter = 0
        self.emotional_surprise_counter = 0
        self.emotional_formation_counter = 0
        self.emotional_AND_counter = 0
        self.emotional_SEQ_counter = 0
        self.emotional_merge_counter = 0
        self.emotional_merge_node_counter = 0
        self.epsilon_counter = 0        

        if hasattr(self, 'needs'):
            self.needs_over_time = [[] for x in range(len(self.needs))]
            for i, n in enumerate(self.needs):
                self.needs_over_time[i].append(n)

    def get_action_learning_disabled(self, sensor_states, reward, done, info):
        self.sensor_states = sensor_states

        # Update needs
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs[i] = n

        # propagation
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()

        # Q-global update
        self._update_global_Q(self.top_active)

        # Decision making
        self._update_exploit()
        self._update_utility()

        # Action to take, here we choose to just exploit the policy. i.e no exploration
        self._exploit_policy()

        # Check if user overrides the action
        if 'override_action' in info.keys():
            if info['override_action'] != None:
                self._action = info['override_action']

        # stats
        self.action_frequency[self._action] += 1        
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs_over_time[i].append(n)

        return self._action

    def get_action_learning_enabled(self, sensor_states, reward, done, info):
        
        # If we are talking with an Animat environment, use temporal difference in needs as reward
        if 'needs' in info.keys():
            prev_needs = list(self.needs) 
            self.rewards = list(info['needs'])
            for i, n in enumerate(info["needs"]):                
                self.needs[i] = n                
                self.rewards[i] -=  prev_needs[i]
        else:
            self.rewards = [reward]

        self.sensor_states = sensor_states

        # First propagation
        pre_top_active = list(self.top_active)
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()

        # Combination update
        if self.use_probabilistic_formation:
            self._update_combinations(self.top_active)

        # Probabililstic merge update
        if self.use_probabilistic_merge:
            self._update_comb(self.top_active)

        # First q-global update
        self._update_global_Q(self.top_active)

        # Local Q update
        self._update_local_Q(self.rewards, pre_top_active, self._action)

        # Check if emotionally surprised
        if self.use_emotional_learning:
            is_surprised, surprised_nodes = self._check_if_surprised(pre_top_active, self._action)

        # Local R update
        if self.use_moving_window_reliability:
            self._update_local_R_moving_window(pre_top_active, self._action)
        else:
            self._update_local_R_online(pre_top_active, self._action)

        # Probabilistic formation
        if self.use_probabilistic_formation:
            self._probabilistic_formation()

        # Probabilistic merge 
        if self.use_probabilistic_merge:
            self._probabilistic_merge()

        # Emotional formation
        if self.use_emotional_formation and is_surprised:
            self._emotional_formation(pre_top_active, self._action, surprised_nodes)
        
        # Emotional merge
        if self.use_emotional_merge and is_surprised:
            self._emotional_merge(pre_top_active)

        # Second propagation to prepare for decision making
        self.pg.propagate_activity(self.sensor_states)
        self.top_active = self.pg.get_top_active_nodes()

        # Second q-global update to prepare for decision making
        self._update_global_Q(
            self.top_active, self.use_reliability_in_exploit)

        # Decision making
        if self.use_explore:
            self._update_well_being(self.needs)
            self._update_sustainability(self.well_being)
            self._update_explore()
        self._update_exploit()
        self._update_utility()

        # Action to take
        self._select_action()

        # Check if user overrides the action
        if 'override_action' in info.keys():
            if info['override_action'] != None:
                self._action = info['override_action']

        # stats
        self.action_frequency[self._action] += 1
        if 'needs' in info.keys():
            for i, n in enumerate(info["needs"]):
                self.needs_over_time[i].append(n)

        return self._action

    def get_action(self, sensor_states, reward, done, info, time_zero):
        # In case learning is disabled or time_zero = True, take alternate path
        if self.learning_enabled and not time_zero:
            action = self.get_action_learning_enabled(
                sensor_states, reward, done, info)
        else:
            action = self.get_action_learning_disabled(
                sensor_states, reward, done, info)

        # learning rate decay
        self.alpha *= self.alpha_decay
        self.alpha = max(self.alpha, self.min_alpha)

        return action

    def dump_info(self):
        """Prints useful info to console."""

        print("Q-Matrices:")
        for ni, qm in enumerate(self.Q_matrices):
            print("Need {}".format(ni))
            print("Actions : {}".format(self.action_names))

            for b, value in qm.items():
                print("{} : {}".format(b, value))
        print("----------")
        print("R-Matrices:")
        for ni, qm in enumerate(self.R_matrices):
            print("Need {}".format(ni))
            print("Actions : {}".format(self.action_names))

            for b, value in qm.items():
                print("{} : {}".format(b, value))

    def _update_combinations(self, top_active):
        """Updates the 'matrix' that tracks how often b and b' are active at the same time."""

        # counts
        for b in top_active:
            if not b in self.and_comb:
                self.and_comb[b] = []

            # limit entries
            if len(self.and_comb[b]) == self.combination_window:
                self.and_comb[b] = self.and_comb[b][1:]

            # append list with nodes active together with us
            active_nodes = list(top_active)
            active_nodes.remove(b)
            self.and_comb[b].append(active_nodes)

    def _probabilistic_formation(self):

        # time to try and add this
        biased_coin = random.random()
        if biased_coin > self.probabilistic_prob:
            return

        self.probabilistic_formation_counter += 1

        # select a random node from self.and_comb
        b = random.sample(self.and_comb.keys(), 1)
        b = b[0]
        # print(b)

        # for each timestep get list of nodes active together with b
        time_data = self.and_comb[b]
        # print(time_data)

        # flatten list
        flat_list = [item for sublist in time_data for item in sublist]

        # counts per entry
        counts = collections.Counter(flat_list)

        # get valid connections
        valid_connections = self.pg.get_valid_AND_connections(b, counts.keys())

        if len(valid_connections) == 0:
            return
        # print(valid_connections)

        # remove invalid connections i.e entries in counts that are not valid
        valid_counts = {key: value for key,
                        value in counts.items() if key in valid_connections}
        # print(valid_counts)

        # select b' weighted by occurence, RWS
        def weighted_random_choice(choices):
            max = sum(choices.values())
            pick = random.uniform(0, max)
            current = 0
            for key, value in choices.items():
                current += value
                if current > pick:
                    return key
            return None

        bp = weighted_random_choice(valid_counts)
        if bp == None:
            return

        # form b AND b', it should always be possible at this stage
        node = self.pg.add_AND_node([b, bp])
        assert(node != None)

        print("Formed {}: '{}' AND '{}'".format(node, b, bp))
        self._add_new_node(node)
        self.probabilistic_node_counter += 1

    def _update_global_Q(self, top_active, use_reliability=True):
        if use_reliability:
            QR_sum = np.zeros(self.action_space.n)
            R_sum = np.zeros(self.action_space.n)
            for i in range(len(self.rewards)):
                QR_sum.fill(0)
                R_sum.fill(0)
                for b in top_active:
                    QR_sum += np.multiply(self.Q_matrices[i][b],
                                          self.R_matrices[i][b])
                    R_sum += self.R_matrices[i][b]
                self._global_Q[i] = np.divide(QR_sum, R_sum)
        else:
            Q_sum = np.zeros(self.action_space.n)
            for i in range(len(self.rewards)):
                Q_sum.fill(0)
                for b in top_active:
                    Q_sum = np.add(Q_sum, self.Q_matrices[i][b])
                self._global_Q[i] = np.divide(Q_sum, len(top_active))

    def _update_local_Q(self, rewards, pre_top_active, a):
        for i in range(len(self.rewards)):
            self._new_R_values[i].clear()
            global_Q_max = np.amax(self._global_Q[i])
            for b in pre_top_active:
                pre_Q = self.Q_matrices[i][b][a]
                self.Q_matrices[i][b][a] = pre_Q + self.alpha * \
                    (rewards[i] + self.gamma*global_Q_max - pre_Q)
                self._update_new_R_values(
                    i, b, pre_Q, self.Q_matrices[i][b][a], rewards[i])
                if self.use_emotional_learning:
                    if self.use_new_surprise:
                        self._updates_since_new_node[i][b][a] += 1
                    else:
                        self._surprise_values[i][b] = [pre_Q, self.Q_matrices[i][b][a]]

    def _update_new_R_values(self, i, b, old_Q, new_Q, reward):
        def based_on_delta_Q():
            return (new_Q - old_Q)

        def based_on_Q():
            return new_Q

        def based_on_reward():
            return reward

        func_dict = {'delta_Q': based_on_delta_Q,
                     'Q': based_on_Q, 'reward': based_on_reward}
        self._new_R_values[i][b] = func_dict[self.reliability_based_on]()

    def _update_local_R_moving_window(self, pre_top_active, a):
        for i in range(len(self.rewards)):
            for b in pre_top_active:
                np.append(
                    self._R_values_moving_window[i][b][a], (self._new_R_values[i][b]))

                if len(self._R_values_moving_window[i][b][a]) > self.reliability_window:
                    self._R_values_moving_window[i][b][a] = np.delete(
                        self._R_values_moving_window[i][b][a], 0)

                rel_value = self.initial_reliability
                n_values = len(self._R_values_moving_window[i][b][a])
                if n_values > 1:
                    self._R_mean[i][b][a] = np.mean(
                        self._R_values_moving_window[i][b][a])
                    self._R_std[i][b][a] = np.std(
                        self._R_values_moving_window[i][b][a])
                    rel_value = 1 / (self._R_std[i][b][a] + 1)

                # use of a sigmoid function to soften the initial effect of new nodes
                sigmoid_value = self._reliability_sigmoid_function(n_values)
                self.R_matrices[i][b][a] = sigmoid_value*rel_value

    def _update_local_R_online(self, pre_top_active, a):
        " This is Welford's online algorithm "
        " https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance "

        for i in range(len(self.rewards)):
            for b in pre_top_active:
                self._R_count[i][b][a] += 1
                var = 0
                rel_value = self.initial_reliability
                if self._R_count[i][b][a] > 1:
                    delta = self._new_R_values[i][b] - self._R_mean[i][b][a]
                    self._R_mean[i][b][a] += delta / self._R_count[i][b][a]
                    delta2 = self._new_R_values[i][b] - self._R_mean[i][b][a]
                    self._R_M2[i][b][a] += delta * delta2
                    var = self._R_M2[i][b][a] / (self._R_count[i][b][a] - 1)
                    self._R_std[i][b][a] = np.sqrt(var)
                    rel_value = 1 / (self._R_std[i][b][a] + 1)
                else:
                    self._R_mean[i][b][a] = self._new_R_values[i][b]

                # use of a sigmoid function to soften the initial effect of new nodes
                sigmoid_value = self._reliability_sigmoid_function(
                    self._R_count[i][b][a])
                self.R_matrices[i][b][a] = sigmoid_value*rel_value

    def _reliability_sigmoid_function(self, x):
        # Logistic function, https://en.wikipedia.org/wiki/Logistic_function
        sigmoid_value = self.sigmoid_max / (1 + np.exp(-self.sigmoid_slope*(x - self.sigmoid_middle)))
        return sigmoid_value

    def _emotional_formation(self, pre_top_active, a, surprised):
        "TODO: Add support for SEQ nodes"
        if len(pre_top_active) > 1:
            self.emotional_formation_counter += 1
            for i in range(len(self.rewards)):            
                random_uniform = random.random()
                if random_uniform < self.emotional_AND_prob:
                    self._add_emotional_AND_node(
                        pre_top_active, surprised[i], i, a)
                else:
                    pass

    def _add_emotional_AND_node(self, nodes, surprised, need, action):
        if len(nodes) > 1:
            # Sort surprised and not surprised nodes with respect to how many times they have been updated
            candidates = np.asarray(nodes)
            surprised_nodes = candidates[np.where(surprised == 1)[0]]
            not_surprised_nodes = candidates[np.where(surprised == 0)[0]]
            surprised_nodes_updates = np.asarray(
                [self._R_count[need][b][action] for b in surprised_nodes])
            not_surprised_nodes_updates = np.asarray(
                [self._R_count[need][b][action] for b in not_surprised_nodes])
            max_inds = surprised_nodes_updates.argsort()[::-1]
            sorted_surprised = surprised_nodes[max_inds]
            min_inds = not_surprised_nodes_updates.argsort()
            sorted_not_surprised = not_surprised_nodes[min_inds]

            # Tries to connect the most updated surprised nodes with one of the least updated not surprised nodes
            done = False
            new_node = None
            while not done:
                if len(sorted_surprised) > 0:
                    look_for_second_node = True
                    ind_counter = 0
                    first_node = sorted_surprised[0]
                    sorted_surprised = np.delete(sorted_surprised, 0)
                    while look_for_second_node:
                        if ind_counter < len(sorted_not_surprised):
                            second_node = sorted_not_surprised[ind_counter]
                            new_node = self.pg.add_AND_node(
                                [first_node, second_node])
                            if new_node is not None:
                                look_for_second_node = False
                                done = True
                        else:
                            look_for_second_node = False
                        ind_counter += 1
                else:
                    done = True

            # Tries to connect the surprised nodes with each other and then the not surprised nodes with each other.
            if new_node is None:
                if len(surprised_nodes) > 1:
                    new_node = self.pg.add_random_AND_node(surprised_nodes)
                if new_node is None:
                    if len(not_surprised_nodes) > 1:
                        new_node = self.pg.add_random_AND_node(
                            not_surprised_nodes)

            if new_node is not None:
                predecessors = [x for x in self.pg.G.predecessors(new_node)]
                print("Formed {}: '{}' AND '{}'".format(
                    new_node, predecessors[0], predecessors[1]))
                self._add_new_node(new_node)
                self.emotional_AND_counter += 1

    def _check_if_surprised(self, pre_top_active, a):
        surprised = []
        if self.use_new_surprise:            
            for i in range(len(self.rewards)):
                surprised.append(np.zeros(len(pre_top_active)))
                for j, b in enumerate(pre_top_active):
                    if self._updates_since_new_node[i][b][a] > self.new_surprise_min_updates:
                        lower_limit = self._R_mean[i][b][a] - \
                            self.new_surprise_num_std*self._R_std[i][b][a]
                        upper_limit = self._R_mean[i][b][a] + \
                            self.new_surprise_num_std*self._R_std[i][b][a]
                        if self._new_R_values[i][b] < lower_limit or self._new_R_values[i][b] > upper_limit:
                            surprised[i][j] = 1
                        #print(lower_limit, upper_limit, self._new_R_values[i][b], surprised[i][j])
        else:
            for i in range(len(self.rewards)):
                surprised.append(np.zeros(len(pre_top_active)))
                for j, b in enumerate(pre_top_active):
                    if self._R_count[i][b][a] > self.surprise_min_updates:
                        old_value = self._surprise_values[i][b][0]
                        new_value = self._surprise_values[i][b][1]
                        if old_value != 0:
                            update_value = np.fabs(new_value - old_value) / np.fabs(old_value)
                        else:
                            update_value = self.emotional_learning_threshold + 1
                        if update_value > self.emotional_learning_threshold:
                            surprised[i][j] = 1
                        #print(old_value, new_value, update_value, surprised[i][j], self.emotional_learning_threshold, surprised[i][j])
        
        is_surprised = False
        for i in range(len(self.rewards)):
            if self.enough_if_one_top_active_is_surprised:
                if sum(surprised[i]) > 0:
                    is_surprised = True
            else:
                if sum(surprised[i]) == len(pre_top_active):
                    is_surprised = True
        
        if is_surprised:
            self.emotional_surprise_counter += 1

        return is_surprised, surprised

    def _update_exploit(self):
        if hasattr(self, 'needs'):
            new_values = np.add(self.needs, np.multiply(
                self.exploit_discount_rate, self._global_Q))
        else:
            new_values = np.multiply(
                self.exploit_discount_rate, self._global_Q)
        self._exploit = np.amin(new_values, axis=0)

    def _update_explore(self):
        random_uniform = np.random.random(self.action_space.n)
        self._explore = self.explore_rate * \
            np.add(random_uniform, self.sustainability)

    def _update_utility(self):
        if self.use_explore:
            self._utility = np.amin([self._exploit, self._explore], axis=0)
            self._used_for_explore_counter = np.argmin(
                [self._exploit, self._explore], axis=0)
        else:
            self._utility = np.copy(self._exploit)

    def _exploit_policy(self):
        max_value = np.amax(self._utility)
        best_actions = np.argwhere(
            self._utility == max_value).flatten()
        self._action = np.random.choice(best_actions)

    def _select_action(self):
        if self.use_explore:
            max_value = np.amax(self._utility)
            best_actions = np.argwhere(self._utility == max_value).flatten()
            self._action = np.random.choice(best_actions)
            if self._used_for_explore_counter[self._action] == 1:
                self.explore_counter += 1
        else:
            biased_coin = random.random()
            if biased_coin > self.epsilon:
                max_value = np.amax(self._utility)
                best_actions = np.argwhere(
                    self._utility == max_value).flatten()
                self._action = np.random.choice(best_actions)
            else:
                self.epsilon_counter += 1
                self._action = self.action_space.sample()

    def _fill_pg_AND_nodes(self):
        n_AND_nodes = 2**len(self.sensor_names) - len(self.sensor_names)
        for _ in range(n_AND_nodes):
            node = self.pg.add_random_AND_node()
            if node != None:
                self._add_new_node(node)

    def _add_new_node(self, node):

        b1, b2 = self.pg.G.predecessors(node)

        for i, _ in enumerate(self.rewards):

            # Q-value for newly formed node
            if self.use_average_q:
                self.Q_matrices[i][node] = np.divide(
                    np.add(self.Q_matrices[i][b1], self.Q_matrices[i][b2]), 2.0)
            else:
                self.Q_matrices[i][node] = np.zeros(self.action_space.n)

            # R-value new node
            self.R_matrices[i][node] = np.empty(self.action_space.n)
            self.R_matrices[i][node].fill(self.initial_reliability)            
            self._R_mean[i][node] = np.zeros(self.action_space.n)
            self._R_std[i][node] = np.zeros(self.action_space.n)
            if self.use_moving_window_reliability:
                moving_window = []
                [moving_window.append(np.empty(0))
                 for _ in range(self.action_space.n)]
                self._R_values_moving_window[i][node] = moving_window
            else:
                self._R_count[i][node] = np.zeros(self.action_space.n)
                self._R_M2[i][node] = np.zeros(self.action_space.n)         
            if self.use_emotional_learning:   
                if self.use_new_surprise:
                    self._updates_since_new_node[i][node] = np.zeros(self.action_space.n)

        # bump up learning rate
        self.alpha = self.start_alpha

        # reset updates for new surprise
        if self.use_emotional_learning:
            if self.use_new_surprise:
                self.reset_updates_since_new_node()

        # Reset all R-values
        if self.reset_reliability_new_node:
            self.reset_reliability()
        
        # Remove possible predecessors from probabilistic merge
        if self.use_probabilistic_merge:
            remove_these = []
            for key in self.comb.keys():
                predecessors = key.split(',')
                origin = set(self.pg.G.nodes[predecessors[0]]['origin'] +
                             self.pg.G.nodes[predecessors[1]]['origin'])
                uid = self.pg._get_origin_uid(origin, 'AND')
                if uid in self.pg.G.graph['uid']:
                    remove_these.append(key)            
            for key in remove_these:
                del self.comb[key]

    def reset_updates_since_new_node(self):
        for i, _ in enumerate(self.rewards):
            for b in self.pg.G.nodes:
                self._updates_since_new_node[i][b].fill(0)

    def reset_reliability(self):
        for i, _ in enumerate(self.rewards):
            for b in self.pg.G.nodes:
                self.R_matrices[i][b].fill(self.initial_reliability)
                self._R_std[i][b].fill(0)
                self._R_mean[i][b].fill(0)
                if self.use_moving_window_reliability:
                    self._R_values_moving_window[i][b].clear()
                    [self._R_values_moving_window[i][b].append(
                        np.empty(0)) for _ in range(self.action_space.n)]
                else:
                    self._R_count[i][b].fill(0)
                    self._R_M2[i][b].fill(0)

    def get_brain_complexity(self):
        return self.pg.G.number_of_nodes()

    def get_information(self):
        """ Get a description for all information available, both 'constant' and 'variable' """

        if not self.information:
            self.setup_information()

        return self.information

    def get_variable_information(self):
        """ Get information that can change over time. i.e entries 'variable' """

        # variable information, careful to name and structure exactly as for self.information
        self.information_variable['Q-learning']['Alpha'] = "%.4f" % self.alpha

        self.information_variable['Decision making']['Epsilon counter'] = str(self.epsilon_counter)
        self.information_variable['Decision making']['Exploration counter'] = str(self.explore_counter)

        self.information_variable['Probabilistic merge']['Merge counter'] = str(self.probabilistic_merge_counter)
        self.information_variable['Probabilistic merge']['Merge node counter'] = str(self.probabilistic_merge_node_counter)

        self.information_variable['Emotional merge']['Merge counter'] = str(self.emotional_merge_counter)
        self.information_variable['Emotional merge']['Merge node counter'] = str(self.emotional_merge_node_counter)

        self.information_variable['Effect surprise']['Surprise counter'] = str(self.emotional_surprise_counter)

        self.information_variable['Emotional formation']['Formation counter'] = str(self.emotional_formation_counter)
        self.information_variable['Emotional formation']['AND counter'] = str(self.emotional_AND_counter)
        self.information_variable['Emotional formation']['SEQ counter'] = str(self.emotional_SEQ_counter)

        self.information_variable['Perception graph']['# AND nodes'] = str(len(self.pg.G.graph['node_types']['AND']))
        self.information_variable['Perception graph']['# SEQ nodes'] = str(len(self.pg.G.graph['node_types']['SEQ']))
        self.information_variable['Perception graph']['# Total nodes'] = str(len(self.pg.G))
        
        return self.information_variable
        
    def setup_information(self):
        """ Setup the information to display in GUI runs. """

        # dict with categories
        self.information = collections.OrderedDict()
        self.information_variable = collections.OrderedDict()

        # each category is then a dict with entries
        self.information['Q-learning'] = collections.OrderedDict()
        self.information['Decision making'] = collections.OrderedDict()
        self.information['Probabilistic merge'] = collections.OrderedDict()
        self.information['Emotional merge'] = collections.OrderedDict()
        self.information['Effect surprise'] = collections.OrderedDict()
        self.information['Emotional formation'] = collections.OrderedDict()
        self.information['Perception graph'] = collections.OrderedDict()

        # categories that can hold variable information, these are set in self.get_variable_information()
        self.information_variable['Q-learning'] = collections.OrderedDict()
        self.information_variable['Decision making'] = collections.OrderedDict()
        self.information_variable['Probabilistic merge'] = collections.OrderedDict()
        self.information_variable['Emotional merge'] = collections.OrderedDict()
        self.information_variable['Effect surprise'] = collections.OrderedDict()
        self.information_variable['Emotional formation'] = collections.OrderedDict()
        self.information_variable['Perception graph'] = collections.OrderedDict()

        # an entry is a tuple (value, volatility)
        ## Q-learning parameters
        self.information['Q-learning']['Alpha'] = ("%.4f" % self.alpha, 'variable')
        self.information['Q-learning']['Start alpha'] = ("%.4f" % self.start_alpha, 'constant')
        self.information['Q-learning']['Minimum alpha'] = ("%.4f" % self.min_alpha, 'constant')
        self.information['Q-learning']['Alpha decay'] = ("%.4f" % self.alpha_decay, 'constant')
        self.information['Q-learning']['Gamma'] = ("%.4f" % self.gamma, 'constant')
        self.information['Q-learning']['Initial reliability'] = ("%.4f" % self.initial_reliability, 'constant')
        self.information['Q-learning']['Sigmoid max'] = ("%.4f" % self.sigmoid_max, 'constant')
        self.information['Q-learning']['Sigmoid middle'] = ("%.4f" % self.sigmoid_middle, 'constant')
        self.information['Q-learning']['Sigmoid slope'] = ("%.4f" % self.sigmoid_slope, 'constant')
        self.information['Q-learning']['Use average Q'] = (str(self.use_average_q), 'constant')

        self.information['Q-learning']['Use moving window reliability'] = (str(self.use_moving_window_reliability), 'constant')
        if self.use_moving_window_reliability:
            self.information['Q-learning']['Reliability window'] = (str(self.reliability_window), 'constant')

        self.information['Q-learning']['Reliability uses'] = (str(self.reliability_based_on), 'constant')
        self.information['Q-learning']['Reliability reset for new node'] = (str(self.reset_reliability_new_node), 'constant')
        self.information['Q-learning']['Reliability use in exploit'] = (str(self.use_reliability_in_exploit), 'constant')

        # # Exploration
        self.information['Decision making']['Epsilon'] = ("%.4f" % self.epsilon, 'constant')
        self.information['Decision making']['Epsilon counter'] = (str(self.epsilon_counter), 'variable')

        self.information['Decision making']['Use exploration'] = (str(self.use_explore), 'constant')
        if self.use_explore:
            self.information['Decision making']['Exploration rate'] = ("%.4f" % self.explore_rate, 'constant')
            self.information['Decision making']['Exploration counter'] = (str(self.explore_counter), 'variable')
            self.information['Decision making']['Sustainability horizon'] = (str(self.sustainability_horizon), 'constant')
            self.information['Decision making']['Maximum sustainability'] = ("%.4e" % self.sustainability_maximum, 'constant')
            self.information['Decision making']['Exploit discount rate'] = ("%.4f" % self.exploit_discount_rate, 'constant')

        # # Probabilistic merge
        self.information['Probabilistic merge']['Use probabilistic merge'] = (str(self.use_probabilistic_merge), 'constant')
        if self.use_probabilistic_merge:
            self.information['Probabilistic merge']['Merge counter'] = (str(self.probabilistic_merge_counter), 'variable')
            self.information['Probabilistic merge']['Merge node counter'] = (str(self.probabilistic_merge_node_counter), 'variable')
            self.information['Probabilistic merge']['Merge probability'] = ("%.4f" % self.probabilistic_merge_prob, 'constant')

        # # Emotional merge
        self.information['Emotional merge']['Use emotional merge'] = (str(self.use_emotional_merge), 'constant')
        if self.use_emotional_merge:
            self.information['Emotional merge']['Merge counter'] = (str(self.emotional_merge_counter), 'variable')
            self.information['Emotional merge']['Merge node counter'] = (str(self.emotional_merge_node_counter), 'variable')

        # # Effect surprise parameters
        self.information['Effect surprise']['Use emotional learning'] = (str(self.use_emotional_learning), 'constant')
        if self.use_emotional_learning:
            self.information['Effect surprise']['Minimum updates'] = (str(self.surprise_min_updates), 'constant')
            self.information['Effect surprise']['Surprise treshold'] = ("%.4f" % self.emotional_learning_threshold, 'constant')
            self.information['Effect surprise']['Surprise counter'] = (str(self.emotional_surprise_counter), 'variable')

        # # Emotional formation
        self.information_variable['Emotional formation']['Use emotional formation'] = (str(self.use_emotional_formation), 'constant')
        if self.use_emotional_formation:
            self.information['Emotional formation']['Formation counter'] = (str(self.emotional_formation_counter), 'variable')
            self.information['Emotional formation']['AND counter'] = (str(self.emotional_AND_counter), 'variable')
            self.information['Emotional formation']['SEQ counter'] = (str(self.emotional_SEQ_counter), 'variable')
            self.information['Emotional formation']['AND probability'] = ("%.4f" % self.emotional_AND_prob, 'constant')

        # # Perception graph
        self.information['Perception graph']['# SENSOR nodes'] = (len(self.sensor_names), 'constant')
        self.information['Perception graph']['# AND nodes'] = (str(len(self.pg.G.graph['node_types']['AND'])), 'variable')
        self.information['Perception graph']['# SEQ nodes'] = (str(len(self.pg.G.graph['node_types']['SEQ'])), 'variable')
        self.information['Perception graph']['# Total nodes'] = (str(len(self.pg.G)), 'variable')
        
        # refresh the variable information
        self.get_variable_information()        