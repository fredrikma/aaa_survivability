import numpy as np
import pandas as pd
import sys
import os
import matplotlib.pyplot as plt
from matplotlib import style
from scipy import stats

if __name__ == '__main__':
        
    # Displays results from scenario 2 dqn comparison
    
    # ['Energy', 'Episode length', 'Green food', 'Number of nodes', 'Red food']
    legend_location = [(0.70,0.5),(0.70,0.2),0,2,0]
    style.use('ggplot')

    #data_directory = "./data/thesis_results/scenario_1_model_comparison/2018_05_01_02_42_31/raw_data/"
    #image_path = "./data/thesis_results/scenario_1_model_comparison/2018_05_01_02_42_31/images/"
    data_directory = "./saved/2018_05_02_10k/scenario_1_model_comparison/2018_05_02_02_04_51/raw_data/"
    #image_path = "./saved/2018_05_02_10k/scenario_1_model_comparison/2018_05_02_02_04_51/images/"
    image_path = None
    
    if len(sys.argv) >= 2:
        data_directory = sys.argv[1]
    if len(sys.argv) >= 3:
        image_path = sys.argv[2]

    filenames = ["AnimatBaseline.csv", "AnimatDQN.csv"]
    config_names = ["New Animat", "DQN"]

    # load pandas
    data = []
    for filename in filenames:         
        data.append(pd.read_csv(data_directory + filename))
        
    # Time values is always identical for all runs    
    time_variable = 'Trained time steps'        
    time_values = data[0][time_variable].unique()
    variable_names = [x for x in data[0].keys() if x != time_variable]
    
    # Calculate mean and std over all agents for each evaluation        
    mean_data = []
    std_data = []            
    for idx, df in enumerate(data):
        mean_dict = {}
        std_dict = {}            
        for variable_name in variable_names:
            if variable_name != time_variable:
                mean_dict[variable_name] = []
                std_dict[variable_name] = []                
            for time_value in time_values:
                if variable_name != time_variable:
                    mean = df[df[time_variable]==time_value][variable_name].mean()
                    std = df[df[time_variable]==time_value][variable_name].std()
                    mean_dict[variable_name].append(mean)
                    std_dict[variable_name].append(std)
        mean_data.append(mean_dict)
        std_data.append(std_dict)
    
    # Plot mean/std
    # ['Energy', 'Episode length', 'Green food', 'Number of nodes', 'Red food']
    for ie, variable_name in enumerate(variable_names):
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        #ax.set(title=variable_name, ylabel=variable_name, xlabel=time_variable)                
        ax.set(ylabel=variable_name, xlabel=time_variable)

        if variable_name == 'Number of nodes':
            for i in range(len(config_names)-1):
                ax.errorbar(time_values, mean_data[i][variable_name], std_data[i][variable_name], 
                    marker='.', alpha=0.6, capsize=4, capthick=1.5)
        else:
            for i in range(len(config_names)):
                ax.errorbar(time_values, mean_data[i][variable_name], std_data[i][variable_name], 
                    marker='.', alpha=0.6, capsize=4, capthick=1.5)
            
        
        current_ylim = ax.get_ylim()
        plt.ylim(ymin=max(0, current_ylim[0]))

        # Add legend
        plt.legend(config_names, loc=legend_location[ie])        

        # Save image
        if image_path:
            fig.savefig(image_path + "/" + variable_name.replace(' ', '_') + ".png")

    if not image_path:
        plt.show()