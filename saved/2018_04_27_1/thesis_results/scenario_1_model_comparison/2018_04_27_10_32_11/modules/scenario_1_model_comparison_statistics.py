import numpy as np
import pandas as pd
import sys
import os
import matplotlib.pyplot as plt
from matplotlib import style
from scipy import stats

if __name__ == '__main__':
        
    # Displays results from scenario 1 model comparison

    legend_location = 2
    style.use('ggplot')

    data_directory = "./data/thesis_results/scenario_1_model_comparison/2018_04_25_06_03_28/raw_data/"    
    #image_path = "../../data/thesis_results/scenario_1_node_formation/2018_04_21_17_38_21/images/"
    image_path = None
    
    if len(sys.argv) >= 2:
        data_directory = sys.argv[1]
    if len(sys.argv) >= 3:
        image_path = sys.argv[2]

    filenames = ["AnimatBaseline_1.csv", "AnimatBaseline_2.csv", "RandomAgent.csv", "AnimatIncrementalQLearner.csv"]

    config_names = ["Animat_1", "Animat_2", "Random", "Q-learner"]
    #config_names = ["config_1", "config_2"]

    # load pandas
    data = []
    for filename in filenames:         
        data.append(pd.read_csv(data_directory + filename))
        
    # Time values is always identical for all runs    
    time_variable = 'Trained time steps'        
    time_values = data[0][time_variable].unique()
    variable_names = [x for x in data[0].keys() if x != time_variable]
    
    # Calculate mean and std over all agents for each evaluation        
    mean_data = []
    std_data = []            
    for df in data:        
        mean_dict = {}
        std_dict = {}            
        for variable_name in variable_names:
            if variable_name != time_variable:
                mean_dict[variable_name] = []
                std_dict[variable_name] = []                
            for time_value in time_values:
                if variable_name != time_variable:
                    mean = df[df[time_variable]==time_value][variable_name].mean()
                    std = df[df[time_variable]==time_value][variable_name].std()
                    mean_dict[variable_name].append(mean)
                    std_dict[variable_name].append(std)
        mean_data.append(mean_dict)
        std_data.append(std_dict)

    # # Calculate A/B testing p-values
    # ab_p_values_data = []        
    # for variable_name in variable_names:
    #     ab_p_values_dict = {}    
    #     for i in range(len(config_names)):
    #         df_1 = data[i]
    #         for j in range(i+1, len(config_names)):
    #             df_2 = data[j]
    #             ab_test_name = config_names[i] + ' vs ' + config_names[j]
    #             ab_p_values_dict[ab_test_name] = []
    #             for time_value in time_values:
    #                 a_data = df_1[df_1[time_variable]==time_value][variable_name]
    #                 b_data = df_2[df_2[time_variable]==time_value][variable_name]
    #                 # Calculate Welch's t-test
    #                 p_value = stats.ttest_ind(a_data, b_data, equal_var = False)[1]
    #                 ab_p_values_dict[ab_test_name].append(p_value)

    #     ab_p_values_data.append(ab_p_values_dict)    
    
    # Plot mean/std
    for variable_name in variable_names:
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.set(title=variable_name, ylabel=variable_name, xlabel=time_variable)                

        for i in range(len(config_names)):
            ax.errorbar(time_values, mean_data[i][variable_name], std_data[i][variable_name], 
                marker='.', alpha=0.6, capsize=4, capthick=1.5)            
        
        # log scale for number of nodes
        if variable_name == 'Number of nodes':
            ax.set_yscale('log')
        else:
            current_ylim = ax.get_ylim()
            plt.ylim(ymin=max(0, current_ylim[0]))

        # Add legend
        plt.legend(config_names, loc=legend_location)        

        # Save image
        if image_path:
            fig.savefig(image_path + "/" + variable_name.replace(' ', '_') + ".png")

    # # Plot A/B tests
    # for i, ab_p_values_dict in enumerate(ab_p_values_data):
    #     fig = plt.figure()
    #     ax = fig.add_subplot(1,1,1)
    #     ax.set(title='A/B test: ' + variable_names[i], ylabel='p-value', xlabel=time_variable)                
    #     legend = []
    #     for ab_name, ab_p_values in ab_p_values_dict.items():
    #         legend.append(ab_name)
    #         ax.plot(time_values, ab_p_values, marker='.')            
        
    #     # Add dashed line at p=0.05
    #     ax.axhline(0.05, ls='--', c='black', alpha=0.5)
    #     legend.append('p=0.05')

    #     # None of the statistics we are looking at can be negative
    #     current_ylim = ax.get_ylim()
    #     plt.ylim(ymin=max(0, current_ylim[0]))

    #     # Add legend
    #     plt.legend(legend, loc=legend_location) 

    #     # Save image
    #     if image_path:
    #         fig.savefig(image_path + "/" + variable_names[i] + "_AB.png")

    if not image_path:
        plt.show()